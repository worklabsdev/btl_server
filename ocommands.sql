CREATE EVENT `AutoDeleteOldCronLogs`
ON SCHEDULE EVERY 1 MONTH
ON COMPLETION PRESERVE ENABLE
COMMENT 'Delete Old Crons Log'
DO 
DELETE LOW_PRIORITY FROM crons WHERE createdAt < DATE_SUB(NOW(), INTERVAL 60 DAY);




CREATE EVENT `AutoDeleteUserLoginHist`
ON SCHEDULE EVERY 1 MONTH
ON COMPLETION PRESERVE ENABLE
COMMENT 'Delete Old User Logins'
DO 
DELETE LOW_PRIORITY FROM loginHists WHERE createdAt < DATE_SUB(NOW(), INTERVAL 90 DAY);


SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));