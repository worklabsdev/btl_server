let apiPath = {

};
exports.apiPath = apiPath

let appConstants = {
	"name": process.env.APP_SHORT_NAME || "BT",
	"fullName": process.env.APP_NAME || "Bamboo Tree"
};
exports.appConstants = appConstants;

let successCodes = {
	"success": 1,
	"failed": 0,
	"created": 1,
	"authFailed": -1,
	"validationFailed": -2,
	"error": 0,
	"actionFailed": 0,
	"pageNotFound": 0
};
exports.successCodes = successCodes;

let statusCodes = {
	"success": 200,
	"failed": 500,
	"created": 201,
	"validationFailed": 400,
	"authFailed": 401,
	"error": 500,
	"actionFailed": 400,
	"pageNotFound": 404,

	"actionComplete": 204,
	"actionPending": 202
};
exports.statusCodes = statusCodes;

let globalMessages = {
	"responseMessages": {
		"errorMsg": "Something went wrong, please try again",
		"successMsg": "Success",
		"validationFailedMsg": "Parameter missing or parameter type is wrong",
		"authFailedMsg": "Please login first, to continue",
		"authHeaderMissing": "Access token is missing",
		"accountSuspended": "Your account has been suspended, kindly contact admin",
		"imageUploadError": " not uploaded, please try again",
		"accountNotFound": "Sorry these credentials are incorrect",
		"otpVerificationRequired": "Please verify the otp first, to access this resource",
		"pageNotFound": "Incorrect url",
		"invalidEmailOrPass": "Sorry either your email or password is incorrect",
		"nothingToUpdate": "There is nothing to update",
		"ouserNotAvail": "Sorry, this user is currently not available",
		"FailedAction": "Failed action",
		"FileUploadError": "not uploaded, Please try again"
	},
	"pushMessages": {
		"referalOldPush": "You received {{coins}} coins from your referal",
		"ChalCancelledOuserNotAvailMsg": "Sorry, your challenge has been cancelled for non availability of other user",
		"UserChalStaredMsg": "Your challenge has started",
		"AdminChalStaredMsg": "Your challenge {{startTitle}} has started",
		"AdminChalEndNoResultMsg": "Your challenge {{startTitle}} has ended with no result",
		"UserChalEndNoResultMsg": "You have lost the challenge against {{name}}.", 
		//"Your challenge with {{name}} has ended with no result",
		"UserChalWonMsg": "You have won the challenge with {{name}}",
		"UserChalLostMsg": "You have lost the challenge with {{name}}",
		"AdminChalWonMsg": "You have won the challenge {{startTitle}}"
	}
};
exports.globalMessages = globalMessages;

let staticValues = {
	"user": {
		"defaultYellowCoins": 99999999,
		"defaultAvail": "1",
		"defaultTimezone": "Asia/Calcutta"
	}
};
exports.staticValues = staticValues;

// ///	Friend Request Type
const notificationTypes = {
	"fReqSent": "FReqSent",
	"fReqAccepted": "FReqAccepted",
	"chalInvite": "ChalInvite",
	"chalAccepted": "ChalAccepted",
	"chalRejected": "ChalRejected",
	"adminNot": "AdminNot",
	"adminChalInvite": "AChalInvite",

	"referalOld": "ReferalOld",

	"sysChalCancelled": "SysChalCancelled",
	"sysChalStarted": "ChalStarted",
	"chalEndNoResult": "ChalEndNoResult",

	"chalWon": "ChalWon",
	"chalLost": "ChalLost"
};
exports.notificationTypes = notificationTypes;

const notificationCreatorTypes = {
	"user": "User",
	"admin": "Admin",
	"system": "System"
};
exports.notificationCreatorTypes = notificationCreatorTypes;

//	///	Coins History Types
const coinsHistoryTypes = {
	"redeemed": "Redeemed",

	"challengeWon": "ChalWon",
	"challengeLost": "ChalLost",
	"challengeCreated": "ChalCreated",
	"chalCreatedRefund": "ChalCreatedRefund",
	"challengeAccepted": "ChalAccepted",

	"registered": "Registered",

	"referalNew": "ReferalNew",
	"referalOld": "ReferalOld",

	"sysChalCancelled": "sysChalCancelled",

	"adminUpdate": "AdminUpdate"
};
exports.coinsHistoryTypes = coinsHistoryTypes;

//	Challenge Users Statuses
const challengeUsersStatus = {
	"pending": "Pending",
	"accepted": "Accepted",
	"started": "Started",
	"won": "Won",
	"lost": "Lost",
	"noResult": "NoResult",
	"deleted": "Deleted",
	"rejected": "Rejected",

	"creatorDel": "CreatorDeleted",
	"creatorBlocked": "CreatorBlocked",
	"challengerDel": "challengerDeleted",
	"challengerBlocked": "ChallengerBlocked"
};
exports.challengeUsersStatus = challengeUsersStatus

const defaultValues = {
	pageLimit: 10,
	pageSize: 10,
	mysqlDtFormat: "YYYY-MM-DD hh:mm:ss"
};
exports.defaultValues = defaultValues;