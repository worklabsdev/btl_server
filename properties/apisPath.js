const basePaths = {
	"common": "/",
	"v1": "/api/v1",
	"v1User": "/api/v1/user"
};
exports.basePaths = basePaths;

const paths = {
	"v1": {
		"userAuth": {
			"signup": `${basePaths.v1}`
		}
	},
	"commonPaths": {
		"successPath": `${basePaths.common}success`,
		"errorPath": `${basePaths.common}error`
	}
};
exports.paths = paths;