const responseHandler = require("../../../commonFuncs/responseHandler");

const { successMsg, errorMsg } = require("../../../properties/constant").globalMessages.responseMessages;

let { appConstants } = require("../../../properties/constant");

// let constants = require("./constants");
// let utils = require("./utils");

//	///	Home
const home = async (request, response) => {
	try {
		let ouser = await services.friendDetails(response, { userId: request.body.ouserId });

		return responseHandler.successHandler(response, null, { ouser });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.home = home;

//	///	Success View Return
exports.successView = (request, response) => {
	let message = response.trans(request.query.message || successMsg);

	return response.render('Common/success',{ message, appName: appConstants.fullName });
};

//	///	Error View Return
exports.errorView = (request, response) => {
	let message = response.trans(request.query.message || errorMsg);

	return response.render('Common/error',{ message, appName: appConstants.fullName });
};