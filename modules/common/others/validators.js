const Joi = require("@hapi/joi");

const apiRef = "otherValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

// ///		Other User Profile
exports.home = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/home`;

	let schema = Joi.object().keys({
		"ouserId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};
