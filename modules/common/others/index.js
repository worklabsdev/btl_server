var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

//router.post("/profile", verifyJWTMiddleware, validators.ouserProfile, controllers.ouserProfile);

router.get("/success", controllers.successView);

router.get("/error", controllers.errorView);

module.exports = router;
