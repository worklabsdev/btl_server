const Db = require("../../../models");

const { infoLogger, errorLogger } = require("../../../packages/logging/consoleLogger");

let cronLogConfig = appConfig.errorSettings.log.cron;

//  Cron Error Handler
const cronLogHandler = (data, odata) => {

    if(data.error && cronLogConfig.error) {
        errorLogger(odata.reference, odata.requestId, data.error, data);
    }
    else if(cronLogConfig.info) {
        infoLogger(odata.reference, odata.requestId, data);
    }

    return Db.crons.create(data);

};
exports.cronLogHandler = cronLogHandler;