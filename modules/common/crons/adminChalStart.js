const Db = require("../../../models");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { i18n } = require("../../../packages/i18n");
const { sendPushNotification } = require("../../../packages/fcm");

const { generateRequestId } = require("../../../commonFuncs/commonFuncs");
const { currentUTC } = require("../../../commonFuncs/momentFuncs");

const { commonSerAttributes } = require("../../../services/commonServices");
const { cronTypes } = require("../../../services/cronServices");
const { updateChallengeUsers } = require("../../../services/challengeUsers");


const { challengeUsersStatus, notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");
const { pushMessages } = require("../../../properties/constant").globalMessages;

const { cronLogHandler } = require("./commonFuncs");

const type = cronTypes.adminChalStart;
let reference = `Cron/${type}`;

let requestId = generateRequestId();
let odata = {}

const adminChalStartCron = async () => {
try{

    return Db.challenges.findAll({
        where: Sequelize.literal(`(challenges.userId=0) AND (challenges.deleted="0") AND (challenges.blocked="0") AND (challenges.startDt <= now()) AND (challenges.endDt > now())`),
        attributes: {
            exclude: ["directions"]
        },
        include: [
            {
                model: Db.challengeUsers,
                as: "challengeUsers",
                where: Sequelize.literal(`(challengeUsers.status = "${challengeUsersStatus.accepted}")`),
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        required: true,
                        where: {
                            deleted: "0",
                            blocked: "0"
                        },
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            }
        ],
        //logging: console.log
    }).then((challenges) => {

        challenges = JSON.parse(JSON.stringify(challenges));

        challenges.forEach((challenge, key) => {

            adminChalUserStartHandler(challenge, {
                requestId
            });

        });

        return challenges;

    });

}
catch(error) {

    cronLogHandler({
        reference: `${reference}/adminChalStartCron/Error`,
        requestId,
        action: {},
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference,
        requestId
    });

}
};
exports.adminChalStartCron = adminChalStartCron;

//  Admin Challenge User Start
const adminChalUserStartHandler = async (chal, odata) => {
let ref = `${reference}/adminChalUserStartHandler`;
try {
    let challenge = JSON.parse(JSON.stringify(chal));

    let chalUserStatus = challengeUsersStatus.started;

    let pushType = notificationTypes.sysChalStarted;
    let pushMessage = i18n.__(pushMessages.AdminChalStaredMsg, {
        startTitle: challenge.startTitle
    });

    let utcDt = currentUTC();

    let notificationArray = [];
    let userIdsArray = [];
    let chalUserIdsArray = [];

    //  Push Notification Send
    challenge.challengeUsers.forEach((challengeUser, index) => {

        chalUserIdsArray.push(challengeUser.challengeUserId);
        userIdsArray.push(challengeUser.userId);

        notificationArray.push({
            userId: challengeUser.userId,
            ouserId: 0,
            adminId: 1,
            challengeId: challenge.challengeId,
            message: pushMessage,
            type: pushType,
            creator: notificationCreatorTypes.system
        });

    });
    //  Push Notification Send

    let promiseAll = await Promise.all([

        Db.userNotifications.bulkCreate(notificationArray),// User Notifications Create

        updateChallengeUsers(// Challenges User Update
            { 
                challengeUserId: {
                    [Op.in]: chalUserIdsArray
                } 
            },
            { 
                status: chalUserStatus,
                startedAt: utcDt
            }
        ),// Challenges User Update

        Db.users.update(//  Users Challenges Count Increment
            {
                challengesPlayed: Sequelize.literal(`challengesPlayed + 1`)
            },
            {
                where: {
                    userId: {
                        [Op.in]: userIdsArray
                    }
                }
            }
        )//  Users Challenges Count Increment

    ]);
    promiseAll = JSON.parse(JSON.stringify(promiseAll));

    challenge.challengeUsers.forEach((challengeUser, key) => {

        //  Getting Specific User Notification Id
        let userNotificationId = "0";
        if(promiseAll[0][key] && promiseAll[0][key].userNotificationId)
            userNotificationId = promiseAll[0][key].userNotificationId;
        //  Getting Specific User Notification Id

        //  Challenger Push
        sendPushNotification(
            challengeUser.user.fcmId,
            {
                message: pushMessage,
                userNotificationId: `${userNotificationId}`,
                pushType: pushType,
                challengeId: `${challenge.challengeId}`
            },
            {
                requestId: requestId,
                deviceOS: challengeUser.user.deviceOS || null
            }
        );
        //  Challenger Push

    });

    cronLogHandler({
        reference: `${ref}/Success`,
        requestId,
        challengeId: challenge.challengeId,
        action: {
            challenge: chal,
            promiseAll
        },
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
catch(error) {

    cronLogHandler({
        reference: `${ref}/Error`,
        requestId,
        challengeId: chal.challengeId,
        action: {
            challenge: chal
        },
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
};