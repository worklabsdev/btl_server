const Db = require("../../../models");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { i18n } = require("../../../packages/i18n");
const { sendPushNotification } = require("../../../packages/fcm/");

const { generateRequestId } = require("../../../commonFuncs/commonFuncs");

const { cronTypes } = require("../../../services/cronServices");
const { challengeUsersTypes, updateChallengeUsers } = require("../../../services/challengeUsers");
const { commonSerAttributes } = require("../../../services/commonServices");

const { coinsHistoryTypes, challengeUsersStatus, notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");
const { pushMessages } = require("../../../properties/constant").globalMessages;

const { cronLogHandler } = require("./commonFuncs");

const type = cronTypes.userChalEnd;
let reference = `Cron/${type}`;

let requestId = generateRequestId();
let odata = {}


const userChalEndCron = async () => {
try{

    return Db.challenges.findAll({
        where: Sequelize.literal(`(challenges.adminId=0) AND (challenges.deleted="0") AND (challenges.blocked="0") AND (challenges.endDt <= (now() + INTERVAL 3 MINUTE) )`),
        attributes: {
            exclude: ["directions"]
        },
        include: [
            {
                model: Db.challengeUsers,
                as: "chalUser",
                where: Sequelize.literal(`(chalUser.userType = "${challengeUsersTypes.creator}")`),
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            },
            {
                model: Db.challengeUsers,
                as: "chalOuser",
                where: Sequelize.literal(`(chalOuser.userType = "${challengeUsersTypes.challenger}") AND (chalOuser.status = "${challengeUsersStatus.started}")`),
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            }
        ],
        //logging: console.log
    }).then((challenges) => {

        challenges = JSON.parse(JSON.stringify(challenges));

        challenges.forEach((challenge, key) => {

            chalEndHandler(challenge, {
                requestId
            });

        });

        return challenges;

    });

}
catch(error) {

    cronLogHandler({
        reference: `${reference}/userChalEndCron/Error`,
        requestId,
        action: {},
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference,
        requestId
    });

}
};
exports.userChalEndCron = userChalEndCron;

//      Single Challenge End Handler
const chalEndHandler = async (chal, odata) => {
let ref = `${reference}/chalEndHandler`;
try {
    let challenge = JSON.parse(JSON.stringify(chal));

    let creator = JSON.parse(JSON.stringify(challenge.chalUser.user));
    let challenger = JSON.parse(JSON.stringify(challenge.chalOuser.user));
    delete challenge.chalUser;
    delete challenge.chalOuser;

    let chalUserStatus = challengeUsersStatus.noResult;

    //  Push Type And Messages
    let pushType = notificationTypes.chalEndNoResult;    
    let CreatorPushMessage = i18n.__(pushMessages.UserChalEndNoResultMsg, {
        name: challenger.name
    });
    let ChallengerPushMessage = i18n.__(pushMessages.UserChalEndNoResultMsg, {
        name: creator.name
    });

    // let AdminPushMessage = i18n.__(adminNotificationsSerMessages.UserChalStarted, {
    //     name: creator.name
    // });
    //  Push Type And Messages

    let promiseAll = await Promise.all([

        Db.userNotifications.bulkCreate([// User Notifiactions Create
            {
                userId: creator.userId,
                ouserId: challenger.userId,
                challengeId: challenge.challengeId,
                message: CreatorPushMessage,
                type: pushType,
                creator: notificationCreatorTypes.system                
            },
            {
                userId: challenger.userId,
                ouserId: creator.userId,
                challengeId: challenge.challengeId,
                message: ChallengerPushMessage,
                type: pushType,
                creator: notificationCreatorTypes.system                
            }
        ]),// User Notifiactions Create

        updateChallengeUsers(// Challenge User Update
            { challengeId: challenge.challengeId },
            { status: chalUserStatus }
        ),// Challenge User Update

        // Db.adminNotifications.create({
        //     userId: creator.userId,
        //     challengeId: challenge.challengeId,
        //     message: AdminPushMessage,
        //     type: data.notificationType  
        // })

    ]);
    promiseAll = JSON.parse(JSON.stringify(promiseAll));

    //  Creator Push
    sendPushNotification(
        creator.fcmId,
        {
            message: CreatorPushMessage,
            userNotificationId: `${promiseAll[0][0].userNotificationId}`,
            pushType: pushType,
            challengeId: `${challenge.challengeId}`,
            userId: `${challenger.userId}`
        },
        {
            requestId: requestId,
            deviceOS: creator.deviceOS || null
        }
    );
    //  Creator Push

    //  Challenger Push
    sendPushNotification(
        challenger.fcmId,
        {
            message: ChallengerPushMessage,
            userNotificationId: `${promiseAll[0][1].userNotificationId}`,
            pushType: pushType,
            challengeId: `${challenge.challengeId}`,
            userId: `${creator.userId}`
        },
        {
            requestId: requestId,
            deviceOS: challenger.deviceOS || null
        }
    );
    //  Challenger Push

    cronLogHandler({
        reference: `${ref}/Success`,
        requestId,
        challengeId: challenge.challengeId,
        action: {
            challenge,
            promiseAll
        },
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
catch(error) {

    cronLogHandler({
        reference: `${ref}/Error`,
        requestId,
        challengeId: chal.challengeId,
        action: {
            challenge
        },
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
};