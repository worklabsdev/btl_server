var schedule = require('node-schedule');

const { userChalStartCron } = require("./userChalStart");
const { userChalEndCron } = require("./userChalEnd");
const { adminChalStartCron } = require("./adminChalStart");
const { adminChalEndCron } = require("./adminChalEnd");

//console.log('Before Cron Job Setup');
schedule.scheduleJob('*/10 * * * *', async () => {

    console.log('User Challenge Start Cron:', new Date());
    await userChalStartCron();

    console.log('User Challenge End Cron:', new Date());
    await userChalEndCron();

    console.log('Admin Challenge Start Cron:', new Date());
    await adminChalStartCron();

    console.log('Admin Challenge End Cron:', new Date());
    await adminChalEndCron();

});