const Db = require("../../../models");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { i18n } = require("../../../packages/i18n");
const { sendPushNotification } = require("../../../packages/fcm");

const { generateRequestId } = require("../../../commonFuncs/commonFuncs");
const { currentUTC } = require("../../../commonFuncs/momentFuncs");

const { cronTypes } = require("../../../services/cronServices");
const { challengeUsersTypes, updateChallengeUsers } = require("../../../services/challengeUsers");
const { commonSerAttributes } = require("../../../services/commonServices");

const { coinsHistoryTypes, challengeUsersStatus, notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");
const { pushMessages } = require("../../../properties/constant").globalMessages;

const { cronLogHandler } = require("./commonFuncs");

const type = cronTypes.userChalStart;
let reference = `Cron/${type}`;

let requestId = generateRequestId();
let odata = {}

const userChalStartCron = async () => {
try{

    return Db.challenges.findAll({
        where: Sequelize.literal(`(challenges.adminId=0) AND (challenges.deleted="0") AND (challenges.blocked="0") AND (challenges.startDt <= now()) AND (challenges.endDt > now())`),
        attributes: {
            exclude: ["directions"]
        },
        include: [
            {
                model: Db.challengeUsers,
                as: "chalUser",
                where: Sequelize.literal(`(chalUser.userType = "${challengeUsersTypes.creator}")`),
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            },
            {
                model: Db.challengeUsers,
                as: "chalOuser",
                where: Sequelize.literal(`(chalOuser.userType = "${challengeUsersTypes.challenger}") AND (chalOuser.status = "${challengeUsersStatus.accepted}")`),
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            }
        ],
        //logging: console.log
    }).then((challenges) => {

        challenges = JSON.parse(JSON.stringify(challenges));

        challenges.forEach((challenge, key) => {

            chalStartHandler(challenge, {
                requestId
            });

        });

        return challenges;

    });

}
catch(error) {

    cronLogHandler({
        reference: `${reference}/userChalStartCron/Error`,
        requestId,
        action: {},
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference,
        requestId
    });

}
};
exports.userChalStartCron = userChalStartCron;


//      Single Challenge Start Handler
const chalStartHandler = async (chal, odata) => {
let ref = `${reference}/chalStartHandler`;
try {

    let challenge = JSON.parse(JSON.stringify(chal));

    //  User or Ouser Block Case
    if(
        (challenge.chalUser.user.blocked === "1") ||
        (challenge.chalUser.user.deleted === "1") ||
        (challenge.chalOuser.user.blocked === "1") ||
        (challenge.chalOuser.user.deleted === "1")
    ) {
        userBlockedHandler(challenge, odata);
        return;
    }
    //  User or Ouser Block Case

    let creator = JSON.parse(JSON.stringify(challenge.chalUser.user));
    let challenger = JSON.parse(JSON.stringify(challenge.chalOuser.user));
    delete challenge.chalUser;
    delete challenge.chalOuser;

    let chalUserStatus = challengeUsersStatus.started;

    let pushType = notificationTypes.sysChalStarted;
    let pushMessage = i18n.__(pushMessages.UserChalStaredMsg);

    let utcDt = currentUTC();
 
    let promiseAll = await Promise.all([

        Db.userNotifications.bulkCreate([// User Notifiactions Create
            {
                userId: creator.userId,
                ouserId: challenger.userId,
                challengeId: challenge.challengeId,
                message: pushMessage,
                type: pushType,
                creator: notificationCreatorTypes.system                
            },
            {
                userId: challenger.userId,
                ouserId: creator.userId,
                challengeId: challenge.challengeId,
                message: pushMessage,
                type: pushType,
                creator: notificationCreatorTypes.system                
            }
        ]),// User Notifiactions Create

        updateChallengeUsers(// Challenge User Update
            { challengeId: challenge.challengeId },
            { 
                status: chalUserStatus,
                startedAt: utcDt
            }
        ),// Challenge User Update

        Db.users.update(//  User Challenges Count Increment
            {
                challengesPlayed: Sequelize.literal(`challengesPlayed + 1`)
            },
            {
                where: {
                    userId: {
                        [Op.in]: [creator.userId, challenger.userId]
                    }
                }
            }
        )//  User Challenges Count Increment
    ]);
    promiseAll = JSON.parse(JSON.stringify(promiseAll));

    //  Creator Push
    sendPushNotification(
        creator.fcmId,
        {
            message: pushMessage,
            userNotificationId: `${promiseAll[0][0].userNotificationId}`,
            pushType: pushType,
            challengeId: `${challenge.challengeId}`,
            userId: `${challenger.userId}`
        },
        {
            requestId: requestId,
            deviceOS: creator.deviceOS || null
        }
    );
    //  Creator Push

    //  Challenger Push
    sendPushNotification(
        challenger.fcmId,
        {
            message: pushMessage,
            userNotificationId: `${promiseAll[0][1].userNotificationId}`,
            pushType: pushType,
            challengeId: `${challenge.challengeId}`,
            userId: `${creator.userId}`
        },
        {
            requestId: requestId,
            deviceOS: challenger.deviceOS || null
        }
    );
    //  Challenger Push

    cronLogHandler({
        reference: `${ref}/Success`,
        requestId,
        challengeId: challenge.challengeId,
        action: {
            challenge: chal,
            promiseAll
        },
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
catch(error) {

    cronLogHandler({
        reference: `${ref}/Error`,
        requestId,
        challengeId: chal.challengeId,
        action: {
            challenge: chal
        },
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
};

//  User Blocked Handler 
const userBlockedHandler = async (chal, odata) => {
let ref = `${reference}/userBlockedHandler`;
try{
    
    let challenge = JSON.parse(JSON.stringify(chal));

    let creator = JSON.parse(JSON.stringify(challenge.chalUser.user));
    let challenger = JSON.parse(JSON.stringify(challenge.chalOuser.user));
    delete challenge.chalUser;
    delete challenge.chalOuser;

    //  Challenge User Status Update
    let chalUserStatus = null;
    if(creator.deleted === "1") {
        chalUserStatus = challengeUsersStatus.creatorDel;
    }
    else if(creator.blocked === "1") {
        chalUserStatus = challengeUsersStatus.creatorBlocked;
    }
    else if(challenger.deleted === "1") {
        chalUserStatus = challengeUsersStatus.challengerDel;
    }
    else {
        chalUserStatus = challengeUsersStatus.challengerBlocked;
    }
    //  CHallenge User Status Update

    let pushType = notificationTypes.sysChalCancelled;
    let pushMessage = i18n.__(pushMessages.ChalCancelledOuserNotAvailMsg);

    let promiseAll = await Promise.all([

        Db.userNotifications.bulkCreate([// User Notifiactions Create
            {
                userId: creator.userId,
                ouserId: challenger.userId,
                challengeId: challenge.challengeId,
                message: pushMessage,
                type: pushType,
                creator: notificationCreatorTypes.system                
            },
            {
                userId: challenger.userId,
                ouserId: creator.userId,
                challengeId: challenge.challengeId,
                message: pushMessage,
                type: pushType,
                creator: notificationCreatorTypes.system                
            }
        ]),// User Notifiactions Create

        Db.userCoinsHistories.bulkCreate([//   Users Coins History Insertion
            {
                userId: creator.userId,
                ouserId: challenger.userId,
                challengeId: challenge.challengeId,
                yellowCoins: challenge.coins,
                type: coinsHistoryTypes.sysChalCancelled
            },
            {
                userId: challenger.userId,
                ouserId: creator.userId,
                challengeId: challenge.challengeId,
                yellowCoins: challenge.coins,
                type: coinsHistoryTypes.sysChalCancelled
            }
        ]),//   Users Coins History Insertion

        updateChallengeUsers(// Challenge User Update
            { challengeId: challenge.challengeId },
            { status: chalUserStatus }
        ),// Challenge User Update

        Db.users.update(// User Coins Update
            {
                yellowCoins: Sequelize.literal(`yellowCoins + ${challenge.coins}`)
            },
            {
                where: {
                    userId: {
                        [Op.in]: [creator.userId, challenger.userId]
                    }
                },
                //logging: console.log
            }
        )// User Coins Update

    ]);
    promiseAll = JSON.parse(JSON.stringify(promiseAll));

    //  Creator Push
    sendPushNotification(
        creator.fcmId,
        {
            message: pushMessage,
            userNotificationId: `${promiseAll[0][0].userNotificationId}`,
            pushType: pushType,
            challengeId: `${challenge.challengeId}`,
            userId: `${challenger.userId}`
        },
        {
            requestId: requestId,
            deviceOS: creator.deviceOS || null
        }
    );
    //  Creator Push

    //  Challenger Push
    sendPushNotification(
        challenger.fcmId,
        {
            message: pushMessage,
            userNotificationId: `${promiseAll[0][1].userNotificationId}`,
            pushType: pushType,
            challengeId: `${challenge.challengeId}`,
            userId: `${creator.userId}`
        },
        {
            requestId: requestId,
            deviceOS: challenger.deviceOS || null
        }
    );
    //  Challenger Push

    cronLogHandler({
        reference: `${ref}/Success`,
        requestId,
        challengeId: challenge.challengeId,
        action: {
            challenge: chal,
            promiseAll
        },
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
catch(error) {

    cronLogHandler({
        reference: `${ref}/Error`,
        requestId,
        challengeId: chal.challengeId,
        action: {
            challenge: chal
        },
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
};