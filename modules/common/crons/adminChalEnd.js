const Db = require("../../../models");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { i18n } = require("../../../packages/i18n");
const { sendPushNotification } = require("../../../packages/fcm");

const { generateRequestId } = require("../../../commonFuncs/commonFuncs");
const { currentUTC } = require("../../../commonFuncs/momentFuncs");

const { commonSerAttributes } = require("../../../services/commonServices");
const { cronTypes } = require("../../../services/cronServices");
const { challengeUsersTypes, updateChallengeUsers } = require("../../../services/challengeUsers");


const { coinsHistoryTypes, challengeUsersStatus, notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");
const { pushMessages } = require("../../../properties/constant").globalMessages;

const { cronLogHandler } = require("./commonFuncs");

const type = cronTypes.adminChalEnd;
let reference = `Cron/${type}`;

let requestId = generateRequestId();
let odata = {}

const adminChalEndCron = async () => {
try{

    return Db.challenges.findAll({
        where: Sequelize.literal(`(challenges.userId=0) AND (challenges.deleted="0") AND (challenges.blocked="0") AND ( challenges.endDt <=  (now() + INTERVAL 3 MINUTE) )`),
        attributes: {
            exclude: ["directions"]
        },
        include: [
            {
                model: Db.challengeUsers,
                as: "challengeUsers",
                where: Sequelize.literal(`(challengeUsers.status = "${challengeUsersStatus.started}")`),
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        required: true,
                        where: {
                            deleted: "0",
                            blocked: "0"
                        },
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            }
        ],
        //logging: console.log
    }).then((challenges) => {

        challenges = JSON.parse(JSON.stringify(challenges));

        challenges.forEach((challenge, key) => {

            adminChalUserEndHandler(challenge, {
                requestId
            });

        });

        return challenges;

    });

}
catch(error) {

    cronLogHandler({
        reference: `${reference}/adminChalEndCron/Error`,
        requestId,
        action: {},
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference,
        requestId
    });

}
};
exports.adminChalEndCron = adminChalEndCron;

//  Admin Challenge User Start
const adminChalUserEndHandler = async (chal, odata) => {
let ref = `${reference}/adminChalUserEndHandler`;
try {
    let challenge = JSON.parse(JSON.stringify(chal));

    let chalUserStatus = challengeUsersStatus.noResult;

    let pushType = notificationTypes.chalEndNoResult;
    let pushMessage = i18n.__(pushMessages.AdminChalEndNoResultMsg, {
        startTitle: challenge.startTitle
    });

    let notificationArray = [];
    let chalUserIdsArray = [];    

    challenge.challengeUsers.forEach((challengeUser, index) => {

        chalUserIdsArray.push(challengeUser.challengeUserId);

        notificationArray.push({
            userId: challengeUser.userId,
            ouserId: 0,
            adminId: 1,
            challengeId: challenge.challengeId,
            message: pushMessage,
            type: pushType,
            creator: notificationCreatorTypes.system
        });

    });

    let promiseAll = await Promise.all([

        Db.userNotifications.bulkCreate(notificationArray),// User Notifications Create

        updateChallengeUsers(// Challenges User Update
            { 
                challengeUserId: {
                    [Op.in]: chalUserIdsArray
                } 
            },
            { 
                status: chalUserStatus
            }
        ),// Challenges User Update

    ]);
    promiseAll = JSON.parse(JSON.stringify(promiseAll));

    //  Push Notification Send
    challenge.challengeUsers.forEach((challengeUser, key) => {

        //  Getting Specific User Notification Id
        let userNotificationId = "0";
        if(promiseAll[0][key] && promiseAll[0][key].userNotificationId)
            userNotificationId = promiseAll[0][key].userNotificationId;
        //  Getting Specific User Notification Id

        sendPushNotification(
            challengeUser.user.fcmId,
            {
                message: pushMessage,
                userNotificationId: `${userNotificationId}`,
                pushType: pushType,
                challengeId: `${challenge.challengeId}`
            },
            {
                requestId: requestId,
                deviceOS: challengeUser.user.deviceOS || null
            }
        );

    });
    //  Push Notification Send


    cronLogHandler({
        reference: `${ref}/Success`,
        requestId,
        challengeId: challenge.challengeId,
        action: {
            challenge: chal,
            promiseAll
        },
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
catch(error) {

    cronLogHandler({
        reference: `${ref}/Error`,
        requestId,
        challengeId: chal.challengeId,
        action: {
            challenge: chal
        },
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        odata: odata
    }, {
        reference: ref,
        requestId
    });

}
};