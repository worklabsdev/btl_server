const Db = require("../../../models");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { i18n } = require("../../../packages/i18n");
const { sendPushNotification } = require("../../../packages/fcm");

const { updateChallengeUsers, challengeUserStatuses } = require("../../../services/challengeUsers");
const { cronTypes } = require("../../../services/cronServices");
const { commonSerAttributes, waitTimeIncvalue, totalDistsStepsIncFunc } = require("../../../services/commonServices");

const { cronLogHandler } = require("./commonFuncs");

const { coinsHistoryTypes, challengeUsersStatus, notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");
const { pushMessages } = require("../../../properties/constant").globalMessages;

const type = cronTypes.userOngoingChalSync;
let reference = `Cron/${type}`;

const { generateRequestId } = require("../../../commonFuncs/commonFuncs");

let requestId = generateRequestId();
let odata = {};

//      Called Whenever New Data is synced from devices
const newOngoingChalSync = async (data) => {
let ref = `${reference}/ongoingChalSync`;
try {

    return Db.challenges.findAll({
        // where: Sequelize.literal(`(challenges.deleted="0") AND (challenges.blocked="0") AND 
        //     (challenges.startDt <= "${data.dt}") AND (challenges.endDt > "${data.dt}")
        // `),
        where: Sequelize.literal(`(challenges.deleted="0") AND (challenges.blocked="0") AND 
        (challenges.startDt <= now()) AND (challenges.endDt > "${data.dt}")
    `),
        attributes: commonSerAttributes.chalCronSyncAtts,
        include: [
            {
                model: Db.users,
                as: "creator",
                required: false,
                attributes: commonSerAttributes.user4PushNotificationAtts
            },
            {
                model: Db.challengeUsers,
                as: "chalUser",
                attributes: commonSerAttributes.chalUserCronSyncAtts,
                where: Sequelize.literal(`(chalUser.userId=${data.userId}) AND (chalUser.status = "${challengeUsersStatus.started}")`),
                required: true,
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            },
            {
                model: Db.challengeUsers,
                as: "chalOuser",
                attributes: commonSerAttributes.chalUserCronSyncAtts,
                where: Sequelize.literal(`(chalOuser.userId != ${data.userId})`),
                required: false,
                include: [
                    {
                        model: Db.users,
                        as: "user",
                        attributes: commonSerAttributes.user4PushNotificationAtts
                    }
                ]
            },
            {
                model: Db.challengeSyncs,
                as: "challengeSync",
                attributes: commonSerAttributes.chalSyncsAtts,
                required: false,
                where: {
                    userDeviceSyncId: data.userDeviceSyncId
                }
            }
        ],
        group: ["challenges.challengeId"],
        logging: console.log
    })
    .then((challenges) => {

        let waitTime = waitTimeIncvalue;
        challenges = JSON.parse(JSON.stringify(challenges));

        challenges.forEach(async (challenge, key) => {

            challenge.chalDistSyncFunc = await chalDistSyncFunc(challenge, {
                requestId,
                ...data
            });

        });

        return challenges;

    });

}
catch(error) {

    cronLogHandler(
        {
            reference: `${ref}/Error`,
            requestId,
            challengeId: 0,
            action: {
                data
            },
            error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
            odata: odata
        },
        {
            reference: ref,
            requestId
        }
    );

}
};
exports.newOngoingChalSync = newOngoingChalSync;

//  Challenge Distance Sync
const chalDistSyncFunc = async (chal, odata) => {
if(odata.reference) {
    odata.reference = `${odata.reference}/chalDistSyncFunc`;
}
else {
    odata.reference = `${reference}/chalDistSyncFunc`;
}

if(!odata.requestId)
    odata.requestId = requestId;

try {
    let challenge = JSON.parse(JSON.stringify(chal));
    let promiseAll = [];

    if(!challenge.challengeSync) {// Inserting Challenge Sync

        promiseAll.push(
            Db.challengeSyncs.create({
                devicesWebhookId: odata.devicesWebhookId || 0,
                challengeId: challenge.challengeId,
                userDeviceSyncId: odata.userDeviceSyncId,
                distance: odata.incrementDistance,
                steps: odata.incrementSteps
            })
        );

    }// Inserting Challenge Sync
    else {// Challenge Sync Update
        promiseAll.push(
            Db.challengeSyncs.update(
                {
                    distance: Sequelize.literal(`distance + ${odata.incrementDistance}`),
                    steps: Sequelize.literal(`steps + ${odata.incrementSteps}`)
                },
                {
                    where: {
                        challengeSyncId: challenge.challengeSync.challengeSyncId
                    }
                }
            )
        );
    }// Challenge Sync Update

    //	Common Seq Statement
    let totalDistsStepsInc = totalDistsStepsIncFunc(incrementDistance, incrementSteps);
    //	Common Seq Statement

    promiseAll.push(
        updateChallengeUsers(
            { challengeUserId: challenge.chalUser.challengeUserId },
            {
                totalDistance: totalDistsStepsInc.totalDistance,
                //Sequelize.literal(`totalDistance + ${odata.incrementDistance}`),
                totalSteps: totalDistsStepsInc.totalSteps 
                //Sequelize.literal(`totalSteps + ${odata.incrementSteps}`)
            }
        )
    );
    await Promise.all(promiseAll);

    let userChalIncDists = challenge.chalUser.totalDistance + odata.incrementDistance;

    let challengeWon = 0;
    if(userChalIncDists >= challenge.totalDistance) {// Challenge Won

        challengeWon = 1;

        await chalWonProcedureFunc(
            challenge,
            odata
        );

    }// Challenge Won

    cronLogHandler(
        {
            reference: `${odata.reference}/Success`,
            requestId: odata.requestId,
            challengeId: challenge.challengeId,
            action: {
                challengeWon,
                challenge: chal,
                promiseAll
            },
            odata: odata
        },
        {
            reference: odata.reference,
            requestId: odata.requestId
        }
    );

} catch(error) {

    cronLogHandler(
        {
            reference: `${odata.reference}/Error`,
            requestId: odata.requestId,
            challengeId: chal.challengeId,
            action: {
                challenge: chal
            },
            error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
            odata: odata
        },
        {
            reference: odata.reference,
            requestId: odata.requestId
        }
    );

}
};

//  Challenge Won Module
const chalWonProcedureFunc = async (chal, odata) => {
//odata.reference = `${odata.reference}/chalWonProcedureFunc`;
try {

    let winner = chal.chalUser.user;
    let looser = {
        userId: 0,
        fcmId: null
    };
    if(chal.userId != 0) {//    User Challenge Case

        if(chal.userId === winner.userId) {
            looser = chal.chalOuser.user;
        }
        else {
            looser = chal.creator;
        }        

    }//    User Challenge Case

    //   Winner Update
    await updateChallengeUsers(
        {
            challengeId: chal.challengeId,
            userId: winner.userId
        },
        {
            status: challengeUserStatuses.won
        }
    );
    //   Winner Update

    //   Looser Update
    if(looser.userId) {

        await updateChallengeUsers(
            {
                challengeId: chal.challengeId,
                userId: looser.userId
            },
            {
                status: challengeUserStatuses.lost
            }
        );

    }
    //   Looser Update

    delete chal.chalUser;
    delete chal.challengeSync;

    //      //      Winner  Handler     //      //
    chalWinnerProcedureFunc(
        chal,
        winner,
        looser,
        odata
    );
    //      //      Winner  Handler     //      //

    //      //      Looser Handler      //      //
    if(looser.userId) {
        chalLooserProcedureFunc(
            chal,
            winner,
            looser,
            odata
        );
    }
    //      //      Looser Handler      //      //

} catch(error) {

    cronLogHandler(
        {
            reference: `${odata.reference}/Error`,
            requestId: odata.requestId,
            challengeId: chal.challengeId,
            action: {
                challenge: chal
            },
            error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
            odata: odata
        },
        {
            reference: odata.reference,
            requestId: odata.requestId
        }
    );

}
};

//      Challenge Winner Procedure
const chalWinnerProcedureFunc = async (chal, winner, looser, odata) => {
odata.reference = `${odata.reference}/chalWinnerProcedureFunc`;
try {
    let winnerPushMsg = null;
    let creator = null;
    let winnerPushType = notificationTypes.chalWon;

    let greenCoinsInc = 0;
    let yellowCoinsInc = 0;

    if(chal.userId) {
        winnerPushMsg = i18n.__(pushMessages.UserChalWonMsg, {
            name: looser.name
        });

        yellowCoinsInc = chal.coins;
        greenCoinsInc = chal.coins;
        creator = notificationCreatorTypes.user;
    }
    else {
        winnerPushMsg = i18n.__(pushMessages.AdminChalWonMsg, {
            startTitle: chal.startTitle
        });

        yellowCoinsInc = chal.coins;
        creator = notificationCreatorTypes.admin;
    }

    let promiseAll = await Promise.all([

        Db.users.update(//      Winner Update
            {
                yellowCoins: Sequelize.literal(`yellowCoins + ${yellowCoinsInc}`),
                greenCoins: Sequelize.literal(`greenCoins + ${greenCoinsInc}`),
                totalYellowCoins: Sequelize.literal(`totalYellowCoins + ${yellowCoinsInc}`),
                totalGreenCoins: Sequelize.literal(`totalGreenCoins + ${greenCoinsInc}`),
                challengesWon: Sequelize.literal(`challengesWon + 1`),
                challengesCompleted: Sequelize.literal(`challengesCompleted + 1`)
            },
            {
                where: { userId: winner.userId }
            }
        ),//      Winner Update

        Db.userCoinsHistories.create({//   Winner Coins History Insertion
            userId: winner.userId,
            ouserId: looser.userId,
            challengeId: chal.challengeId,
            yellowCoins: yellowCoinsInc,
            greenCoins: greenCoinsInc,
            type: coinsHistoryTypes.challengeWon
        }),//   Winner Coins History Insertion

        Db.userNotifications.create({//   Winner Notification Insertion
            userId: winner.userId,
            ouserId: looser.userId,
            adminId: chal.adminId,
            challengeId: chal.challengeId,
            deviceId: odata.deviceId || 0,
            userDeviceId: odata.userDeviceId || 0,
            userDeviceSyncId: odata.userDeviceSyncId || 0,
            message: winnerPushMsg,
            type: winnerPushType,
            creator: creator
        })//   Winner Notification Insertion

    ]);
    promiseAll = JSON.parse(JSON.stringify(promiseAll));

    //  Winner Push Notifications
    sendPushNotification(
        winner.fcmId,
        {
            message: winnerPushMsg,
            userNotificationId: `${promiseAll[2].userNotificationId}`,
            pushType: winnerPushType,
            challengeId: `${chal.challengeId}`,
            userId: `${looser.userId}`
        },
        {
            requestId: requestId,
            deviceOS: winner.deviceOS || null
        }
    );
    //  Winner Push Notifications

    cronLogHandler({
        reference: `${odata.reference}/Success`,
        requestId: odata.requestId,
        challengeId: chal.challengeId,
        action: {
            challenge: chal,
            winner,
            looser,
            promiseAll
        },
        odata: odata
    }, {
        reference: odata.reference,
        requestId: odata.requestId
    });
    

} catch(error) {

    cronLogHandler(
        {
            reference: `${ref}/Error`,
            requestId,
            challengeId: chal.challengeId,
            action: {
                challenge: chal,
                winner
            },
            error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
            odata: odata
        },
        {
            reference: ref,
            requestId
        }
    );

}
};

//      Challenge Looser Procedure
const chalLooserProcedureFunc = async (chal, winner, looser, odata) => {
odata.reference = `${odata.reference}/chalLooserProcedureFunc`;
try {

    let looserPushMsg = i18n.__(pushMessages.UserChalLostMsg, {
        name: winner.name
    });
    let looserPushType = notificationTypes.chalLost;

    let promiseAll = await Promise.all([

        Db.users.update(//      Looser Update
            {
                challengesCompleted: Sequelize.literal(`challengesCompleted + 1`)
            },
            {
                where: { userId: looser.userId }
            }
        ),//      Looser Update

        Db.userNotifications.create({//   Looser Notification Insertion
            userId: looser.userId,
            ouserId: winner.userId,
            adminId: 0,
            challengeId: chal.challengeId,
            message: looserPushMsg,
            type: looserPushType,
            creator: notificationCreatorTypes.user
        })//   Looser Notification Insertion

    ]);
    promiseAll = JSON.parse(JSON.stringify(promiseAll));

    //  Looser Push Notifications
    sendPushNotification(
        looser.fcmId,
        {
            message: looserPushMsg,
            userNotificationId: `${promiseAll[1].userNotificationId}`,
            pushType: looserPushType,
            challengeId: `${chal.challengeId}`,
            userId: `${looser.userId}`
        },
        {
            requestId: requestId,
            deviceOS: looser.deviceOS || null
        }
    );
    //  Looser Push Notifications


} catch(error) {

    cronLogHandler(
        {
            reference: `${odata.reference}/Error`,
            requestId: odata.requestId,
            challengeId: chal.challengeId,
            action: {
                challenge: chal,
                winner
            },
            error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
            odata: odata
        },
        {
            reference: odata.reference,
            requestId: odata.requestId
        }
    );

}
};