var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware, userAuthView } = require("../../../middlewares/authMiddleware");

const validator = require("./validators");
const controllers = require("./controllers");

router.get("/auth/:accessToken", validator.auth, userAuthView, controllers.auth);

router.get("/redirect", validator.redirect, controllers.redirect);

router.post("/unsync", verifyJWTMiddleware, validator.unsync, controllers.unsync);

router.all("/webhook", validator.webhook, controllers.webhook);

module.exports = router;
