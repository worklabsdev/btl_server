const fitbitConstants = {
    deviceId: 6
};
exports.fitbitConstants = fitbitConstants;

const fitbitAPIURL = {
    profileGet: "https://api.fitbit.com/1/user/-/profile.json",

    activitySubsCreate: "https://api.fitbit.com/1/user/-/activities/apiSubscriptions",
    activitySubsList: "https://api.fitbit.com/1/user/-/activities/apiSubscriptions.json",
    activitySubDelete: "https://api.fitbit.com/1/user/-/activities/apiSubscriptions",

    activityGet: "https://api.fitbit.com/1/user/-/activities/date",
    activityGetToday: "https://api.fitbit.com/1/user/-/activities/date/today.json"
};
exports.fitbitAPIURL = fitbitAPIURL;