var Sequelize = require("sequelize");

const Db = require("../../../models");

const responseHandler = require("../../../commonFuncs/responseHandler");

const { statusCodes } = require("../../../properties/constant");
//const { slugGenerator } = require("../../../commonFuncs/commonFuncs");
const { currentUTCGenerate } = require("../../../commonFuncs/momentFuncs");

//let services = require("./services");
let constants = require("./constants");
let utils = require("./utils");

const { deviceSerGetOne, deviceSerMessages } = require("../../../services/devices");
const { userDeviceGetOne, userDeviceUpdate, userDeviceCreate, userDeviceDetails4Push, userDevicesSerMessages } = require("../../../services/userDevices");

//	///	Auth Url Generate
const auth = async (request, response) => {
try {

	let generateAuthURL = utils.generateAuthURL(request.userDetails.userId);

	return response.redirect(generateAuthURL);
	// let result = {
	// 	userDetails: request.userDetails,
	// 	generateAuthURL,
	// 	fitbitConfig: utils.fitbitConfig
	// };
	// return responseHandler.successHandlerView(response, null, result);

} catch (error) {
	return responseHandler.errorHandlerView(response, error, { data: request.body });
}
};
exports.auth = auth;

//	///	Redirect Auth
const redirect = async (request, response) => {
let asyncParallel;
try {

	if(request.body.error_description) {
		return responseHandler.errorHandlerView(
			response,
			new Error(request.body.error_description)
		);
	}

	let fitbitApiResult = await utils.generateAccessToken({
		code: request.body.code,
		state: request.body.state,
		apiRefFull: request.apiRefFull,
		requestId: request.requestId
	});
	fitbitApiResult = JSON.parse(JSON.stringify(fitbitApiResult));

	if(fitbitApiResult.error) {//	Error Handling
		return utils.fitbitApiErrorHandler(response, fitbitApiResult.error.errors);
	}//	Error Handling

	let fitbitApiResponse = fitbitApiResult.response;

	//	Remove Old Fitbit Accounts
	await Db.userDevices.update(
		{
			accessToken: null,
			refreshToken: null,
			deleted: "1",
			isValid: "0"
		},
		{
			where: Sequelize.literal(`(deleted="0") AND (isValid="1") AND (
				(deviceUserId = "${fitbitApiResponse.user_id}")
					OR
				(userId = ${request.body.state})
			)`)
		}
	)
	//	Remove Old Fitbit Accounts

	asyncParallel = await Promise.all([

		deviceSerGetOne({//	Fitbit Device Get
			deviceId: constants.fitbitConstants.deviceId
		}),//	Fitbit Device Get

		utils.fitbitUserApis({//	Fitbit Profile Get
			apiURL: constants.fitbitAPIURL.profileGet,
			access_token: fitbitApiResponse.access_token,
			apiRefFull: request.apiRefFull,
			requestId: request.requestId
		}),//	Fitbit Profile Get

		utils.fitbitUserApis({//	Fitbit Subscription Get
			apiURL: constants.fitbitAPIURL.activitySubsList,
			access_token: fitbitApiResponse.access_token,
			apiRefFull: request.apiRefFull,
			requestId: request.requestId
		}),//	Fitbit Subscription Get

		userDeviceGetOne({
			userId: request.body.state,
			deviceId: constants.fitbitConstants.deviceId
		})

	]);
	asyncParallel = JSON.parse(JSON.stringify(asyncParallel));

//	Error Handling
	if(!asyncParallel[0]) {//	Device Not Available
		return responseHandler.errorHandlerView(
			response,
			new Error(deviceSerMessages.FitbitNotAvail)
		);
	}//	Device Not Available
	if(asyncParallel[1].error) {//	Fitbit Profile Get Error
		return utils.fitbitApiErrorHandler(response, asyncParallel[1].error.errors);
	}//	Fitbit Profile Get Error
	if(asyncParallel[2].error) {//	Subscription Get Error
		return utils.fitbitApiErrorHandler(response, asyncParallel[2].error.errors);
	}//	Subscription Get Error
//	Error Handling

	let profileData = asyncParallel[1].response.user;
	let fitbitSubsLen = asyncParallel[2].response.apiSubscriptions.length;

	//		Creating Fitbit Subscription If Not Present
	if(!fitbitSubsLen) {

		fitbitApiResponse.activitySubId = fitbitApiResponse.user_id;

		let apiURL = `${constants.fitbitAPIURL.activitySubsCreate}/${fitbitApiResponse.activitySubId}.json`;

		let fitbitSub = await utils.fitbitUserApis({
			apiURL: apiURL,
			access_token: fitbitApiResponse.access_token
		});

		if(fitbitSub.error) {
			return utils.fitbitApiErrorHandler(response, fitbitSub.error.errors);
		}

		asyncParallel[2] = fitbitSub.response;

	}
	else {
		fitbitApiResponse.activitySubId = asyncParallel[2].response.apiSubscriptions[0].subscriptionId;
	}
	//		Creating Fitbit Subscription If Not Present

	let dt = currentUTCGenerate("UTC", "YYYY-MM-DD");

	if(asyncParallel[3]) {//	Updating Old Record

		await userDeviceUpdate(
			{
				userDeviceId: asyncParallel[3].userDeviceId
			},
			{
				deviceUserId: fitbitApiResponse.user_id,
				accessToken: fitbitApiResponse.access_token,
				refreshToken: fitbitApiResponse.refresh_token,
				odata: fitbitApiResponse,
				profileData: profileData,
				isValid: "1",
				deleted: "0"
			}
		);

	}//	Updating Old Record
	else {//	Adding New Record

		asyncParallel[3] = await userDeviceCreate({
			userId: request.body.state,
			deviceId: asyncParallel[0].deviceId,
			deviceUserId: fitbitApiResponse.user_id,
			accessToken: fitbitApiResponse.access_token,
			refreshToken: fitbitApiResponse.refresh_token,
			odata: fitbitApiResponse,
			profileData: profileData,
			isValid: "1"
		});

	}//	Adding New Record

	//	Fitbit Device Sync Today
	utils.fitbitWebhookActivities(
		{
			0: {
				date: dt,
				ownerId: fitbitApiResponse.user_id,
				ownerType: "user",
				collectionType: "activities",
				subscriptionId: fitbitApiResponse.user_id
			}
		},
		{
			apiRefFull: request.apiRefFull,
			requestId: request.requestId,
			headers: request.headers
		}
	);
	//	Fitbit Device Sync Today


	return responseHandler.successHandlerView(response, deviceSerMessages.FitbitAuthSuccess);

} catch (error) {

	return responseHandler.errorHandlerView(response, error, { 
		data: request.body,
		apiRefFull: request.apiRefFull,
		requestId: request.requestId
	});

}
};
exports.redirect = redirect;

//	Webhook
const webhook = async (request, response) => {

	utils.fitbitWebhookActivities(request.body, {
		apiRefFull: request.apiRefFull,
		requestId: request.requestId,
		headers: request.headers
	});

	return response.status(statusCodes.actionComplete).json({});

};
exports.webhook = webhook;


//	Unsync
const unsync = async (request, response) => {
try {

	let userDevice = await userDeviceDetails4Push({
		userDeviceId: request.body.userDeviceId,
		userId: request.userDetails.userId
	});
	if(!userDevice || (userDevice.isValid === "0") || (userDevice.deleted === "1") ) {
		return responseHandler.failedActionHandler(response, userDevicesSerMessages.UserDeviceNotAvailMsg);
	}

	await userDeviceUpdate(
		{ userDeviceId: userDevice.userDeviceId },
		{
			accessToken: null,
			refreshToken: null,
			odata: {},
			deleted: "1",
			isValid: "0"
		}
	);

	return responseHandler.successHandler(response, null);

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.unsync = unsync;
