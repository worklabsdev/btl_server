const Joi = require("@hapi/joi");

const apiRef = "devices/fitbit";

const { joiValidate, joiValidateView } = require("../../../commonFuncs/validate");

// ///		Create Auth URL
exports.auth = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/auth`;

	request.body = {
		...request.query,
		...request.params
	};

	let schema = Joi.object().keys({
		"accessToken": Joi.string().required()
	});

	let valCheck = await joiValidateView(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///		Add Device
exports.redirect = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/redirect`;

	request.body = {
		...request.query
	};

	let schema = Joi.object().keys({
		"code": Joi.string().optional(),
		"state": Joi.number().min(1).optional(),

		"error_description": Joi.string().optional(),
		"error": Joi.string().optional()
	})
	.or("error_description", "code")
	.with("code", "state")
	.with("error_description", "error");

	let valCheck = await joiValidateView(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///		Fitbit Webhook
exports.webhook = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/webhook`;

	next();
	// request.body = {
	// 	...request.body,
	// 	...request.query,
	// 	...request.params
	// };

	// let schema = Joi.object().keys({
	// });
	// let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	// if (valCheck === true) next();

};

// ///		Fitbit Unsync
exports.unsync = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/unsync`;

	request.body = {
		...request.body,
		...request.query,
		...request.params
	};

	let schema = Joi.object().keys({
		"userDeviceId": Joi.number().min(1).required(),
	});
	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();

};