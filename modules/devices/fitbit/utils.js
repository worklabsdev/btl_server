var Sequelize = require("sequelize");

const Db = require("../../../models");

const { errorHandlerView } = require("../../../commonFuncs/responseHandler");
const { currentUTCGenerate } = require("../../../commonFuncs/momentFuncs");

const { statusCodes } = require("../../../properties/constant");
const { errorMsg } = require("../../../properties/constant").globalMessages.responseMessages;

const { requestPostMake } = require("../../../packages/requests");
const { infoLogger, errorLogger } = require("../../../packages/logging/consoleLogger");

const { devWebhooksSerCreate, devWebhookSerUpdate } = require("../../../services/devicesWebhooksServices");
const { userDeviceGetDetails4Webhook, userDeviceUpdate } = require("../../../services/userDevices");
const { userDeviceSyncCreate, userDeviceSyncUpdate } = require("../../../services/userDeviceSyncServices");
const { precisionDistValue, totalDistsStepsIncFunc, oldDistsStepsIncFunc } = require("../../../services/commonServices");

const { newOngoingChalSync } = require("../../common/crons/ongoingChalSync");

const { fitbitAPIURL, fitbitConstants } = require("./constants");

const fitbitConfig = appConfig.devices.fitbit;
exports.fitbitConfig = fitbitConfig;
//console.log(JSON.stringify(fitbitConfig, null, 2));

// const fitbitPermissions = "activity heartrate location nutrition profile settings sleep social weight";
const fitbitPermissions = "activity location profile";

//  Generate Fitbit Auth URL
const generateAuthURL = (state) => {

    //https://dev.fitbit.com/build/reference/web-api/explore/#/Subscriptions/getSubscriptionsList

    return `${fitbitConfig.authURL}?response_type=code&client_id=${fitbitConfig.clientId}&redirect_uri=${encodeURI(fitbitConfig.redirectURL)}&scope=${encodeURI(fitbitPermissions)}&expires_in=31536000&state=${state}`;

    //return `${fitbitConfig.authURL}?response_type=code&client_id=${fitbitConfig.clientId}&redirect_uri=${encodeURI(fitbitConfig.redirectURL)}&scope=${encodeURI(fitbitPermissions)}&expires_in=31536000&state=${state}&prompt=${encodeURI("login consent")}`;

};
exports.generateAuthURL = generateAuthURL;

//  Generate Access Token
const generateAccessToken = async (data) => {
    
    let options = {
        url: fitbitConfig.tokenURL,
        options: {
            headers: {
                "Authorization": `Basic ${Buffer.from(fitbitConfig.clientId+":"+fitbitConfig.clientSecret).toString("base64")}`,
                "Content-Type": "application/x-www-form-urlencoded"
            },
            params: {
                clientId: fitbitConfig.clientId,
                grant_type: "authorization_code",
                redirect_uri: fitbitConfig.redirectURL,
                code: data.code,
                state: data.state,
                expires_in: 31536000
            }
        }
    };

    infoLogger(data.apiRefFull, data.requestId, { options });

    return await requestPostMake(options);

};
exports.generateAccessToken = generateAccessToken;

//  Generate Access Token
const fitbitUserApis = async (data) => {

    let options = {
        url: data.apiURL,
        options: {
            headers: {
                "Authorization": `Bearer ${data.access_token}`,
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }
    };

    if(data.params)
        options.params = data.params;

    infoLogger(data.apiRefFull, data.requestId, { options });

    return await requestPostMake(options);

};
exports.fitbitUserApis = fitbitUserApis;

//  Error Handler
const fitbitApiErrorHandler = (response, errors=null, options) => {

    if(!errors || !errors.length) {
        return errorHandlerView(
            response,
            new Error(errorMsg),
            {
                errors,
                options
            }
        );
    }

    return errorHandlerView(
        response,
        new Error(errors[0].message),
        {
            errors,
            options
        }
    );

};
exports.fitbitApiErrorHandler = fitbitApiErrorHandler;

//  FitBit Token Expire Handler

//      Sync Activities Webhook Activities
const fitbitWebhookActivities = async (data, options) => {
try {
    let userDeviceSyncId = 0;

    options.deviceId = fitbitConstants.deviceId;

    infoLogger(options.apiRefFull, options.requestId, {
        data,
        headers: options.headers
    });

	let devWebhooks = await devWebhooksSerCreate({
		deviceId: options.deviceId,
        reference: options.apiRefFull,
        requestId: options.requestId,	
        data: data,
		response: {
			statusCode: statusCodes.actionComplete
		},
		odata: {
			headers: options.headers
		}
	});
	devWebhooks = JSON.parse(JSON.stringify(devWebhooks));
    data.devicesWebhookId = devWebhooks.devicesWebhookId;

    let dt = data[0].date;//userDeviceSyncs
    let userDeviceDetails = await userDeviceGetDetails4Webhook(
        {// User Device Where JSON
            isValid: "1",
            deleted: "0",
            deviceUserId: data[0].ownerId
        },// User Device Where JSON
        {// Options
            userDeviceSyncWhere: {//    User Device Sync Where JSON
                dt: dt,
                deleted: "0"
            }//    User Device Sync Where JSON
        }// Options
    );
    userDeviceDetails = JSON.parse(JSON.stringify(userDeviceDetails));

    if(!userDeviceDetails || !userDeviceDetails.accessToken) {//     Not Access Token
        infoLogger(`${options.apiRefFull}/NotActive`, options.requestId, {
            userDeviceDetails
        });

        return null;
    }//     Not Access Token

	//  ///		Fitbit Activity Details Get
	let apiURL = `${fitbitAPIURL.activityGet}/${dt}.json`;

	let fitbitActivityDetails = await fitbitUserApis({
		apiURL: apiURL,
        access_token: userDeviceDetails.accessToken,
        apiRefFull: options.apiRefFull,
        requestId: options.requestId
    });
    if(fitbitActivityDetails.error) {
        throw new Error(fitbitActivityDetails.error.errors[0].message);
    }

    let fitbitSummary = fitbitActivityDetails.response.summary;

    let oldDistance = oldSteps = totalSteps = totalDistance = incrementSteps = incrementDistance = 0;
    let promiseAllArray = [];

    if(fitbitSummary.steps) {
        totalSteps = fitbitSummary.steps;
    }
    if(fitbitSummary.distances) {
        let distancesObj = {};

        fitbitSummary.distances.forEach(distance => {
            distancesObj[distance.activity] = distance.distance;
        });

        totalDistance = distancesObj["total"];
    }
	//  ///		Fitbit Activity Details Get

    if(!userDeviceDetails.userDeviceSync) {// Db Insert Device Sync

        incrementDistance = oldDistance = (totalDistance).toFixed(4);
        incrementSteps = oldSteps = totalSteps;

        promiseAllArray.push(
            userDeviceSyncCreate({// Device Sync Create
                userDeviceId: userDeviceDetails.userDeviceId,
                userId: userDeviceDetails.userId,
                totalDistance: totalDistance,
                totalSteps: totalSteps,
                oldDistance: oldDistance,
                oldSteps: oldSteps,
                dt: dt
            })// Device Sync Create
        );

    }// Db Insert Device Sync
    else {// Db Update Device Sync

        userDeviceSyncId = userDeviceDetails.userDeviceSync.userDeviceSyncId;

        oldDistance = userDeviceDetails.userDeviceSync.totalDistance;
        oldSteps = userDeviceDetails.userDeviceSync.totalSteps;

        incrementSteps = totalSteps - oldSteps;
        incrementDistance = totalDistance - oldDistance;

        // incrementDistance = `${incrementDistance}`;
        // incrementDistance = incrementDistance.toFixed(precisionDistValue);
        // incrementDistance = parseFloat(incrementDistance);

        //	Common Seq Statement
        let totalDistsStepsInc = totalDistsStepsIncFunc(incrementDistance, incrementSteps);
        //	Common Seq Statement

        promiseAllArray.push(
            userDeviceSyncUpdate(// Device Sync Update
                { userDeviceSyncId: userDeviceDetails.userDeviceSync.userDeviceSyncId },
                {
                    oldDistance: totalDistance,//oldDistance,
                    oldSteps: totalSteps,//oldSteps,
                    totalDistance: totalDistsStepsInc.totalDistance, 
                    //Sequelize.literal(`totalDistance + ${incrementDistance}`),
                    totalSteps: totalDistsStepsInc.totalSteps
                    //Sequelize.literal(`totalSteps + ${incrementSteps}`)
                }
            )// Device Sync Update
        );

    }// Db Update Device Sync

    let lastSyncDt = currentUTCGenerate();

    //	Common Seq Statement
    let totalDistsStepsInc = totalDistsStepsIncFunc(incrementDistance, incrementSteps);
    //	Common Seq Statement

    promiseAllArray.push(
        userDeviceUpdate(// Device Update
            { userDeviceId: userDeviceDetails.userDeviceId },
            {
                totalDistance: totalDistsStepsInc.totalDistance, 
                //Sequelize.literal(`totalDistance + ${incrementDistance}`),
                totalSteps: totalDistsStepsInc.totalSteps
                //Sequelize.literal(`totalSteps + ${incrementSteps}`)
            }
        ),// Device Update

        Db.users.update(
            {
                totalDistance: totalDistsStepsInc.totalDistance,
                //Sequelize.literal(`totalDistance + ${incrementDistance}`),
                totalSteps: totalDistsStepsInc.totalSteps,
                //Sequelize.literal(`totalSteps + ${incrementSteps}`),
                lastSyncDt
            },
            {
                where: {
                    userId: userDeviceDetails.userId
                }
            }
        )    
    );

    await Promise.all(promiseAllArray);

    promiseAllArray = JSON.parse(JSON.stringify(promiseAllArray));
    if(promiseAllArray[0].fulfillmentValue && promiseAllArray[0].fulfillmentValue.userDeviceSyncId) {
        userDeviceSyncId = promiseAllArray[0].fulfillmentValue.userDeviceSyncId;
    }

    let result = {
        fitbitActivityDetails,
        promiseAllArray,
        odata: {
            totalDistance,
            totalSteps,
            oldDistance,
            oldSteps,
            incrementDistance,
            incrementSteps
        }
    };

    infoLogger(`${options.apiRefFull}/Success`, options.requestId, result);

    await devWebhookSerUpdate(//    WebHook Log Update
        { devicesWebhookId: data.devicesWebhookId },
        {
            actionResponse: {
                error: null,
                success: result
            }
        }
    );//    WebHook Log Update

    //	Ongoing Challenges Sync
	if(incrementSteps !== 0) {

        newOngoingChalSync({
            deviceId: options.deviceId,
            userDeviceId: userDeviceDetails.userDeviceId,
            userId: userDeviceDetails.userId,
            userDeviceSyncId: userDeviceSyncId,
            devicesWebhookId: data.devicesWebhookId,
            incrementSteps,
            incrementDistance,
            dt,
            requestId: options.requestId
        });

    }
    //	Ongoing Challenges Sync

	return null;

} catch(error) {

    if(data.devicesWebhookId) {//    WebHook Log Update
		await devWebhookSerUpdate(
			{ devicesWebhookId: data.devicesWebhookId },
			{
				actionResponse: {
					error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
					success: null
				}
			}
		);
    }//    WebHook Log Update

    errorLogger(`${options.apiRefFull}/Error`, options.requestId, error);

}
};
exports.fitbitWebhookActivities = fitbitWebhookActivities;