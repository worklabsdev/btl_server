let pathPrefix = "/devices";

app.use(`${pathPrefix}/fitbit/`, require("./fitbit"));
app.use(`${pathPrefix}/appleHealth/`, require("./appleHealth"));
app.use(`${pathPrefix}/googleFit/`, require("./googleFit"));
app.use(`${pathPrefix}/samsungHealth/`, require("./samsungHealth"));