var Sequelize = require("sequelize");

const Db = require("../../../models");

const { statusCodes } = require("../../../properties/constant");

const { currentUTCGenerate, defaultDateFormat, formatDtFunc } = require("../../../commonFuncs/momentFuncs");

const responseHandler = require("../../../commonFuncs/responseHandler");

const constants = require("./constants");
const utils = require("./utils");

const deviceId = constants.SamsungHealthConstants.deviceId;

const { commonSerAttributes, waitTimeIncvalue } = require("../../../services/commonServices");
const { userDeviceCreate, userDeviceUpdate, userDeviceDetails4SyncSerGet } = require("../../../services/userDevices");
const { devWebhooksSerCreate } = require("../../../services/devicesWebhooksServices");
const { userUpdateSer } = require("../../../services/userServices");

//	Sync Data
const syncData = async (request, response) => {
try {
	//[{"steps":84,"date":"2019-09-24","distance":77.03421020507812},{"steps":171,"date":"2019-09-25","distance":159.45661973953247}]

	//	Parent Webhook Creation
	let devWebhook = await devWebhooksSerCreate({
		deviceId: deviceId,
		reference: request.apiRefFull,
		requestId: request.requestId,
		data: request.body,
		response: {
			statusCode: statusCodes.actionPending
		},
		odata: {}
	});
	//	Parent Webhook Creation

	request.body.dtObjs = {};
	request.body.timezone = request.userDetails.timezone;
	request.body.data = JSON.parse(request.body.data);

	//	Other Device Sync Update	//
	await userDeviceUpdate(
		Sequelize.literal(`(userId = ${request.userDetails.userId}) AND (isValid="1") AND (deviceId != ${deviceId})`),
		{
			accessToken: null,
			refreshToken: null,
			odata: {},
			isValid: "0",
			deleted: "0"
		}
	);
	//	Other Device Sync Update	//

	//	User Device Insert or Update
	let userDevice = await userDeviceDetails4SyncSerGet({
		userId: request.userDetails.userId,
		deviceId: deviceId
	});
	if(!userDevice) {
		userDevice = await userDeviceCreate({
			userId: request.userDetails.userId,
			deviceId: deviceId,
			deviceUserId: null,
			accessToken: null,
			refreshToken: null,
			totalDistance: 0,
			totalSteps: 0,
			profileData: {},
			odata: {},
			isValid: "1"
		});
	} else {
		await userDeviceUpdate(
			{ userDeviceId: userDevice.userDeviceId },
			{
				deleted: "0",
				isValid: "1"
			}
		);
	}
	userDevice = JSON.parse(JSON.stringify(userDevice));
	//	User Device Insert or Update

	let lastSyncDt = currentUTCGenerate();
	//	Device History
	let deviceSyncsHistObjs = {};
	let deviceSyncsHist = await Db.userDeviceSyncs.findAll({
		where: Sequelize.literal(`(userDeviceId=${userDevice.userDeviceId}) AND (dt >= ${request.body.dt})`),
		attributes: commonSerAttributes.userDeviceSyncAtts,
		raw: true,
		//logging: console.log
	}).then((deviceSyncsHist) => {

		let deviceSyncsHistObj = {};

		deviceSyncsHist.forEach((deviceSync) => {
			deviceSyncsHistObj[deviceSync.dt] = deviceSync.userDeviceSyncId;

			deviceSync.totalDistance = parseFloat(deviceSync.totalDistance);
			deviceSync.totalSteps = parseFloat(deviceSync.totalSteps);

			deviceSyncsHistObjs[deviceSync.dt] = deviceSync;
		});

		return deviceSyncsHistObj;
	});
	//	Device History

	//	Distance Formatter
	request.body.data.forEach((distance, index) => {

		distance.distance = distance.distance/1000;// KM Conversion

		//let dt = distance.date;
		let dt = formatDtFunc(distance.date, defaultDateFormat);
		lastSyncDt = dt;//formatDtFunc(dt);

		if(request.body.dtObjs[dt] === undefined) {
			request.body.dtObjs[dt] = {
				distance: distance.distance,
				steps: distance.steps,
				userDeviceSyncId: 0,
				dt: dt
			};
		}
		else {
			request.body.dtObjs[dt].distance = request.body.dtObjs[dt].distance + distance.distance;
		}

		if(deviceSyncsHist[dt]) {
			request.body.dtObjs[dt].userDeviceSyncId = deviceSyncsHist[dt];
		}

	});
	//	Distance Formatter

	await userUpdateSer(
		{
			userId: request.userDetails.userId
		},
		{
			lastSyncDt
		}
	);

	//	ForEach Data Update Insert Sync deviceSyncsHistObjs
	let waitTime = 0;
	Object.keys(request.body.dtObjs).forEach( (key, index) => {
		var val = request.body.dtObjs[key];
		val.dt = key;

		setTimeout(() => {

			utils.samsungHealthWebhookActivities(val, {
				apiRefFull: request.apiRefFull,
				requestId: request.requestId,
				deviceId: deviceId,
				devicesWebhookId: devWebhook.devicesWebhookId,
				userId: request.userDetails.userId,
				userDeviceId: userDevice.userDeviceId,
				deviceSyncsHistObj: deviceSyncsHistObjs[key],
				waitTime,
				index
			});

		}, waitTime);

		waitTime = waitTime + waitTimeIncvalue;

	});
	//	ForEach Data Update Insert Sync

	// return response.json({
	// 	data: request.body,
	// 	devWebhook,
	// 	userDevice
	// });

	return responseHandler.successHandler(response, null, {
		lastSyncDt,
		//data: request.body
	});

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.syncData = syncData;
