const SamsungHealthConstants = {
    deviceId: 4//Samsung Health
};
exports.SamsungHealthConstants = SamsungHealthConstants;

const SamsungHealthMsgs = {
    "SamsungHealthOnlyAllowedMsg": "Only samsung health is allowed"
};
exports.SamsungHealthMsgs = SamsungHealthMsgs;