const BaseJoi = require("@hapi/joi");
const Extension = require("@hapi/joi-date");

const Joi = BaseJoi.extend(Extension);

const apiRef = "devices/samsungHealth";

const { joiValidate } = require("../../../commonFuncs/validate");

// ///		Sync Data
exports.syncData = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/syncData`;

	let schema = Joi.object().keys({
		"dt": Joi.date().format("YYYY-MM-DD").required(),
		"data": Joi.array().items(
			Joi.object().keys({
				"steps": Joi.number().min(0).required(),
				"date": Joi.string().required(),
				"distance": Joi.number().min(0).required()
			})
		).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};