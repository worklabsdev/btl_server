var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validator = require("./validators");
const controllers = require("./controllers");

router.post("/syncData", verifyJWTMiddleware, validator.syncData, controllers.syncData);

module.exports = router;
