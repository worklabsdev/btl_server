const BaseJoi = require("@hapi/joi");
const Extension = require("@hapi/joi-date");

const Joi = BaseJoi.extend(Extension);

const apiRef = "devices/appleHealth";

const { joiValidate } = require("../../../commonFuncs/validate");

// ///		Sync Data
exports.syncData = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/syncData`;

	request.body = {
		// ...request.query,
		// ...request.params,
		...request.body
	};

	let schema = Joi.object().keys({
		//"userDeviceId": Joi.number().min(0).required(),
		"dt": Joi.date().format("YYYY-MM-DD").required(),
		"steps": Joi.array().items(
			Joi.object().keys({
				"startDate": Joi.string().required(),
				"endDate": Joi.string().required(),
				"value": Joi.number().min(0).required()
			})
		).required(),

		"distance": Joi.array().items(
			Joi.object().keys({
				"startDate": Joi.string().required(),
				"endDate": Joi.string().required(),
				// "startDate": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
				// "endDate": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
				"value": Joi.number().min(0).required()
			})
		).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};