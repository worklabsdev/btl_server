var Sequelize = require("sequelize");

const Db = require("../../../models");

const { statusCodes } = require("../../../properties/constant");

const { currentUTCGenerate, defaultDateFormat, momentDtConvertToUTC } = require("../../../commonFuncs/momentFuncs");

const responseHandler = require("../../../commonFuncs/responseHandler");

const constants = require("./constants");
const utils = require("./utils");

const deviceId = constants.AppleHealthConstants.deviceId;

const { commonSerAttributes, waitTimeIncvalue } = require("../../../services/commonServices");
const { userDeviceCreate, userDeviceUpdate, userDeviceDetails4SyncSerGet } = require("../../../services/userDevices");
const { devWebhooksSerCreate } = require("../../../services/devicesWebhooksServices");
const { userUpdateSer } = require("../../../services/userServices");

//	Sync Data
const syncData = async (request, response) => {
try {
	// [{"value":24000.95, "endDate": "2019-09-20T00:00:00.000+0530", "startDate": "2019-09-19T00:00:00.000+0530"}, {"value":24000.95, "endDate": "2019-09-21T00:00:00.000+0530", "startDate": "2019-09-20T00:00:00.000+0530"}, {"value":24000.95, "endDate": "2019-09-22T00:00:00.000+0530", "startDate": "2019-09-21T00:00:00.000+0530"}]

	// [{"value": 2000, "endDate": "2019-09-20T00:00:00.000+0530", "startDate": "2019-09-19T00:00:00.000+0530"}]

	//	Parent Webhook Creation
	let devWebhook = await devWebhooksSerCreate({
		deviceId: deviceId,
		reference: request.apiRefFull,
		requestId: request.requestId,
		data: request.body,
		response: {
			statusCode: statusCodes.actionPending
		},
		odata: {}
	});
	//	Parent Webhook Creation

	request.body.dtObjs = {};
	request.body.timezone = request.userDetails.timezone;
	request.body.steps = JSON.parse(request.body.steps);
	request.body.distance = JSON.parse(request.body.distance);

	//	Other Device Sync Update	//
	await userDeviceUpdate(
		Sequelize.literal(`(userId = ${request.userDetails.userId}) AND (isValid="1") AND (deviceId != ${deviceId})`),
		{
			accessToken: null,
			refreshToken: null,
			odata: {},
			isValid: "0",
			deleted: "0"
		}
	);
	//	Other Device Sync Update	//

	//	User Device Insert or Update
	let userDevice = await userDeviceDetails4SyncSerGet({
		userId: request.userDetails.userId,
		deviceId: deviceId
	});
	if(!userDevice) {
		userDevice = await userDeviceCreate({
			userId: request.userDetails.userId,
			deviceId: deviceId,
			deviceUserId: null,
			accessToken: null,
			refreshToken: null,
			totalDistance: 0,
			totalSteps: 0,
			profileData: {},
			odata: {},
			isValid: "1"
		});
	} else {
		await userDeviceUpdate(
			{ userDeviceId: userDevice.userDeviceId },
			{
				deleted: "0",
				isValid: "1"
			}
		);
	}
	userDevice = JSON.parse(JSON.stringify(userDevice));
	//	User Device Insert or Update

	let lastSyncDt = currentUTCGenerate();
	//	Device History
	let deviceSyncsHistObjs = {};
	let deviceSyncsHist = await Db.userDeviceSyncs.findAll({
		where: Sequelize.literal(`(userDeviceId=${userDevice.userDeviceId}) AND (dt >= ${request.body.dt})`),
		attributes: commonSerAttributes.userDeviceSyncAtts,
		raw: true,
		//logging: console.log
	}).then((deviceSyncsHist) => {

		let deviceSyncsHistObj = {};

		deviceSyncsHist.forEach((deviceSync) => {
			deviceSyncsHistObj[deviceSync.dt] = deviceSync.userDeviceSyncId;

			deviceSync.totalDistance = parseFloat(deviceSync.totalDistance);
			deviceSync.totalSteps = parseFloat(deviceSync.totalSteps);

			deviceSyncsHistObjs[deviceSync.dt] = deviceSync;
		});

		return deviceSyncsHistObj;
	});
	//	Device History

	//	Steps Formatter
	request.body.steps.forEach((step) => {

		step.date = momentDtConvertToUTC(step.endDate, defaultDateFormat);

		if(request.body.dtObjs[step.date] === undefined) {
			request.body.dtObjs[step.date] = {
				steps: step.value,
				distance: 0,
				userDeviceSyncId: 0,
				dt: step.date
			};
		}
		else {
			request.body.dtObjs[step.date].steps = request.body.dtObjs[step.date].steps + step.value;
		}
	});
	//	Steps Formatter

	//	Distance Formatter
	request.body.distance.forEach((distance) => {

		let dt = momentDtConvertToUTC(distance.endDate, defaultDateFormat);

		lastSyncDt = momentDtConvertToUTC(distance.endDate);

		distance.value = distance.value/1000;// KM Conversion

		if(request.body.dtObjs[dt] === undefined) {
			request.body.dtObjs[dt] = {
				distance: distance.value,
				steps: 0,
				userDeviceSyncId: 0,
				dt: dt
			};
		}
		else {
			request.body.dtObjs[dt].distance = request.body.dtObjs[dt].distance + distance.value;
		}

		if(deviceSyncsHist[dt]) {
			request.body.dtObjs[dt].userDeviceSyncId = deviceSyncsHist[dt];
		}

	});
	//	Distance Formatter

	delete request.body.steps;
	delete request.body.distance;

	await userUpdateSer(
		{
			userId: request.userDetails.userId
		},
		{
			lastSyncDt
		}
	);

	//	ForEach Data Update Insert Sync deviceSyncsHistObjs
	let waitTime = 0;
	Object.keys(request.body.dtObjs).forEach( (key, index) => {
		var val = request.body.dtObjs[key];
		val.dt = key;

		setTimeout(() => {

			utils.appleHealthWebhookActivities(val, {
				apiRefFull: request.apiRefFull,
				requestId: request.requestId,
				deviceId: deviceId,
				devicesWebhookId: devWebhook.devicesWebhookId,
				userId: request.userDetails.userId,
				userDeviceId: userDevice.userDeviceId,
				deviceSyncsHistObj: deviceSyncsHistObjs[key],
				waitTime,
				index
			});

		}, waitTime);

		waitTime = waitTime + waitTimeIncvalue;

	});
	//	ForEach Data Update Insert Sync

	// return response.json({
	// 	data: request.body,
	// 	devWebhook,
	// 	userDevice
	// });

	return responseHandler.successHandler(response, null, {
		lastSyncDt
	});

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.syncData = syncData;
