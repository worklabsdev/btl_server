var Sequelize = require("sequelize");

const Db = require("../../../models");

const { statusCodes } = require("../../../properties/constant");

const { infoLogger, errorLogger } = require("../../../packages/logging/consoleLogger");

const { devWebhooksSerCreate } = require("../../../services/devicesWebhooksServices");
const { userDeviceSyncCreate, userDeviceSyncUpdate } = require("../../../services/userDeviceSyncServices");
const { userDeviceUpdate } = require("../../../services/userDevices");
const { precisionDistValue, totalDistsStepsIncFunc } = require("../../../services/commonServices");

const { newOngoingChalSync } = require("../../common/crons/ongoingChalSync");

//		Each Date Sync
const appleHealthWebhookActivities = async (data, options) => {
options.apiRefFull = `${options.apiRefFull}/appleHealthWebhookActivities`;
try {

	infoLogger(options.apiRefFull, options.requestId, {
		dt: new Date(),
		data,
        options
	});

	let userDeviceSyncId = 0;
    let oldDistance = oldSteps = totalSteps = totalDistance = incrementSteps = incrementDistance = 0;
	let promiseAllArray = [];
	if(data.userDeviceSyncId === 0) {//	Device Sync Create

		incrementDistance = oldDistance = (data.distance).toFixed(precisionDistValue);
		incrementDistance = parseFloat(incrementDistance);
		oldDistance = parseFloat(oldDistance);

        incrementSteps = oldSteps = data.steps;

		promiseAllArray.push(
			userDeviceSyncCreate({
				userDeviceId: options.userDeviceId,
				userId: options.userId,
				totalDistance: incrementDistance,
				totalSteps: incrementSteps,
				oldDistance: incrementDistance,
				oldSteps: incrementSteps,
				dt: data.dt
			}, 
			{
				//logging: console.log
			})
		);

	}//	Device Sync Create
	else {//	Device Sync Update

		userDeviceSyncId = data.userDeviceSyncId;

		oldDistance = options.deviceSyncsHistObj.totalDistance;
		oldSteps = options.deviceSyncsHistObj.totalSteps;

		totalDistance = parseFloat(data.distance.toFixed(precisionDistValue));
		totalSteps = parseInt(data.steps);

		incrementDistance = totalDistance - oldDistance;
		incrementSteps = totalSteps - oldSteps;

		//	Common Seq Statement
		let totalDistsStepsInc = totalDistsStepsIncFunc(incrementDistance, incrementSteps);
		//	Common Seq Statement

		promiseAllArray.push(
			userDeviceSyncUpdate(
				{ userDeviceSyncId: data.userDeviceSyncId },
				{
					oldDistance: totalDistance, 
					//Sequelize.literal(`oldDistance + ${incrementDistance}`),
					oldSteps: totalSteps,
					//Sequelize.literal(`oldSteps + ${incrementSteps}`),
					totalDistance: totalDistsStepsInc.totalDistance,
					//Sequelize.literal(`totalDistance + ${incrementDistance}`),
					totalSteps: totalDistsStepsInc.totalSteps
					//Sequelize.literal(`totalSteps + ${incrementSteps}`)
				},
				{
					//logging: console.log
				}
			)
		);

	}//	Device Sync Update

	//	Increment Steps > 0
	if(incrementSteps > 0) {

		let totalDistsStepsInc = totalDistsStepsIncFunc(incrementDistance, incrementSteps);

		promiseAllArray.push(
			userDeviceUpdate(// Device Update
				{ userDeviceId: options.userDeviceId },
				{
					totalDistance: totalDistsStepsInc.totalDistance, 
					//Sequelize.literal(`totalDistance + ${incrementDistance}`),
					totalSteps: totalDistsStepsInc.totalSteps
					//Sequelize.literal(`totalSteps + ${incrementSteps}`)
				}
			),// Device Update
			Db.users.update(//	User Update
				{
					totalDistance: totalDistsStepsInc.totalDistance, 
					//Sequelize.literal(`totalDistance + ${incrementDistance}`),
					totalSteps: totalDistsStepsInc.totalSteps 
					//Sequelize.literal(`totalSteps + ${incrementSteps}`)
				},
				{
					where: {
						userId: options.userId
					}
				}
			)//	User Update
		);

	}
    await Promise.all(promiseAllArray);

    promiseAllArray = JSON.parse(JSON.stringify(promiseAllArray));
    if(promiseAllArray[0].fulfillmentValue && promiseAllArray[0].fulfillmentValue.userDeviceSyncId) {
        userDeviceSyncId = promiseAllArray[0].fulfillmentValue.userDeviceSyncId;
    }

    let result = {
        promiseAllArray,
        odata: {
            totalDistance: data.distance,
            totalSteps: data.steps,
            oldDistance,
            oldSteps,
            incrementDistance,
            incrementSteps
        }
    };

	infoLogger(`${options.apiRefFull}/Success`, options.requestId, result);
	
	//    WebHook Success Creation
	let devicesWebhook = await devWebhooksSerCreate({
		deviceId: options.deviceId,
		reference: options.apiRefFull,
		requestId: options.requestId,
		devicesWebhookSubId: options.devicesWebhookId,
		data: {
			data
		},
		response: {
			statusCode: statusCodes.success
		},
		odata: {
			options,
			apiRefFull: `${options.apiRefFull}/success`,
			requestId: options.requestId
		},
		actionResponse: {
			error: null,
			success: result
		}
	});
	devicesWebhook = JSON.parse(JSON.stringify(devicesWebhook));
	//    WebHook Success Creation

	//	Ongoing Challenges Sync
	if(incrementSteps !== 0) {

		newOngoingChalSync({
			userId: options.userId,
			deviceId: options.deviceId,
			userDeviceId: options.userDeviceId,
			userDeviceSyncId: userDeviceSyncId,
			devicesWebhookId: devicesWebhook.devicesWebhookId,
			incrementSteps,
			incrementDistance,
			dt: data.dt,
			requestId: options.requestId 
		});

	}
	//	Ongoing Challenges Sync

	return result;

} catch(error) {

    errorLogger(options.apiRefFull, options.requestId, error);

	//    WebHook Error Creation
	await devWebhooksSerCreate({
		deviceId: options.deviceId,
		devicesWebhookSubId: options.devicesWebhookId,
		reference: options.apiRefFull,
		requestId: options.requestId,
		data: {
			data
		},
		response: {
			statusCode: statusCodes.error
		},
		odata: {
			options,
			apiRefFull: `${options.apiRefFull}/error`,
			requestId: options.requestId
		},
		actionResponse: {
			error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
			success: null
		}
	});
	//    WebHook Error Creation

}
};
exports.appleHealthWebhookActivities = appleHealthWebhookActivities;