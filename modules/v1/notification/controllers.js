const responseHandler = require("../../../commonFuncs/responseHandler");

const { userNotsListSer, userNotsCountSer, updateNotificationsSer } = require("../../../services/userNotifications");

let services = require("./services");
let constants = require("./constants");

//	///	Listing Screens
const notificationList = async (request, response) => {
try {

	request.body.userId = request.userDetails.userId;

	request.body.searchWhere = {
		deleted: "0",
		userId: request.body.userId
	};

	request.body.searchWhereUnreadCount = JSON.parse(JSON.stringify(request.body.searchWhere));
	request.body.searchWhereUnreadCount["isRead"] = "0";

	let promiseAll = await Promise.all([
		userNotsCountSer(request.body.searchWhereUnreadCount),
		userNotsListSer(request.body)
	]);
	promiseAll = JSON.parse(JSON.stringify(promiseAll));

	//	Unread Update
	if(promiseAll[0] !== 0) {
		await updateNotificationsSer(
			request.body.searchWhereUnreadCount,
			{
				isRead: "1"
			}
		);
	}
	//	Unread Update

	return responseHandler.successHandler(response, null, {
		unreadCount: promiseAll[0],
		list: promiseAll[1]
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.notificationList = notificationList;

//		Unread Notifications Count
const unreadCount = async (request, response) => {
try {

	request.body.searchWhere = {
		deleted: "0",
		userId: request.userDetails.userId,
		isRead: "0"
	};

	let unreadCount = await userNotsCountSer(request.body.searchWhere);

	return responseHandler.successHandler(response, null, {
		unreadCount
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.unreadCount = unreadCount;