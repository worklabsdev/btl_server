var Sequelize = require("sequelize");
const Op = Sequelize.Op;

var Db = require("../../../models");// ///// DB Models

const { failedActionHandler } = require("../../../commonFuncs/responseHandler");

const { ouserNotAvail } = require("../../../properties/constant").globalMessages.responseMessages;
//const { pageLimit } = require("../../../properties/constant").defaultValues;
const { coinsHistoryTypes, challengeUsersStatus } = require("../../../properties/constant");

const constants = require("./constants");

const { accepted } = require("../../../services/friendsServices").fReqStatuses;
const { updateChallengeUsers, challengeUsersTypes, challengeUserStatuses } = require("../../../services/challengeUsers");
const { updateNotificationsSer } = require("../../../services/userNotifications");
const { updateUserCoinsHistory } = require("../../../services/userCoinsHistories");

const { updateChallenge, challengeDefaultSort, chalSerMessages } = require("../../../services/challenges");
const { ChalNotAvailMsg } = require("../../../services/challenges").chalSerMessages;
const { creatorIncludePush } = require("../../../services/challenges").chalSerIncludes;
const { chalDetails4PushAtts } = require("../../../services/challenges").chalSerAttributes;

//	///	Challenge Create Other User Get
module.exports.ouser4ChallengeCreate = (response, userId, ouserId) => {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await Db.users.findOne(
				{
					where: {
						userId: ouserId,
						deleted: "0",
						blocked: "0"
					},
					include: [
						{
							as: "userFriendsSent",
							model: Db.userFriends,
							where: {
								ouserId: userId,
								status: accepted
							},
							required: false
						},
						{
							as: "userFriendsRec",
							model: Db.userFriends,
							where: {
								userId,
								status: accepted
							},
							required: false
						},
						{
							as: "blockedUser",
							model: Db.userBlockeds,
							where: {
								ouserId: userId,
								deleted: "0"
							},
							required: false
						},
						{
							as: "blockerUser",
							model: Db.userBlockeds,
							where: {
								userId,
								deleted: "0"
							},
							required: false
						}
					],
					// logging: console.log,
					// attributes: sequelizeAtts.userProfile
				}
			);
			user = JSON.parse(JSON.stringify(user));

			if (!user || user.blockedUser || (user.avail === "0"))
			{ return failedActionHandler(response, ouserNotAvail); }
			if (!user.userFriendsSent && !user.userFriendsRec) return failedActionHandler(response, constants.messages.friendFirstError);
			if (user.blockerUser) return failedActionHandler(response, constants.messages.unblockOUserToChallenge);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

//	///		Create Challenge
exports.createChallenge = async (data) => {
try {

	let challengeUsers = [];
	//	////	Creator Challenger Include
	if (data.userId) {
		challengeUsers.push({
			userId: data.userId,
			userType: challengeUsersTypes.creator,
			yellowCoins: data.totalDays
		});

		if (data.ouserId) {
			challengeUsers.push({
				userId: data.ouserId,
				userType: challengeUsersTypes.challenger,
				yellowCoins: 0,
			});
		}
	}
	//	////	Creator Challenger Include

	//	////	Creator Coins Hist Add
	let challengeHists = [
		{
			userId: data.userId,
			ouserId: data.ouserId,
			yellowCoins: data.totalDays*-1,
			greenCoins: 0,
			type: coinsHistoryTypes.challengeCreated
		}
	];
	//	////	Creator Coins Hist Add

		let challenge = await Db.challenges.create(
			{
				...data,
				coins: data.totalDays,
				challengeUsers,
				challengeHists
			},
			{
				include: [
					{
						as: "challengeUsers",
						model: Db.challengeUsers
					},
					{
						as: "challengeHists",
						model: Db.userCoinsHistories
					}
				]
			}
		);

		challenge.updateUser = await Db.users.update(
			{
				yellowCoins: Sequelize.literal(`yellowCoins - ${data.totalDays}`)
			},
			{
				where: {
					userId: data.userId
				}
			}
		);

		return challenge;
	}
	catch (error) {
		throw new Error(error.message);
	}
};

//	User Challenge Accept or Reject
exports.acceptRejectUserChal = async (data) => {
	try {

		let promiseAllArray = [
			updateChallengeUsers(
				{ challengeId: data.challengeId },
				{
					status: data.actionRequested
				}
			),
			Db.userNotifications.create(//	Notification Create
				{
					userId: data.ouserId,
					ouserId: data.userId,
					challengeId: data.challengeId,
					message: data.notificationMsg,
					type: data.notificationType,
					creator: data.creator
				}
			)
		];

		if(data.actionRequested === challengeUserStatuses.deleted) {//	Rejecting Challenge Update Creator Yellow Coins

			promiseAllArray.push(
				Db.userCoinsHistories.create(//	Creator Coins Refund
					{
						userId: data.ouserId,
						ouserId: data.userId,
						challengeId: data.challengeId,
						yellowCoins: data.coins,
						greenCoins: 0,
						type: coinsHistoryTypes.chalCreatedRefund
					}
				),//	Creator Coins Refund
				Db.users.update(//	Creator Coin Add
					{
						yellowCoins: Sequelize.literal(`yellowCoins + ${data.coins}`)
					},
					{
						where: {
							userId: data.ouserId
						}
					}
				),//	Creator Coin Add

			);

		}//	Rejecting Challenge Update Creator Yellow Coins
		else {//	Accepting Challenge Update Acceptor History

			promiseAllArray.push(
				Db.userCoinsHistories.create(//	Acceptor Coins History Add
					{
						userId: data.userId,
						ouserId: data.ouserId,
						challengeId: data.challengeId,
						yellowCoins: data.coins*-1,
						greenCoins: 0,
						type: coinsHistoryTypes.challengeAccepted
					}
				),//	Acceptor Coins History Add
				Db.users.update(//	Acceptor Coin Reduce
					{
						yellowCoins: Sequelize.literal(`yellowCoins - ${data.coins}`)
					},
					{
						where: {
							userId: data.userId
						}
					}
				),//	Acceptor Coin Reduce

			);

		}//	Accepting Challenge Update Acceptor History

		let promiseAll = await Promise.all(promiseAllArray);

		promiseAll = JSON.parse(JSON.stringify(promiseAll));

		return promiseAll;

	} catch (error) {
		throw new Error(error.message);
	}
};

//	Admin Challenge Accept or Reject
exports.acceptRejectAdminChal = async (data) => {
	try {

		let promiseAllArray = [
			Db.challengeUsers.create(
				{
					challengeId: data.challengeId,
					userId: data.userId,
					userType: challengeUsersTypes.challenger,
					yellowCoins: data.coins,
					status: data.actionRequested
				}
			)
		];

		if(data.actionRequested === challengeUserStatuses.accepted) {
			
			promiseAllArray.push(
				Db.adminNotifications.create(//	Admin Notification Create
					{
						userId: data.userId,
						challengeId: data.challengeId,
						message: data.notificationMsg,
						type: data.notificationType
					}
				)//	Admin Notification Create
			);

		}

		let promiseAll = await Promise.all(promiseAllArray);

		promiseAll = JSON.parse(JSON.stringify(promiseAll));

		return promiseAll;


	} catch (error) {
		throw new Error(error.message);
	}
};

//	///	Get Challenge For Update
exports.getChallenge4Deletion = (response, data) => {
	return new Promise(async (resolve, reject) => {
		try {
			let challenge = await Db.challenges.findOne({
				where: {
					challengeId: data.challengeId,
					userId: data.userId,
					deleted: "0",
					blocked: "0"
				},
				include: [
					{
						model: Db.challengeUsers,
						as: "challengeUser",
						attributes: constants.sequelizeAtts.challengerDetailList,
						where: {
							userId: data.userId,
							status: challengeUsersStatus.pending
						},
						required: false
					}
				],

				//logging: console.log
			});
			if (!challenge) return failedActionHandler(response, ChalNotAvailMsg);
			if (!challenge.challengeUser) return failedActionHandler(response, constants.messages.chalNoLogerBeDeleted);

			return resolve(challenge);
		}
		catch (error) {
			return reject(error);
		}
	});
};

//	///		Delete Challenge
exports.deleteChallenge = async (challengeId) => {
	try {
		var result = await Promise.all([
			updateChallenge(
				{ challengeId }, //	Where
				{ deleted: "1" }, //	New Data
			),
			updateChallengeUsers(
				{ challengeId }, //	Where
				{ status: challengeUsersStatus.deleted }, //	New Data
			),
			updateNotificationsSer(
				{ challengeId, deleted: "0" }, //	Where
				{ isRead: "1", deleted: "0" }, //	New Data
			),
			updateUserCoinsHistory(
				{ challengeId }, //	Where
				{ deleted: "1" }, //	New Data
			)
		]);

		return;
	}
	catch (error) {
		throw new Error(error.message);
	}
};

//	///		Admin Challenges Count	//
exports.appAdminChallengesCount = (extraCheck) => {

	let  whereRaw = Sequelize.literal(`(challenges.deleted="0") AND (challenges.blocked="0") AND (challenges.adminId != 0) ${extraCheck}`);

	return Db.challenges.count({
		where: whereRaw,
		//logging: console.log
	});

};

//	///		Admin Challenges 	//
exports.appAdminChallengesLists = (data) => {

	let  whereRaw = Sequelize.literal(`(challenges.deleted="0") AND (challenges.blocked="0") AND (challenges.adminId != 0) ${data.extraCheck}`);

	return Db.challenges.findAll({
		where: whereRaw,
		attributes: constants.sequelizeAtts.userChallengesList,

		order: data.orderBy || challengeDefaultSort,
		offset: data.offset,
		limit: data.pageSize,

		include: [
			{
				model: Db.challengeUsers,
				as: "challengeUser",
				attributes: constants.sequelizeAtts.challengerDetailList,
				where: {
					userId: data.userId,
					//status: challengeUsersStatus.pending
				},
				required: false,
				order: [["challengeUserId", "DESC"]]
			}
		],

		//logging: console.log
	});

};

//	///		User Challenges Count			//	///
const appUserChallengesCounts = (extraCheck) => {

	let  whereRaw = Sequelize.literal(`(challenges.deleted="0") AND (challenges.blocked="0") AND (challenges.adminId = 0) ${extraCheck}`);

	return Db.challenges.count({
		where: whereRaw,
		//logging: console.log
	});

};
exports.appUserChallengesCounts = appUserChallengesCounts;

//	///		User Challenges App List		//
exports.appUserChallengesLists = (data) => {

	let  whereRaw = Sequelize.literal(`(challenges.deleted="0") AND (challenges.blocked="0") AND (challenges.adminId = 0) ${data.extraCheck}`);

	return Db.challenges.findAll({
		where: whereRaw,
		attributes: constants.sequelizeAtts.userChallengesList,

		order: data.orderBy || challengeDefaultSort,
		offset: data.offset,
		limit: data.pageSize,

		include: [
			{
				model: Db.challengeUsers,
				as: "challengeUsers",
				attributes: constants.sequelizeAtts.challengerDetailList,

				include: [
					{
						model: Db.users,
						as: "user",
						attributes: ["userId", "name", "username", "phoneCode", "phoneNumber", "email", "profilePic"]
					}
				],

				// where: {
				// 	status: challengeUsersStatus.pending
				// },
				required: true,
				order: [["challengeUserId", "ASC"]]
			}
		],

		//logging: console.log
	});

};

//	Count List
exports.challengesInfoCounts = (data) => {

	return Db.sequelize.query(`SELECT

		(SELECT COUNT(*) FROM challenges C JOIN challengeUsers CU ON (CU.userId=${data.userId} AND CU.challengeId=C.challengeId AND CU.status NOT IN ("${challengeUsersStatus.rejected}") ) WHERE deleted="0" AND blocked="0") as activeCount,

		(SELECT COUNT(*) FROM challenges C JOIN challengeUsers CU ON (CU.userId=${data.userId} AND CU.challengeId=C.challengeId AND CU.status NOT IN ("${challengeUsersStatus.rejected}") ) WHERE deleted="0" AND blocked="0") as playedCount,

		(SELECT COUNT(*) FROM challenges C JOIN challengeUsers CU ON (CU.userId=${data.userId} AND CU.challengeId=C.challengeId AND CU.status="${challengeUsersStatus.won}") WHERE deleted="0" AND blocked="0") as wonCount,

		(SELECT COUNT(*) FROM challenges C JOIN challengeUsers CU ON (CU.userId=${data.userId} AND CU.challengeId=C.challengeId AND CU.status IN ("${challengeUsersStatus.won}", "${challengeUsersStatus.lost}", "${challengeUsersStatus.noResult}") ) WHERE deleted="0" AND blocked="0") as completedCount

	FROM dual
	`, { type: Sequelize.QueryTypes.SELECT});

};

//	Challenge Details for Accept or Reject
exports.chalDetails4Action = (response, whereJSON, opts) => {

	return new Promise(async (resolve, reject) => {
	try {

		let challenge = await Db.challenges.findOne({
			where: whereJSON,
			include: [
				creatorIncludePush,
				{
					model: Db.challengeUsers,
					as: "challengeUser",
					where: {
						userType: challengeUsersTypes.challenger,
						//status: { [Op.notIn]: [challengeUserStatuses.deleted] },
						userId: opts.userId
					},
					attributes: constants.sequelizeAtts.challengerDetailList,
					orderBy: [ ["challengeUserId", "DESC"] ],
					required: false
				}
			],
			attributes: chalDetails4PushAtts,

			//logging: console.log
		});
		if (!challenge)
			return failedActionHandler(response, ChalNotAvailMsg);

		challenge = JSON.parse(JSON.stringify(challenge));

		return resolve(challenge);

	} catch (error) {
		return reject(error);
	}
	});

}