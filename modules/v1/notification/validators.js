const Joi = require("@hapi/joi");

const apiRef = "notificationValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

// ///		Notification Listings
exports.notificationList = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/notificationList`;

	let schema = Joi.object().keys({
		"page": Joi.number().min(0).optional(),
		"limit": Joi.number().min(1).optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///		Unread Notifications Count
exports.unreadCount = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/unreadCount`;

	let schema = Joi.object().keys({
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};