var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validator = require("./validators");
const controllers = require("./controllers");

router.post("/list", verifyJWTMiddleware, validator.notificationList, controllers.notificationList);

router.get("/unreadCount", verifyJWTMiddleware, validator.unreadCount, controllers.unreadCount);

module.exports = router;
