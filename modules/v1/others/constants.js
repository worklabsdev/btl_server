const redeemFactor = 2;

const defaultValues = {
};

const messages = {
	"insuffientGreenCoins": "Insufficient green coins",
	"coinRedeemedSuccess": "Coin redeemed successfully"
};

const sequelizeAtts = {

};

module.exports = {
	redeemFactor,
	defaultValues,
	messages,
	sequelizeAtts
};
