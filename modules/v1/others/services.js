var Sequelize = require("sequelize");

const { errorHandler } = require("../../../commonFuncs/responseHandler");

var Db = require("../../../models");// ///// DB Models

const { coinsHistoryTypes } = require("../../../properties/constant");

const { userSignInResponse } = require("../userSignIn/services");
const { userUpdate } = require("../../../services/userServices");

const { createUserCoinsHistories } = require("../../../services/userCoinsHistories");
const { challengeUserStatuses } = require("../../../services/challengeUsers");
const { userFriendsCountSQL } = require("../../../services/friendsServices");

//  /// Redeemed Coins
exports.redeemCoins = async (response, data) => {

	let userId = data.userId;
	delete data.userId;

	try {

		await createUserCoinsHistories(//	User History Create
			{
				userId,
				yellowCoins: data.yellowCoins,
				greenCoins: data.greenCoins*-1,
				type: coinsHistoryTypes.redeemed
			}
		);//	User History Create

		await userUpdate(//	User Update Coins
			response,
			{ userId },
			{
				yellowCoins: Sequelize.literal(`yellowCoins + ${data.yellowCoins}`),
				greenCoins: Sequelize.literal(`greenCoins - ${data.greenCoins}`),
			}
		);//	User Update Coins

		let user = await userSignInResponse({ userId });

		return user;
	}
	catch (error) {
		return errorHandler(response, error);
	}
};

//	Dashboard Data Get
exports.dashboardCountsGet = (data) => {

	let challengesNotIn = `"${challengeUserStatuses.pending}", "${challengeUserStatuses.accepted}", "${challengeUserStatuses.deleted}"`;

	return Db.sequelize.query(`SELECT 
		U.userId,
		U.lastSyncDt,
		COALESCE(ROUND(UDS.distanceSum, 2), 0) as distanceSum,
		COALESCE(UDS.stepsSum, 0) as stepsSum,
		
		COALESCE(CAST(UCH.greenCoinsSum AS UNSIGNED), 0) as greenCoinsSum,
		#COALESCE(CAST(UCH.yellowCoinsSum AS UNSIGNED), 0) as yellowCoinsSum,

		COALESCE(CU.newChallengesSum, 0) as newChallengesSum
	from users U

	LEFT JOIN (
		SELECT 
			UDS.userId,
			SUM(UDS.totalDistance) distanceSum,
			SUM(UDS.totalSteps) stepsSum
		FROM userDeviceSyncs UDS
		WHERE (UDS.dt BETWEEN "${data.startDt}" AND "${data.endDt}")
		GROUP BY userId) UDS
	ON (U.userId = UDS.userId)

	LEFT JOIN (
		SELECT
			UCH.userId,
			SUM(UCH.greenCoins) as greenCoinsSum
			#SUM(UCH.yellowCoins) as yellowCoinsSum
		FROM userCoinsHistories UCH
		WHERE (UCH.createdAt BETWEEN "${data.startDt}" AND "${data.endDt}")
		GROUP BY userId) UCH
	ON (U.userId = UCH.userId)

	LEFT JOIN (
		SELECT
			userId,
			COUNT(*) as newChallengesSum
		FROM challengeUsers CU
		WHERE (CU.startedAt BETWEEN "${data.startDt}" AND "${data.endDt}") AND
		(CU.status NOT IN (${challengesNotIn}))
	GROUP BY userId) CU
	ON (U.userId = CU.userId)

	WHERE U.userId=${data.userId}`, { 
		type: Sequelize.QueryTypes.SELECT,
		//logging: console.log
	})
	.then((details) => {
		return details[0];
	});

};



//		User Profile for Dashboard Details
exports.dashboardUserDetails = (data) => {

	let friendsCountSQL = userFriendsCountSQL(data.userId);

	return Db.users.findOne({
		where: {
			userId: data.userId
		},
		attributes: [
			"userId", "yellowCoins", "greenCoins", "name", "profilePic", "email", "username", "totalDistance", "totalSteps",
			[Sequelize.literal(friendsCountSQL), "friendsCount"]
		],
		include: [
			{
				model: Db.userDevices,
				as: "userDevice",
				required: false,
				attributes: [
					"userDeviceId", "deviceId", "isValid", "updatedAt",
					[Sequelize.literal(`userDevice.totalDistance + 0`), "totalDistance"],
					[Sequelize.literal(`userDevice.totalSteps + 0`), "totalSteps"]
				],
				where: {
					deleted: "0",
					isValid: "1"
				},
				include: [
					{
						model: Db.devices,
						as: "device",
						attributes: ["deviceId", "name"],
						required: true,
						where: {
							blocked: "0"
						}
					}
				],
			}
		],
		order: [
			["userDevice", "updatedAt", "DESC"]
		],

		//logging: console.log
	})

};