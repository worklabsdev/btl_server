var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/redeemCoins", verifyJWTMiddleware, validators.redeemCoins, controllers.redeemCoins);

router.post("/dashData", verifyJWTMiddleware, validators.dashData, controllers.dashData);

router.post("/dashTotalData", verifyJWTMiddleware, validators.dashTotalData, controllers.dashTotalData);

router.post("/dashUserData", verifyJWTMiddleware, validators.dashUserData, controllers.dashUserData);

router.post("/dashChalsData", verifyJWTMiddleware, validators.dashChalsData, controllers.dashChalsData);

router.post("/leaderBoard", verifyJWTMiddleware, validators.leaderBoard, controllers.leaderBoard);

module.exports = router;
