const moment = require("moment");

const responseHandler = require("../../../commonFuncs/responseHandler");
const { currentUTC, utcConvertor, defaultMomentFormat } = require("../../../commonFuncs/momentFuncs");

const { pageOffsetCreator } = require("../../../utils/common");

const { appSettingsSerGetOne } = require("../../../services/appSettingsServices");
const { userDashChallengesList } = require("../../../services/challenges");
const { userDeviceSyncGetOne } = require("../../../services/userDeviceSyncServices");
const { globalLeaderListSer, friendsLeaderListSer } = require("../../../services/userCoinsHistories");
const { userMedalsLevelDbCalculator } = require("../../../services/medalServices");

let services = require("./services");
let constants = require("./constants");

//	///	Redeem Coins
const redeemCoins = async (request, response) => {
try {
	request.body.userId = request.userDetails.userId;

	request.body.greenCoins = parseInt(request.body.greenCoins);

	if (request.body.greenCoins > request.userDetails.greenCoins) return responseHandler.failedActionHandler(response, constants.messages.insuffientGreenCoins, null);

	let redeemFactor = constants.redeemFactor;
	let appSettingsGet = await appSettingsSerGetOne({
		appSettingId: 1
	});
	if(appSettingsGet)
		redeemFactor = appSettingsGet.redeemFactor;

	request.body.yellowCoins = request.body.greenCoins * redeemFactor;

	let user = await services.redeemCoins(response, request.body);

	return responseHandler.successHandler(response, constants.messages.coinRedeemedSuccess, { user });

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.redeemCoins = redeemCoins;

//	Dashoard Data
const dashData = async (request, response) => {
try {
	request.body = pageOffsetCreator(request.body);

	request.body.tempCurrentUTC = currentUTC();

	request.body.thisWeekStart = moment().startOf('isoWeek').format(defaultMomentFormat); //moment().startOf('week');

	let promiseAll = await Promise.all([
		services.dashboardCountsGet({
			userId: request.userDetails.userId,
			startDt: request.body.startDt,
			endDt: request.body.endDt
		}),
		services.dashboardUserDetails({
			userId: request.userDetails.userId
		}),
		userDeviceSyncGetOne(
			{
				userId: request.userDetails.userId,
				deleted: "0"
			},
			{
				attributes: ["userDeviceSyncId", "dt"],
				order: [["dt", "DESC"]]
			}
		),
		userMedalsLevelDbCalculator({//	Level Medals Calculator
			totalGreenCoins: request.userDetails.totalGreenCoins
		}),//	Level Medals Calculator

		globalLeaderListSer({//	LeadersBoard User List
			userId: request.userDetails.userId,
			startDt: request.body.tempCurrentUTC,
			endDt: request.body.thisWeekStart,
			getUserRank: true,
			pageSize: 1,
			offset: 0
		})//	LeadersBoard User List
	]);
	promiseAll = JSON.parse(JSON.stringify(promiseAll));

	if(promiseAll[4].length) {
		promiseAll[1].rank = promiseAll[4][0].rank;
		promiseAll[1].greenCoinsSum = promiseAll[4][0].greenCoinsSum;
	}
	else {
		promiseAll[1].rank = 0;
		promiseAll[1].greenCoinsSum = 0;
	}

	return responseHandler.successHandler(response, null, {
		data: request.body,
		// promiseAll: promiseAll,
		totals: promiseAll[0],
		user: promiseAll[1],
		lastSync: promiseAll[2],
		levelData: promiseAll[3]
	});

} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
}
exports.dashData = dashData;

//	DashBoard Total Counts
const dashTotalData = async (request, response) => {
try {

	let totals = await services.dashboardCountsGet({
		userId: request.userDetails.userId,
		startDt: request.body.startDt,
		endDt: request.body.endDt
	});

	return responseHandler.successHandler(response, null, {
		//data: request.body,
		totals
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.dashTotalData = dashTotalData;

//	DashBoard User Data
const dashUserData = async (request, response) => {
try {

	let user = await services.dashboardUserDetails({
		userId: request.userDetails.userId
	});

	return responseHandler.successHandler(response, null, {
		//data: request.body,
		user
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.dashUserData = dashUserData;

//	DashBoard Challenges Data
const dashChalsData = async (request, response) => {
try {

	let challenges = await userDashChallengesList({
		userId: request.userDetails.userId,
		startDt: request.body.startDt,
		endDt: request.body.endDt
	});

	return responseHandler.successHandler(response, null, {
		//data: request.body,
		challenges
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.dashChalsData = dashChalsData;

//	LeaderBoard Listing
const leaderBoard = async (request, response) => {
try {
	request.body = pageOffsetCreator(request.body);

	request.body.userId = request.userDetails.userId;

	let leaderboard = [];
	let userRank = null;
	if(request.body.type == "0") {//	Global LeaderBoard

		leaderboard = await globalLeaderListSer(request.body);

		userRank = await globalLeaderListSer({
			...request.body,
			getUserRank: true
		});

	}//	Global LeaderBoard
	else {//	Friends LeaderBoard

		leaderboard = await friendsLeaderListSer(request.body);

		userRank = await friendsLeaderListSer({
			...request.body,
			getUserRank: true
		});

	}//	Friends LeaderBoard

	if(!userRank.length)
		userRank = null;
	else
		userRank = userRank[0];

	return responseHandler.successHandler(response, null, {
		data: request.body,
		userRank: userRank,
		leaderboard,
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.leaderBoard = leaderBoard;