const BaseJoi = require("@hapi/joi");
const Extension = require("@hapi/joi-date");

const Joi = BaseJoi.extend(Extension);

const apiRef = "otherValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

// ///		Other User Profile
exports.redeemCoins = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/redeemCoins`;

	let schema = Joi.object().keys({
		"greenCoins": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	DashBoard Data
exports.dashData = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/dashData`;

	let schema = Joi.object().keys({
		"startDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
		"endDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	DashBoard Data
exports.dashTotalData = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/dashTotalData`;

	let schema = Joi.object().keys({
		"startDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
		"endDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	DashBoard Data
exports.dashUserData = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/dashUserData`;

	let schema = Joi.object().keys({
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	DashBoard Challenges Data
exports.dashChalsData = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/dashChalsData`;

	let schema = Joi.object().keys({
		"startDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
		"endDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	Leaders Board Listing
exports.leaderBoard = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/leaderBoard`;

	let schema = Joi.object().keys({
		"type": Joi.string().valid("0", "1").required(),
		"startDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
		"endDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
		"page": Joi.number().min(0).optional(),
		"limit": Joi.number().min(1).optional()
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};