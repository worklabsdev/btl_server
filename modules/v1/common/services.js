var Db = require("../../../models");// ///// DB Models

exports.userFriends = (userId) => {
	return Db.users.findOne({
		where: {
			userId
		},
		attributes: [
			"userId"
		],
		include: [
			{
				as: "userFriendsRecs",
				model: Db.userFriends,
				where: { userId: "4" },
				required: false
			}
		]
	});
};
