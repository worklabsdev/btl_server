var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validator = require("./validators");
const controllers = require("./controllers");

router.post("/appData", controllers.appData);

router.post("/test", controllers.testApi);

module.exports = router;
