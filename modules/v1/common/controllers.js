const {
	errorHandler, successHandler, createdHandler, failedActionHandler
} = require("../../../commonFuncs/responseHandler");

// const { logger } = require("../../../commonFuncs/logging");

const { userChalStartCron } = require("../../common/crons/userChalStart")
const { userChalEndCron } = require("../../common/crons/userChalEnd");
const { adminChalStartCron } = require("../../common/crons/adminChalStart");
const { adminChalEndCron } = require("../../common/crons/adminChalEnd");
const { ongoingChalSync, newOngoingChalSync } = require("../../common/crons/ongoingChalSync");

const { fitbitWebhookActivities } = require("../../devices/fitbit/utils");
const { appleHealthWebhookActivities } = require("../../devices/appleHealth/utils");

const { sendSMS } = require("../../../packages/sms");
// const  { slugGenerator } = require("../../../commonFuncs/commonFuncs");

const { appSettingsSerGetOne } = require("../../../services/appSettingsServices");
const { appDevicesList } = require("../../../services/devices");
const { challengeUsersTypes, challengeUserStatuses } = require("../../../services/challengeUsers");

const services = require("./services");

const Db = require("../../../models");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

//	///			App Data
const appData = async (request, response) => {
	try {
		// let appVersions = appConfig.appVersions;
		// let devices = await appDevicesList();

		let promiseAll = await Promise.all([
			appDevicesList(),
			appSettingsSerGetOne()
		]);
	
		return successHandler(response, "", { 
			appVersions: promiseAll[1],
			devices: promiseAll[0]
		});

	} catch (error) {
		return errorHandler(response, error, { data: request.body });
	}
};
exports.appData = appData;

//	Test Api
const testApi = async (request, response) => {
try {


	// let challenges = await userChalEndCron();
	// return response.json({
	// 	success: 1,
	// 	challenges
	// });


	// let challenges = await userChalStartCron();
	// return response.json({
	// 	success: 1,
	// 	challenges
	// });


	// let challenges =  await adminChalStartCron();
	// return response.json({
	// 	success: 1,
	// 	challenges
	// });


	// let challenges =  await adminChalEndCron();
	// return response.json({
	// 	success: 1,
	// 	challenges
	// });

	// let challenges =  await ongoingChalSync({
	// 	userId: 347,
	// 	userDeviceSyncId: 1,
	// 	devicesWebhookId: 0,
	// 	incrementSteps: 100,
	// 	incrementDistance: 30,
	// 	dt: '2019-09-03 09:30:00'
	// });

	// let challenges = await appleHealthWebhookActivities(
	// 	{
	// 		"steps": 1766,
	// 		"distance": 6.100420000000027,
	// 		"userDeviceSyncId": 1,
	// 		"dt": "2019-09-19"
	// 	},
	// 	{
	// 		"apiRefFull": "devices/appleHealth/syncData",
	// 		"requestId": "c89afe9d-1b12-4a75-acdb-6a3a7a547719",
	// 		"deviceId": 1,
	// 		"devicesWebhookId": 42,
	// 		"userId": 432,
	// 		"userDeviceId": 1,
	// 		"deviceSyncsHistObj": {
	// 		  "userDeviceSyncId": 1,
	// 		  "userDeviceId": 1,
	// 		  "dt": "2019-09-19",
	// 		  "totalDistance": 6.10042,
	// 		  "totalSteps": 1766,
	// 		  "oldDistance": "6.10042",
	// 		  "oldSteps": "1766"
	// 		},
	// 		"waitTime": 6000
	// 	  }
	// );

	// let challenges = await appleHealthWebhookActivities(
	// 	{
	// 		"steps": 1766,
	// 		"distance": 6.100420000000027,
	// 		"userDeviceSyncId": 0,
	// 		"dt": "2019-09-19",
	// 	},
	// 	{
	// 		"apiRefFull": "devices/appleHealth/syncData",
	// 		"requestId": "96c78cf1-269c-4595-ba62-1aaf324d2945",
	// 		"deviceId": 1,
	// 		"devicesWebhookId": 38,
	// 		"userId": 432,
	// 		"userDeviceId": 1,
	// 		"waitTime": 6000
	// 	}
	// );

	let challenges = await newOngoingChalSync({
		userId: 432,
		userDeviceSyncId: 4,
		devicesWebhookId: 8,
		incrementSteps: 2000,
		incrementDistance: 240.0599,
		dt: '2019-09-19',
		requestId: '96c78cf1-269c-4595-ba62-1aaf324d2945'
	});

	return response.json({
		success: 1,
		challenges
	});



	

	// fitbitWebhookActivities(
	// 	{
	// 		0: {
	// 			date: "2019-09-05",
	// 			ownerId: "5XNVH2",
	// 			ownerType: "user",
	// 			collectionType: "activities",
	// 			subscriptionId: "5XNVH2"
	// 		}
	// 	},
	// 	{
	// 		apiRefFull: request.apiRefFull,
	// 		requestId: request.requestId,	
	// 		headers: request.headers
	// 	}
	// );


	//let result = await sendSMS("+919888754351", "Test Message");

	return createdHandler(response, null, {result});

	// let temp = await services.userFriends(1);

	// logger()

} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.testApi = testApi;