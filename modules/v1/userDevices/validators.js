const BaseJoi = require("@hapi/joi");
const Extension = require("@hapi/joi-date");

const Joi = BaseJoi.extend(Extension);

const apiRef = "challengeValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

// ///		Add Device
exports.addDevice = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addDevice`;

	let schema = Joi.object().keys({
		"deviceId": Joi.number().min(1).required(),
		"totalDistance": Joi.number().min(1).required(),
		"totalSteps": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//  ///     Delete Device
exports.deleteDevice = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/deleteDevice`;

	let schema = Joi.object().keys({
		"userDeviceId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};
