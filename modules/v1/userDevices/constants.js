const apiRef = "userDevices";

const defaultValues = {
};

const appDevices = {
	phoneSensor: 1,
	sHealth: 2,
	appleHealth: 3,
	pacer: 4,
	garmin: 5,
	fitbit: 6
};

const messages = {
	"deviceNotAvail": "Sorry, this device is currently not available",
	"deviceAlreadyAdded": "Sorry, this device is already added by you",
	"deviceAddSuccess": "Device added successfully",
	"deviceDelSuccess": "Device deleted successfully",
	"userDeviceNotAvail": "Sorry, this user device is currently not available",
};

const sequelizeAtts = {
	"userChallengesList": { "exclude": ["timezone", "visibleAt", "createdAt", "updatedAt", "deleted", "blocked"] }
};

module.exports = {
	apiRef,
	appDevices,
	defaultValues,
	messages,
	sequelizeAtts
};
