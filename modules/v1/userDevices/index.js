var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validator = require("./validators");
const controllers = require("./controllers");

router.post("/add", verifyJWTMiddleware, validator.addDevice, controllers.addDevice);

router.delete("/delete", verifyJWTMiddleware, validator.deleteDevice, controllers.deleteDevice);

module.exports = router;
