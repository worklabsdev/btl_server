const moment = require("moment");

const { mysqlDtFormat } = require("../../../properties/constant").defaultValues;

exports.addDaysDt = (dt, days) => {
	return moment(dt).add(days, "days").format(mysqlDtFormat);
};

exports.isPastDt = (dt) => {
	return moment().isAfter(dt);
};
