var Sequelize = require("sequelize");

const Op = Sequelize.Op;

var Db = require("../../../models");// ///// DB Models

const { failedActionHandler } = require("../../../commonFuncs/responseHandler");

const constants = require("./constants");

//	Device Check For Adding
exports.deviceCheck4Add = (deviceId, userId) => {
	return Db.devices.findOne({
		where: {
			blocked: "0",
			deviceId
		},
		include: [
			{
				model: Db.userDevices,
				as: "userDevice",
				where: {
					deleted: "0",
					userId
				},
				required: false
			}
		]
	});
};

//	User Device Add
exports.deviceAdd = (data) => {
	return Db.userDevices.create(
		{
			...data,
			syncs: {
				userId: data.userId,
				totalDistance: data.totalDistance,
				totalSteps: data.totalSteps
			}
		},
		{
			include: [
				{
					as: "syncs",
					model: Db.userDeviceSyncs,
				}
			]
		}
	);
};

//	User Device Get For Updation or Deletion
exports.userDeviceValid = (response, whereJSON, opts = {}) => {
	return new Promise(async (resolve, reject) => {
		try {
			let userDevice = await Db.userDevices.findOne({
				where: whereJSON
			});
			if (!userDevice) return failedActionHandler(response, constants.messages.userDeviceNotAvail);

			return resolve(userDevice);
		}
		catch (error) {
			return reject(error);
		}
	});
};

//	Update UserDevice
exports.updateUserDevice = (whereJSON, newData) => {
	return Db.userDevices.update(
		newData,
		{
			where: whereJSON
		}
	);
};
