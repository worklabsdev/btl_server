const responseHandler = require("../../../commonFuncs/responseHandler");

// const { createNotification } = require("../../../services/userNotifications");
// const { notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");

let services = require("./services");
let constants = require("./constants");
//let utils = require("./utils");

//	///	Create Challenge
const addDevice = async (request, response) => {
	try {
		let device = await services.deviceCheck4Add(request.body.deviceId, request.userDetails.userId);
		if (!device) return responseHandler.failedActionHandler(response, constants.messages.deviceNotAvail);
		if (device.userDevice) return responseHandler.failedActionHandler(response, constants.messages.deviceAlreadyAdded);

		request.body.userId = request.userDetails.userId;

		device = await services.deviceAdd(request.body);

		return responseHandler.createdHandler(response, constants.messages.deviceAddSuccess, { device });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

//	///	Delete Challenge
const deleteDevice = async (request, response) => {
	try {
		let userDevice = await services.userDeviceValid(response, {
			userId: request.userDetails.userId,
			deleted: "0",
			userDeviceId: request.body.userDeviceId
		});

		userDevice = await services.updateUserDevice(
			{ userDeviceId: request.body.userDeviceId },
			{ deleted: "1" }
		);

		return responseHandler.createdHandler(response, constants.messages.deviceDelSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

module.exports = {
	addDevice,
	deleteDevice
};
