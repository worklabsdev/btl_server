const defYellowCoins = 999999;
const referalNewYellowCoins = 999999;
const referalOldYellowCoins = 10;

exports.defYellowCoins = defYellowCoins;
exports.referalNewYellowCoins = referalNewYellowCoins;
exports.referalOldYellowCoins = referalOldYellowCoins;

const sequelizeAtts = {
	"userSignInAtts": {
		"exclude": ["password", "salt", "socketId", "blocked", "deleted", "createdAt", "updatedAt"],
		// "include": [
		//     "userId", "userName", "phoneCode", "phoneNumber", "email", "facebookId", "googleId", "dob", "address", "state", "city", "country", "countryISO2", "zipcode", "addressLatitude", "addressLongitude", "currentLatitude", "currentLongitude", "gender", "profilePic", "coverPic", "yellowCoins", "greenCoins", "totalDistance", "totalSteps", "inviteCode", "otp", "otpValidity", "avail", "availDt", "challengesPlayed", "challengesWon", "challengesCompleted", "timezone"
		// ]
	},
	"userDevicesAtts": { "exclude": ["deleted", "createdAt", "updatedAt", "accessToken", "refreshToken", "odata"] },
	"deviceAtts": { "exclude": ["blocked", "createdAt", "updatedAt"] },
};
exports.sequelizeAtts = sequelizeAtts;

const messages = {
	"signupSuccess": "Signed Up Successfully",
	"loggedInSuccess": "Logged in Successfully",
	"otpSentSuccess": "OTP sent successfully",
	"otpExpired": "This OTP has expired",
	"passwordUpdateSuccess": "Password updated successfully",
	"profileUpdateSuccess": "Profile updated successfully",
	"settingUpdateSuccess": "Settings updated successfully",
	"verifyOTPToAddNumber": "Please verify the OTP to add this phone number",
	"otpVerifiedSuccess": "OTP verified successfully",

	"invalidEmailOrPassword": "Sorry either your email or password is incorrect",
	"enterCorrectOldPassword": "Please enter the correct old password",
	"newPasswordOldMatch": "New password cannot be same as old password",
	"InvalidCred": "Invalid creds please try again",
	"InvalidOTP": "Invalid OTP",
	"PhoneAlreadyVerified": "Phone number is already verified",
	"LoggedOutSuccessMsg": "Logged out successfully",

	"PhoneUpdateSameErrorMsg": "Please enter an new phone number to update it",
	"PhoneUpdateSuccessMsg": "Phone Number updated successfully, Please login again to continue"

};
exports.messages = messages;
