const Joi = require("@hapi/joi");

const apiRef = "userValidator";

const { joiValidate } = require("../../../commonFuncs/validate");
const { loginTypes, genderTypes } = require("../../../services/userServices");

const { deviceOSSchema, latSchema, lngSchema } = require("../../../utils/joiSchemas");

//	///			Unique Check
exports.uniqueCheck = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/uniqueCheck`;

	let schema = Joi.object().keys({
		"key": Joi.string().valid("email", "username", "phoneNumber", "facebookId", "googleId").required(),
		"value": Joi.string().required(),
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Email Signup Validation
exports.signup = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/signup`;

	let schema = Joi.object().keys({
		"phoneCode": Joi.string().max(5).optional(),
		"phoneNumber": Joi.string().min(8).max(12).optional(),
		"email": Joi.string().email({ minDomainSegments: 2 }).optional(),
		"password": Joi.string().min(6).optional(),
		"fcmId": Joi.string().optional(),
		"currentLatitude": latSchema.optional(),
		"currentLongitude": lngSchema.optional(),
		"timezone": Joi.string().max(50).required(),

		"referalCode": Joi.string().optional(),

		"deviceOS": deviceOSSchema
	}).or("email", "phoneNumber").with("phoneNumber", "phoneCode")
		.with("email", "password");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	Social Login
exports.socialLogin = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/socialLogin`;

	let schema = Joi.object().keys({
		"loginType": Joi.string().required().valid(loginTypes.google, loginTypes.facebook),

		"googleId": Joi.string().when("loginType", {
			"is": loginTypes.google,
			"then": Joi.string().max(50).required(),
			"otherwise": Joi.forbidden()
		}),

		"facebookId": Joi.string().when("loginType", {
			"is": loginTypes.facebook,
			"then": Joi.string().max(50).required(),
			"otherwise": Joi.forbidden()
		}),

		"email": Joi.string().email().optional(),

		"fcmId": Joi.string().optional(),
		"currentLatitude": latSchema.optional(),
		"currentLongitude": lngSchema.optional(),
		"timezone": Joi.string().max(50).required(),

		"profilePic": Joi.string().uri().trim().optional(),

		"referalCode": Joi.string().optional(),

		"deviceOS": deviceOSSchema
	});

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	Social Link
exports.socialLink = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/socialLink`;

	let schema = Joi.object().keys({
		"loginType": Joi.string().required().valid("Google", "Facebook"),

		"googleId": Joi.string().when("loginType", {
			"is": "Google",
			"then": Joi.string().max(50).required(),
			"otherwise": Joi.forbidden()
		}),

		"facebookId": Joi.string().when("loginType", {
			"is": "Facebook",
			"then": Joi.string().max(50).required(),
			"otherwise": Joi.forbidden()
		})

	});

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		User Login
exports.login = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/login`;

	let schema = Joi.object().keys({
		"phoneNumber": Joi.string().min(8).max(12).optional(),
		"email": Joi.string().email({ minDomainSegments: 2 }).optional(),
		"username": Joi.string().optional(),
		"password": Joi.string().min(6).optional(),
		"fcmId": Joi.string().optional(),
		"currentLatitude": latSchema.optional(),
		"currentLongitude": lngSchema.optional(),
		"timezone": Joi.string().max(50).required(),

		"deviceOS": deviceOSSchema
	}).or("email", "phoneNumber", "username").with("email", "password")
		.with("username", "password");

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       User Add Phone
exports.addPhone = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addPhone`;

	let schema = Joi.object().keys({
		"phoneCode": Joi.string().max(5).required(),
		"phoneNumber": Joi.string().min(8).max(12).optional()
	});

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       User Verify Phone
exports.verifyPhone = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/verifyPhone`;

	let schema = Joi.object().keys({
		"otp": Joi.string().length(4).required()
	});

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Password Reset
exports.passwordReset = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/passwordReset`;

	let schema = Joi.object().keys({
		"password": Joi.string().min(6).required(),
		"passwordConfirmation": Joi.any().valid(Joi.ref("password")).required().options({ "language": { "any": { "allowOnly": "must match password" } } }),
		"otp": Joi.string().length(4).required()
	});

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///	Password Forgot
exports.passwordForgot = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/passwordForgot`;

	let schema = Joi.object().keys({
		"username": Joi.string().required()
	});

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       User Pic Upload
exports.picUpload = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/picUpload`;

	let schema = Joi.object().keys({
		"element": Joi.string().valid("profilePic", "coverPic").required(),
		// upload: Joi.binary().encoding('base64').max(5*1024*1024).required()
	});

	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		User Profile Update
exports.profileUpdate = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/profileUpdate`;

	let schema = Joi.object().keys({
		"name": Joi.string().max(255).required(),
		// .options({ convert: false }).optional(),
		"username": Joi.string().optional(),

		"dob": Joi.date().raw(),

		"address": Joi.string().max(255).optional(),
		"state": Joi.string().max(100),
		"city": Joi.string().max(100),
		"country": Joi.string().max(50),
		"countryISO2": Joi.string().max(2),
		"zipcode": Joi.string().max(20),
		"addressLatitude": latSchema.optional(),
		"addressLongitude": lngSchema.optional(),

		"gender": Joi.string().max(20).valid(genderTypes.male, genderTypes.female, genderTypes.notDisclosed).required()

	}).with("address", ["state", "city", "country", "countryISO2", "zipcode", "addressLatitude", "addressLongitude"]);
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Password Update
exports.passwordUpdate = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/passwordUpdate`;

	let schema = Joi.object().keys({
		"oldPassword": Joi.string().min(6).required(), // .label("old password"),
		"password": Joi.string().min(6).required(), // .disallow(Joi.ref("oldPassword")) //.label("new password"),
		"passwordConfirmation": Joi.any().valid(Joi.ref("password")).required().options({ "language": { "any": { "allowOnly": "must match password" } } })// .label("new password confirmation"),
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Setting Update
exports.settingUpdate = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/settingUpdate`;

	let schema = Joi.object().keys({
		"avail": Joi.string().valid("0", "1").required(),
		"availDays": Joi.number().min(1).max(7).required()
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Data Update on Every Login
exports.dataUpdate = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/dataUpdate`;

	let schema = Joi.object().keys({
		"fcmId": Joi.string().optional(),
		"currentLatitude": latSchema.optional(),
		"currentLongitude": lngSchema.optional(),
		"timezone": Joi.string().max(50).required()
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};


//	///		User Logout
exports.logout = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/logout`;

	let schema = Joi.object().keys({
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		User Medals Get
exports.medalsList = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/medalsList`;

	let schema = Joi.object().keys({
		ouserId: Joi.number().min(1).required()
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		User Phone Update
exports.phoneUpdate = (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/phoneUpdate`;

	let schema = Joi.object().keys({
		"password": Joi.string().min(6).required(),
		"phoneCode": Joi.string().max(5).required(),
		"phoneNumber": Joi.string().min(8).max(12).required()
	});
	let valCheck = joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};