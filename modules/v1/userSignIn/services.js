var Sequelize = require("sequelize");
var { Promise } = Sequelize;

//const { searchUserWhereRaw, userProfilePic } = require("../../../services/userServices");
const { staticValues, coinsHistoryTypes } = require("../../../properties/constant");
const { authHandler, failedActionHandler } = require("../../../commonFuncs/responseHandler");

const { appDevicesListAtts } = require("../../../services/devices").attributes;
const { commonSerAttributes } = require("../../../services/commonServices");

const { sequelizeAtts, defYellowCoins, referalNewYellowCoins } = require("./constants");

var Db = require("../../../models");// ///// DB Models

// ///			User Sign In Response
exports.findOneUser = (whereJSON) => {
	return Db.users.findOne({
		where: whereJSON
	});
};

// ///           User Unique Check
exports.userUniqueCheck = (response, whereJSON, key) => {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await Db.users.findOne(
				{ where: whereJSON },
				{
					raw: true
				}
			);
			if (user) return failedActionHandler(response, `This ${key} is already registered with us`);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

// ///           User Details Get
exports.userDetails = (whereJSON) => {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await Db.users.findOne(
				{ where: whereJSON },
				{
					raw: true
				}
			);
			if (!user) return reject(new Error("This user is currently not available"));
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

// ///			User Get
exports.userGet = (response, whereJSON) => {
	return new Promise(async (resolve, reject) => {
		try {

			let user = await Db.users.findOne({
				where: whereJSON,
				//logging: console.log,
			});

			if(!user)	return authHandler(response, 4);
			else if(user.blocked === "1") return authHandler(response, 3);

			resolve(user);

		}
		catch (error) {
			return reject(error);
		}
	});
};

//	///			User Sign In Response
exports.userSignInResponse = (whereJSON) => {
	return Db.users.findOne({
		where: whereJSON,
		attributes: sequelizeAtts.userSignInAtts,
		include: [
			{
				model: Db.userDevices,
				as: "userDevices",
				required: false,
				where: { deleted: "0", isValid: "1" },
				attributes: sequelizeAtts.userDevicesAtts,
				include: [
					{
						model: Db.devices,
						as: "device",
						where: { blocked: "0" },
						attributes: appDevicesListAtts
					}
				]
			}
		]
	});
};

//	///		User Profile Update Data
const userProfile4UpdateData = (userId) => {

	return Db.users.findOne({
		where: {
			userId
		},
		attributes: sequelizeAtts.userSignInAtts,
		include: [
			{
				model: Db.userDevices,
				as: "userDevice",
				where: { deleted: "0", isValid: "1" },
				required: false,
				attributes: commonSerAttributes.userDeviceAttsNormal,
				include: [
					{
						model: Db.devices,
						as: "device",
						//where: { blocked: "0" },
						attributes: appDevicesListAtts
					},
					{
						model: Db.userDeviceSyncs,
						as: "userDeviceSync",
						attributes: [ "userDeviceSyncId", "dt" ]
					}
				]
			}
		],
		order: [
			["userDevice", "userDeviceSync", "dt", "DESC"]
		]
	});

};
exports.userProfile4UpdateData = userProfile4UpdateData;

// ///           Create User
exports.userCreate = (data) => {

	let yellowCoins = data.yellowCoins || defYellowCoins;
	let referalNewYellowCoins = 0;

	let userCoinsHistories = [{
		yellowCoins,
		type: coinsHistoryTypes.registered
	}];

	if(data.referalUserId) {

		referalNewYellowCoins = data.referalNewYellowCoins || referalNewYellowCoins;

		userCoinsHistories.push({
			ouserId: data.referalUserId,
			yellowCoins: referalNewYellowCoins,
			type: coinsHistoryTypes.referalNew
		});
	}

	yellowCoins = yellowCoins + referalNewYellowCoins;//	Adding Default Yellow + If any referal code

	return Db.users.create({

		name: data.name || "",
		username: data.username || "",

		phoneCode: data.phoneCode || "",
		phoneNumber: data.phoneNumber || null,

		email: data.email || "",

		facebookId: data.facebookId || "",
		googleId: data.googleId || "",

		password: data.password || "",
		salt: data.salt || "",

		accessToken: data.accessToken || "",

		dob: data.dob || null,

		address: "",
		state: "",
		city: "",
		country: "",
		countryISO2: "",
		zipcode: "",
		addressLatitude: 0.0,
		addressLongitude: 0.0,

		currentLatitude: data.currentLatitude || 0.0,
		currentLongitude: data.currentLongitude || 0.0,

		fcmId: data.fcmId || "",

		socketId: data.socketId || "",

		gender: data.gender || "Not Disclosed",

		profilePic: data.profilePic || "",
		coverPic: "",

		yellowCoins: yellowCoins,
		greenCoins: 0,
		totalYellowCoins: yellowCoins,
		totalGreenCoins: 0,

		totalDistance: 0,
		totalSteps: 0,

		inviteCode: data.inviteCode,
		referalUserId: data.referalUserId || 0,

		otp: data.otp || null,
		otpValidity: data.otpValidity || null,

		avail: staticValues.user.defaultAvail,
		availDt: data.availDt || null,

		blocked: "0",
		deleted: "0",

		challengesPlayed: 0,
		challengesWon: 0,
		challengesCompleted: 0,

		timezone: staticValues.user.defaultTimezone,
		deviceOS: data.deviceOS,

		userCoinsHistories
	},
	{
		include: [
			{//	Referal History Insert
				model: Db.userCoinsHistories,
				as: "userCoinsHistories"
			}//	Referal History Insert
		]
	}
	);

};
