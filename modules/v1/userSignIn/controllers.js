const Op = require("sequelize").Op;

const {
	errorHandler, successHandler, createdHandler, failedActionHandler
} = require("../../../commonFuncs/responseHandler");
const {
	inviteCodeGenerator, generateOTP, slugGenerator
} = require("../../../commonFuncs/commonFuncs");
const { generatePassword, verifyPassword } = require("../../../commonFuncs/password");
const { generateJWT } = require("../../../commonFuncs/auth");
const { futureDtCheck, generateFutureDt } = require("../../../commonFuncs/momentFuncs");

const { fileUpload } = require("../../../packages/upload");
const { globalMessages } = require("../../../properties/constant");

const { friendDetails } = require("../friends/services");

const { appSettingsSerGetOne } = require("../../../services/appSettingsServices");
const { appDevicesList } = require("../../../services/devices");
const { userUpdate, loginTypes } = require("../../../services/userServices");
const { createLoginHist, updateLoginHist } = require("../../../services/loginHists");
const { userDeviceUpdate } = require("../../../services/userDevices");
const { userDeviceSyncGetOne } = require("../../../services/userDeviceSyncServices");
const { userMedalsLevelDbCalculator } = require("../../../services/medalServices");

const constants = require("./constants");
const services = require("./services");
const helpers = require("./helpers");

//	////		User Unique Check
const uniqueCheck = async (request, response) => {
try {
	let user = await services.userUniqueCheck(
		response,
		{
			[request.body.key]: request.body.value,
			deleted: "0"
		},
		request.body.key
	);

	return successHandler(response, null, {});
} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.uniqueCheck = uniqueCheck;

// ////          User Signup Function
const signup = async (request, response) => {
try {
	let key = "";
	let whereJSON = {
		deleted: "0"
	};

	if (request.body.email) {
		key = "email";
		whereJSON.email = request.body.email;
	}
	else {
		key = "phone number";
		whereJSON.phoneNumber = request.body.phoneNumber;
	}
	let user = await services.userUniqueCheck(response, whereJSON, key);

	request.body.inviteCode = inviteCodeGenerator();

	if (request.body.password) { // ///   Password Hashed
		let pass = generatePassword(request.body.password);
		request.body.password = pass.hash;
		request.body.salt = pass.salt;
	}// ///   Password Hashed

	let otpGen = generateOTP();

	request.body.otp = otpGen.otp;
	request.body.otpValidity = otpGen.otpValidity;

	request.body.username = slugGenerator(request.body.name);// /	Slug Generate

	////////////////////
	//	App Settings Get
	request.body.yellowCoins = constants.defYellowCoins;
	request.body.referalNewYellowCoins = constants.referalNewYellowCoins;
	request.body.referalOldYellowCoins = constants.referalOldYellowCoins;


	let appVersions = appConfig.appVersions;
	try {
		let appSettings = await appSettingsSerGetOne({});
		if(appSettings) {
			request.body.yellowCoins = appSettings.defYellowCoins;
			request.body.referalNewYellowCoins = appSettings.referalNewYellowCoins;
			request.body.referalOldYellowCoins = appSettings.referalOldYellowCoins;

			appVersions = appSettings.appVersions;
		}
	}
	catch(error) {
		request.body.yellowCoins = constants.defYellowCoins;
		request.body.referalNewYellowCoins = constants.referalNewYellowCoins;
		request.body.referalOldYellowCoins = constants.referalOldYellowCoins;
	}
	//	App Settings Get
	/////////////////////

	//	Referal Code Get Id
	let referalUser = await helpers.getReferalUserId(request.body.referalCode);
	request.body.referalUserId = referalUser.userId;
	//	Referal Code Get Id

	user = await services.userCreate(request.body);

	// ///   JWT Generate Update
	let auth = generateJWT({
		userId: user.userId,
		email: user.email,
		name: user.name,
		phoneNumber: user.phoneNumber
	});
	// ///   JWT Generate Update

	await Promise.all([
		userUpdate(//	User Token Update
			response,
			{ userId: user.userId },
			{ accessToken: auth }
		),//	User Token Update
		updateLoginHist(//	Login History Old Update
			{
				userId: user.userId,
				isValid: "1",
				accessToken: { [Op.ne]: auth }
			},
			{
				isValid: "0"
			}
		),//	Login History Old Update
		createLoginHist({//	Login History Create
			userId: user.userId,
			latitude: request.body.currentLatitude,
			longitude: request.body.currentLongitude,
			timezone: request.body.timezone,
			accessToken: auth,
			odata: {
				type: loginTypes.signup,
				ip: request.ip,
				device: request.device
			}
		})//	Login History Create
	]);

	user = await services.userSignInResponse({
		userId: user.userId
	});

	//	Referal Code Coins Cashback
	helpers.referalCodeCashback(
		response,
		{
			referalUser,
			user: {
				userId: user.userId,
				name: user.name,
				fcmId: user.fcmId,
				email: user.email,
				username: user.username
			},
			yellowCoins: request.body.yellowCoins,
			referalNewYellowCoins: request.body.referalNewYellowCoins,
			referalOldYellowCoins: request.body.referalOldYellowCoins
		},
		{
			requestId: request.requestId,
			apiRefFull: request.apiRefFull
		}
		);
	//	Referal Code Coins Cashback

	return createdHandler(response, constants.messages.signupSuccess, { 
		appVersions,
		auth,
		user
	});
} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.signup = signup;

//	///		User Social Login
const socialLogin = async (request, response) => {
try {
	let jsonWhere = {
		deleted: "0"
	};

	//if(request.body.email) jsonWhere.email = request.body.email;
	if (request.body.googleId) jsonWhere.googleId = request.body.googleId;
	else jsonWhere.facebookId = request.body.facebookId;

	let user = await services.findOneUser(jsonWhere);
	let updateData = {};
	let referalUser = { userId: 0 };
	if (!user) {
		request.body.inviteCode = inviteCodeGenerator();
		request.body.username = slugGenerator(request.body.name);// /	Slug Generate

		////////////////////
		//	App Settings Get
		request.body.yellowCoins = constants.defYellowCoins;
		request.body.referalNewYellowCoins = constants.referalNewYellowCoins;
		request.body.referalOldYellowCoins = constants.referalOldYellowCoins;
		try {
			let appSettings = await appSettingsSerGetOne({});
			if(appSettings) {
				request.body.yellowCoins = appSettings.defYellowCoins;
				request.body.referalNewYellowCoins = appSettings.referalNewYellowCoins;
				request.body.referalOldYellowCoins = appSettings.referalOldYellowCoins;
			}
		}
		catch(error) {
			request.body.yellowCoins = constants.defYellowCoins;
			request.body.referalNewYellowCoins = constants.referalNewYellowCoins;
			request.body.referalOldYellowCoins = constants.referalOldYellowCoins;
		}
		//	App Settings Get
		/////////////////////

		//	Referal Code Get Id
		referalUser = await helpers.getReferalUserId(request.body.referalCode);
		request.body.referalUserId = referalUser.userId;
		//	Referal Code Get Id

		user = await services.userCreate(request.body);
	}
	else {
		updateData = { ...request.body };
	}

	// ///   JWT Generate And Update
	updateData.accessToken = generateJWT({
		userId: user.userId,
		email: user.email,
		name: user.name,
		phoneNumber: user.phoneNumber
	});// auth
	// ///   JWT Generate And Update

	let promiseAll = await Promise.all([
		userUpdate(//	User Token Update
			response,
			{ userId: user.userId },
			updateData
		),//	User Token Update
		updateLoginHist(//	Login History Old Update
			{
				userId: user.userId,
				isValid: "1",
				accessToken: { [Op.ne]: updateData.accessToken }
			},
			{
				isValid: "0"
			}
		),//	Login History Old Update
		createLoginHist({//	Login History Create
			userId: user.userId,
			latitude: request.body.currentLatitude,
			longitude: request.body.currentLongitude,
			timezone: request.body.timezone,
			accessToken: updateData.accessToken,
			odata: {
				type: request.body.loginType,
				ip: request.ip,
				device: request.device
			},
			isValid: "1"
		}),//	Login History Create
		appSettingsSerGetOne({})
	]);

	let appVersions = appConfig.appVersions;
	if(promiseAll[3]) {
		appVersions = promiseAll[3].appVersions;
	}

	user = await services.userSignInResponse({
		userId: user.userId
	});

	//	Referal Code Coins Cashback
	helpers.referalCodeCashback(
		response,
		{
			referalUser,
			user: {
				userId: user.userId,
				name: user.name,
				fcmId: user.fcmId,
				email: user.email,
				username: user.username
			},
			yellowCoins: request.body.yellowCoins,
			referalNewYellowCoins: request.body.referalNewYellowCoins,
			referalOldYellowCoins: request.body.referalOldYellowCoins
		},
		{
			requestId: request.requestId,
			apiRefFull: request.apiRefFull
		}
		);
	//	Referal Code Coins Cashback

	return createdHandler(response, constants.messages.loggedInSuccess, { 
		appVersions,
		auth: updateData.accessToken,
		user 
	});

} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.socialLogin = socialLogin;

//	///			Social Link
const socialLink = async (request, response) => {
try {
	let jsonWhere = {
		userId: {
			$ne: request.userDetails.userId
		}
	};
	let element = "";
	if (request.body.googleId) {
		jsonWhere.googleId = request.body.googleId;
		element = loginTypes.google;
	}
	else {
		jsonWhere.facebookId = request.body.facebookId;
		element = loginTypes.facebook;
	}

	let user = await services.findOneUser(jsonWhere);
	if (user) return failedActionHandler(response, `Sorry this ${element} account is already linked to someone else`);
	let uUpdate = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		request.body
	);
	return createdHandler(response, `${element} account linked successfully`, {});
} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.socialLink = socialLink;

// ///       User Add Phone Number
const addPhone = async (request, response) => {
try {
	let user = await services.userUniqueCheck(
		response,
		{
			phoneNumber: request.body.phoneNumber,
			deleted: "0",
			userId: { [Op.ne]: request.userDetails.userId }
		},
		"Phone Number"
	);

	let otpGen = generateOTP();

	request.body.otp = otpGen.otp;
	request.body.otpValidity = otpGen.otpValidity;

	user = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		request.body
	);

	return successHandler(response, constants.messages.verifyOTPToAddNumber, {});
} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.addPhone = addPhone;

// ///           User Verify Phone
const verifyPhone = async (request, response) => {
try {
	request.userDetails = JSON.parse(JSON.stringify(request.userDetails));

	// if(request.userDetails.phoneVerified === "1")
	// 	return failedActionHandler(response, constants.messages.PhoneAlreadyVerified);

	if (request.body.otp !== request.userDetails.otp)
		return failedActionHandler(response, constants.messages.InvalidOTP);

	// ///       Expiry Check
	// let checkOTPValidity = futureDtCheck(request.userDetails.otpValidity);
	// if (!checkOTPValidity) {
	// 	let otpGen = generateOTP();

	// 	request.body.otp = otpGen.otp;
	// 	request.body.otpValidity = otpGen.otpValidity;

	// 	let user = await userUpdate(
	// 		response,
	// 		{
	// 			userId: request.userDetails.userId,
	// 		},
	// 		request.body
	// 	);

	// 	return failedActionHandler(response, constants.messages.otpExpired, {detail: request.userDetails});
	// }
	// ///       Expiry Check

	let user = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		{
			otp: "",
			otpValidity: null,
			phoneVerified: "1"
		}
	);

	user = await services.userSignInResponse({
		userId: request.userDetails.userId
	});

	return successHandler(response, constants.messages.otpVerifiedSuccess, { user });
} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.verifyPhone = verifyPhone;

// ///	User Resend OTP
const resendOTP = async (request, response) => {
try {
	let otpGen = generateOTP();

	let user = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		{
			otp: otpGen.otp,
			otpValidity: otpGen.otpValidity
		}
	);

	return successHandler(response, constants.messages.otpSentSuccess);
} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.resendOTP = resendOTP;

// ///		Password Forgot
const passwordForgot = async (request, response) => {
try {
	let whereJSON = {
		deleted: "0",
		blocked: "0",
		[Op.or]: [
			{ email: request.body.username },
			{ phoneNumber: request.body.username },
			{ username: request.body.username }
		]
	};

	let user = await services.userGet(response, whereJSON);

	let otpGen = generateOTP();

	// ///   JWT Generate
	let auth = generateJWT({
		userId: user.userId,
		email: user.email,
		name: user.name,
		phoneNumber: user.phoneNumber
	});

	user = await userUpdate(
		response,
		{
			userId: user.userId,
		},
		{
			otp: otpGen.otp,
			otpValidity: otpGen.otpValidity,
			accessToken: auth
		}
	);

	return successHandler(response, constants.messages.otpSentSuccess, { auth });
}
catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.passwordForgot = passwordForgot;

// ///		Password Reset
const passwordReset = async (request, response) => {
try {
	if (request.body.otp !== request.userDetails.otp) return failedActionHandler(response, constants.messages.InvalidOTP);

	// ///       Expiry Check
	// let checkOTPValidity = futureDtCheck(request.userDetails.otpValidity);
	// if (!checkOTPValidity) {
	// 	let otpGen = generateOTP();

	// 	request.body.otp = otpGen.otp;
	// 	request.body.otpValidity = otpGen.otpValidity;

	// 	let user = await userUpdate(
	// 		response,
	// 		{
	// 			userId: request.userDetails.userId,
	// 		},
	// 		request.body
	// 	);

	// 	return failedActionHandler(response, constants.messages.otpExpired);
	// }
	// ///       Expiry Check

	// ///   Paswword Hashed
	let pass = generatePassword(request.body.password);
	request.body.password = pass.hash;
	request.body.salt = pass.salt;
	request.body.otp = "";
	request.body.otpValidity = null;

	let user = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		request.body
	);

	return successHandler(response, constants.messages.passwordUpdateSuccess);
} catch (error) {
	return errorHandler(response, error, { data: request.body });
}
};
exports.passwordReset = passwordReset;

// ///			Pic Upload
const picUpload = async (request, response) => {
try {
	var elementName; var
		columnName;
	if (request.body.element === "profilePic") {
		elementName = "Profile pic";
		columnName = "profilePic";
	}
	else {
		elementName = "Cover pic";
		columnName = "coverPic";
	}

	if (!request.files.upload) throw (new Error(`${elementName} ${globalMessages.responseMessages.imageUploadError}`));

	let imageName = await fileUpload(request.files.upload, elementName);

	let updateJSON = {};
	updateJSON[columnName] = imageName;

	let user = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		updateJSON
	);

	user = await services.userSignInResponse({
		userId: request.userDetails.userId
	});

	return successHandler(response, `${elementName} updated successfully`, { user });
} catch (error) {
	return errorHandler(response, error, {});
}
};
exports.picUpload = picUpload;

//	///		User Profile Update
const profileUpdate = async (request, response) => {
try {
	if ((request.body.username) && (request.userDetails.username !== request.body.username)) { //	User Name Unique Check
		await services.userUniqueCheck(
			response,
			{
				"username": request.body.username,
				deleted: "0"
			},
			"username"
		);
	}//	User Name Unique Check


	let user = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		request.body
	);

	user = await services.userSignInResponse({
		userId: request.userDetails.userId
	});

	return successHandler(response, constants.messages.profileUpdateSuccess, { user });
} catch (error) {
	return errorHandler(response, error, {});
}
};
exports.profileUpdate = profileUpdate;

//	///	User Login
const login = async (request, response) => {
try {
	let whereJSON = {
		deleted: "0",
	};
	if (request.body.email) whereJSON.email = request.body.email;
	else if (request.body.phoneNumber) whereJSON.phoneNumber = request.body.phoneNumber;
	else if (request.body.username) whereJSON.username = request.body.username;

	let user = await services.userGet(response, whereJSON);

	if (request.body.password) {
		let verifyPass = await verifyPassword(request.body.password, user.password);
		if (!verifyPass) return failedActionHandler(response, constants.messages.InvalidCred);
	}
	else { // /		OTP
		let otpGen = generateOTP();

		request.body.otp = otpGen.otp;
		request.body.otpValidity = otpGen.otpValidity;
	}// /	OTP

	if(user.phoneVerified === "0") {
		let otpGen = generateOTP();

		request.body.otp = otpGen.otp;
		request.body.otpValidity = otpGen.otpValidity;
	}

	delete request.body.email;
	delete request.body.password;
	delete request.body.username;

	// ///   JWT Generate
	request.body.accessToken = generateJWT({
		userId: user.userId,
		email: user.email,
		name: user.name,
		phoneNumber: user.phoneNumber
	});


	await Promise.all([
		userUpdate(//	User Token Update
			response,
			{
				userId: user.userId,
			},
			request.body
		),//	User Token Update
		updateLoginHist(//	Login History Old Update
			{
				userId: user.userId,
				isValid: "1",
				accessToken: { [Op.ne]: request.body.accessToken }
			},
			{
				isValid: "0"
			}
		),//	Login History Old Update	
		createLoginHist({//	Login History Create
			userId: user.userId,
			latitude: request.body.currentLatitude,
			longitude: request.body.currentLongitude,
			timezone: request.body.timezone,
			accessToken: request.body.accessToken,
			odata: {
				type: loginTypes.normal,
				ip: request.ip,
				device: request.device
			}
		})//	Login History Create
	]);

	user = await services.userSignInResponse({
		userId: user.userId
	});

	return successHandler(response, constants.messages.loggedInSuccess, { auth: request.body.accessToken, user });
} catch (error) {
	return errorHandler(response, error);
}
};
exports.login = login;

// ///		Password Update
const passwordUpdate = async (request, response) => {
try {
	let verifyPass = await verifyPassword(request.body.oldPassword, request.userDetails.password);
	if (!verifyPass) return failedActionHandler(response, constants.messages.enterCorrectOldPassword);

	if (request.body.password === request.userDetails.password) return failedActionHandler(response, constants.messages.newPasswordOldMatch);

	// ///   Password Hashed
	let pass = generatePassword(request.body.password);
	request.body.password = pass.hash;
	request.body.salt = pass.salt;

	let uUpdate = await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		request.body
	);

	return successHandler(response, constants.messages.passwordUpdateSuccess, {});
} catch (error) {
	return errorHandler(response, error);
}
};
exports.passwordUpdate = passwordUpdate;

//	///		Settings Update
const settingUpdate = async (request, response) => {
try {

	if(request.body.avail === "1") {
		request.body.availDt = null;
	}
	else {
		request.body.availDt = generateFutureDt(parseInt(request.body.availDays)*24*60);
	}
	
	await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		request.body
	);

	let user = await services.userSignInResponse({
		userId: request.userDetails.userId
	});

	return successHandler(response, constants.messages.settingUpdateSuccess, { 
		user
	});

} catch (error) {
	return errorHandler(response, error);
}
};
exports.settingUpdate = settingUpdate;

//	///		Data Update
const dataUpdate = async (request, response) => {
try {
	
	await userUpdate(
		response,
		{
			userId: request.userDetails.userId,
		},
		request.body
	);

	//let appVersions = appConfig.appVersions;
	let res = await Promise.all([
		services.userProfile4UpdateData(request.userDetails.userId),
		appDevicesList(),
		appSettingsSerGetOne(),
		userDeviceSyncGetOne(
			{
				userId: request.userDetails.userId,
				deleted: "0"
			},
			{
				attributes: ["userDeviceSyncId", "dt"],
				order: [["dt", "DESC"]]
			}
		)
	]);
	res = JSON.parse(JSON.stringify(res));

	let result = {
		appVersions: res[2].appVersions,
		settings: res[2],
		devices: res[1],
		user: res[0],
		lastSync: res[3]
	};

	return successHandler(response, constants.messages.settingUpdateSuccess, result);
} catch (error) {
	return errorHandler(response, error);
}
};
exports.dataUpdate = dataUpdate;

//	///		User Logout
const logout = async (request, response) => {
try {

	await Promise.all([

		userUpdate(//	User Update
			response,
			{
				userId: request.userDetails.userId,
			},
			{
				accessToken: null,
				fcmId: null,
				socketId: null,
				deviceOS: null
			}
		),//	User Update

		updateLoginHist(//	Login History Update
			{
				accessToken: request.userDetails.accessToken
			},
			{
				isValid: "0"
			}
		),//	Login History Update

		userDeviceUpdate(//	User Device Update Unsync
			{
				isValid: "1",
				userId: request.userDetails.userId
			},
			{
				accessToken: null,
				refreshToken: null,
				isValid: "0"
			}

		)//	User Device Update Unsync
		
	]);

	return successHandler(response, constants.messages.LoggedOutSuccessMsg);
	
} catch (error) {
	return errorHandler(response, error);
}
};
exports.logout = logout;

//	Medal Listing
const medalsList = async (request, response) => {
try {

	let user = request.userDetails;
	if(parseInt(request.body.ouserId) !== user.userId) {
		user = await friendDetails(response, { userId: request.body.ouserId });
	}
	user = JSON.parse(JSON.stringify(user));

	//	Level Medals Calculator
	let levelData = await userMedalsLevelDbCalculator({
		totalGreenCoins: user.totalGreenCoins
	});
	//	Level Medals Calculator

	return successHandler(response, null, {
		levelData,
		ouser: {
			greenCoins: user.greenCoins,
			totalGreenCoins: user.totalGreenCoins
		}
	});

} catch (error) {
	return errorHandler(response, error);
}
};
exports.medalsList = medalsList;

//	User Phone Number Update
const phoneUpdate = async (request, response) => {
try {

	let verifyPass = await verifyPassword(request.body.password, request.userDetails.password);
	if (!verifyPass)
		return failedActionHandler(response, constants.messages.InvalidCred);

	if(request.body.phoneNumber == request.userDetails.phoneNumber)
		return failedActionHandler(response, constants.messages.PhoneUpdateSameErrorMsg);

	await services.userUniqueCheck(
		response,
		{
			phoneNumber: request.body.phoneNumber,
			deleted: "0",
			userId: { [Op.ne]: request.userDetails.userId }
		},
		"Phone Number"
	);

	await Promise.all([
		userUpdate(//	User Token Update
			response,
			{ userId: request.userDetails.userId },
			{
				phoneCode: request.body.phoneCode,
				phoneNumber: request.body.phoneNumber,
				otp: "",
				otpValidity: null,
				accessToken: "",
				phoneVerified: "0"
			}
		),//	User Token Update
		updateLoginHist(//	Login History Old Update
			{
				userId: request.userDetails.userId,
				isValid: "1",
			},
			{
				isValid: "0"
			}
		),//	Login History Old Update
	]);

	return successHandler(response, constants.messages.PhoneUpdateSuccessMsg);

} catch (error) {
	return errorHandler(response, error);
}
};
exports.phoneUpdate = phoneUpdate;
