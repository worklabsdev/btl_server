const Sequelize = require("sequelize");

var Db = require("../../../models");// ///// DB Models

const { infoLogger, errorLogger } = require("../../../packages/logging/consoleLogger");
const { coinsHistoryTypes, globalMessages } = require("../../../properties/constant");

const { sendPushNotification } = require("../../../packages/fcm/");
const { notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");

const { createNotification } = require("../../../services/userNotifications");
const { user4PushNotificationAtts } = require("../../../services/commonServices").commonSerAttributes;

//  Get Referal User Details
const getReferalUserId = (referalCode=null) => {

    let defaulRef = { userId: 0 };

    if(!referalCode)
        return defaulRef;

    return new Promise(async (resolve, reject) => {
        try {

            let user = await Db.users.findOne({
                where: {
                    inviteCode: referalCode
                },
                attributes: user4PushNotificationAtts,
                //logging: console.log
            });

            if(!user)   return resolve(defaulRef);
            else return resolve(user);

        }
        catch (error) {
            return resolve(defaulRef);
        }
    });

};
exports.getReferalUserId = getReferalUserId;

//      Referal Code Coins Addition
const referalCodeCashback = async (response, data, odata) => {
try {
    odata.apiRefFull = `${odata.apiRefFull}/referalCodeCashback`;

    if( (!data.referalUser.userId) || (data.referalOldYellowCoins <= 0) || (data.referalOldYellowCoins !== undefined) ) {
        infoLogger(odata.apiRefFull, odata.requestId, { skippend: 1, data });
        return;
    }

    let message = response.trans(
        {
            phrase: globalMessages.pushMessages.referalOldPush,
            locale: appConfig.default.language
        },
        {
            coins: data.referalOldYellowCoins
        }
    );
    let type = notificationTypes.referalOld;

    let promiseAll = await Promise.all([
        Db.userCoinsHistories.create(// User Coins History Create
            {
                userId: data.referalUser.userId,
                ouserId: data.user.userId,
                yellowCoins: data.referalOldYellowCoins,
                type: coinsHistoryTypes.referalOld
            }
        ),// User Coins History Create

        Db.users.update(//    User Coins Update
            {
                yellowCoins: Sequelize.literal(`yellowCoins + ${data.referalOldYellowCoins}`),
                totalYellowCoins: Sequelize.literal(`totalYellowCoins + ${data.referalOldYellowCoins}`)
            }
        ),//    User Coins Update

        createNotification(//   User Create Notification
            {
                userId: data.referalUser.userId,
                ouserId: data.user.userId,
                message,
                type,
                creator: notificationCreatorTypes.user
            }
        )//   User Create Notification

    ]);

    sendPushNotification(//    Old user Push Notifications
        data.referalUser.fcmId,
        {
            message,
            userNotificationId: `${promiseAll[2].userNotificationId}`,
            pushType: type,
            userId: `${data.referalUser.userId}`
        },
        {
            requestId: odata.requestId,
            deviceOS: data.referalUser.deviceOS || null
        }
    );//    Old user Push Notifications

    infoLogger(odata.apiRefFull, odata.requestId, { skippend: 0, data });

} catch (error) {
    errorLogger(odata.apiRefFull, odata.requestId, error);
}
};
exports.referalCodeCashback = referalCodeCashback;