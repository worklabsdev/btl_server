var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/uniqueCheck", validators.uniqueCheck, controllers.uniqueCheck);

router.post("/signup", validators.signup, controllers.signup);
router.post("/socialLogin", validators.socialLogin, controllers.socialLogin);

router.post("/socialLink", verifyJWTMiddleware, validators.socialLink, controllers.socialLink);

router.post("/login", validators.login, controllers.login);

router.post("/addPhone", verifyJWTMiddleware, validators.addPhone, controllers.addPhone);

router.post("/verifyPhone", verifyJWTMiddleware, validators.verifyPhone, controllers.verifyPhone);

router.post("/resendOTP", verifyJWTMiddleware, controllers.resendOTP);

router.post("/passwordForgot", validators.passwordForgot, controllers.passwordForgot);
router.post("/passwordReset", verifyJWTMiddleware, validators.passwordReset, controllers.passwordReset);

router.post("/picUpload", verifyJWTMiddleware, multipartMiddleware, validators.picUpload, controllers.picUpload);

router.post("/profileUpdate", verifyJWTMiddleware, validators.profileUpdate, controllers.profileUpdate);

router.post("/passwordUpdate", verifyJWTMiddleware, validators.passwordUpdate, controllers.passwordUpdate);

router.post("/settingUpdate", verifyJWTMiddleware, validators.settingUpdate, controllers.settingUpdate);

router.post("/dataUpdate", verifyJWTMiddleware, validators.dataUpdate, controllers.dataUpdate);

router.post("/logout", verifyJWTMiddleware, validators.logout, controllers.logout);

router.post("/medals", verifyJWTMiddleware, validators.medalsList, controllers.medalsList);

router.post("/phoneUpdate", verifyJWTMiddleware, validators.phoneUpdate, controllers.phoneUpdate);

module.exports = router;
