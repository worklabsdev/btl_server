let pathPrefix = "/api/v1";

app.use(`${pathPrefix}/block/`, require("./block"));
app.use(`${pathPrefix}/challenges/`, require("./challenges"));
app.use(`${pathPrefix}/common/`, require("./common"));
app.use(`${pathPrefix}/friend/`, require("./friends"));
app.use(`${pathPrefix}/other/`, require("./others"));
app.use(`${pathPrefix}/device/`, require("./userDevices"));
app.use(`${pathPrefix}/user/`, require("./userSignIn"));
app.use(`${pathPrefix}/notification/`, require("./notification"));

//http://localhost:3334/api/v1/friend/sendRequest