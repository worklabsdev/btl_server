const responseHandler = require("../../../commonFuncs/responseHandler");

let services = require("./services");
let constants = require("./constants");

//  ////        Add Remove User Friend
const addRemove = async (request, response) => {
try {
	request.body.userId = request.userDetails.userId;

	var ouser = await services.ouserAddRemoveGet(response, request.body.userId, request.body.ouserId);

	let message;

	if (request.body.type === constants.reqTypes.block) { //    ///Blocking User
		if (ouser.blockerUser) return responseHandler.failedActionHandler(response, constants.messages.userAlreadyBlocked);

		let userBlocked = await services.create(request.body);

		message = constants.messages.userBlockedSuccess;
	}//    ///Blocking User
	else { //    /// Unblocking User
		if (!ouser.blockerUser) return responseHandler.failedActionHandler(response, constants.messages.userAlreadyUnblocked);

		let userBlockedId = ouser.blockerUser.userBlockedId;

		let userBlocked = await services.update({ deleted: "1" }, { userBlockedId });

		message = constants.messages.userUnBlockedSuccess;
	}//    /// Unblocking User

	return responseHandler.successHandler(response, message, {});
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.addRemove = addRemove;

//  ///     User Blocked List
const list = async (request, response) => {
	try {
		let users = await services.list(request.userDetails.userId, request.body.page, request.body.limit);

		return responseHandler.successHandler(response, null, { users });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.list = list;