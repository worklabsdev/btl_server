var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validator = require("./validators");
const controllers = require("./controllers");

router.post("/addRemove", verifyJWTMiddleware, validator.addRemove, controllers.addRemove);

router.post("/list", verifyJWTMiddleware, validator.list, controllers.list);

module.exports = router;
