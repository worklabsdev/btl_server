const Joi = require("@hapi/joi");

const apiRef = "blockValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

const { reqTypes } = require("./constants");

// ///		Add Remove Other User
exports.addRemove = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addRemove`;

	let schema = Joi.object().keys({
		"type": Joi.string().valid(reqTypes.block, reqTypes.unblock).required(),
		"ouserId": Joi.number().min(1).required(),
		"comments": Joi.string().optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//  ///     User Blocked List
exports.list = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/list`;

	let schema = Joi.object().keys({
		"page": Joi.number().min(0).optional(),
		"limit": Joi.number().min(1).optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};
