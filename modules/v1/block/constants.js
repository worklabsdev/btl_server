const reqTypes = {
	"block": "block",
	"unblock": "unblock"
};

const messages = {
	"userBlockedSuccess": "User blocked successfully",
	"userAlreadyBlocked": "This user is already blocked by you",
	"userUnBlockedSuccess": "User unblocked successfully",
	"userAlreadyUnblocked": "This user is already unblocked by you"
};

let sequelizeAtts = {
	"userBlockeds": ["userBlockedId", "userId", "ouserId", "comments"]
};

module.exports = {
	reqTypes,
	messages,
	sequelizeAtts
};
