var Sequelize = require("sequelize");

const Op = Sequelize.Op;

const { failedActionHandler } = require("../../../commonFuncs/responseHandler");
const { ouserNotAvail } = require("../../../properties/constant").globalMessages.responseMessages;

const { pageLimit } = require("../../../properties/constant").defaultValues;

const { searchUserWhereRaw, userProfilePic } = require("../../../services/userServices");

const { sequelizeAtts } = require("./constants");

var Db = require("../../../models");// ///// DB Models

//  ///     User Details For Add Remove
exports.ouserAddRemoveGet = (response, userId, ouserId) => {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await Db.users.findOne({
				where: {
					userId: { [Op.ne]: userId, [Op.eq]: ouserId },
					deleted: "0",
					blocked: "0"
				},
				include: [
					{
						as: "blockedUser",
						model: Db.userBlockeds,
						where: { ouserId: userId, deleted: "0" },
						required: false,
						attributes: sequelizeAtts.userBlockeds
					},
					{
						as: "blockerUser",
						model: Db.userBlockeds,
						where: { userId, deleted: "0" },
						required: false,
						attributes: sequelizeAtts.userBlockeds
					}
				]
			});
			if (!user) return failedActionHandler(response, ouserNotAvail);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

//  ///     Create User
exports.create = (data) => {
	return Db.userBlockeds.create({
		...data
	});
};

//  ///     Update User
exports.update = (data, whereJSON) => {
	return Db.userBlockeds.update(
		data,
		{
			where: whereJSON
		}
	);
};

//  ///     Blocked List
exports.list = (userId, offset = 0, limit = pageLimit, search = null) => {
	let searchRaw = searchUserWhereRaw(search);

	return Db.sequelize.query(`
	SELECT 
		U.userId, U.name, U.email, U.phoneCode, U.phoneNumber, U.username, ${userProfilePic} 
		FROM users U 
        JOIN userBlockeds UB ON (UB.userId=${userId} AND UB.ouserId=U.userId AND UB.deleted="0")
        WHERE U.blocked="0" AND U.deleted="0" AND U.userId != ${userId} ${searchRaw}
        ORDER BY UB.updatedAt DESC
        LIMIT ${offset}, ${limit}
	`, { type: Sequelize.QueryTypes.SELECT });
};
