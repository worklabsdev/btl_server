const defaultValues = {
};
exports.defaultValues = defaultValues;

const challengeListTypes = {
	"requests": "Requests",
	"friends": "Friends",
	"admin": "Admin",
	"history": "History"
};
exports.challengeListTypes = challengeListTypes;


const challengeListSubTypes = {
	"admin": "0",
	"friend": "1",
	"ongoing": "0",
	"upcoming": "1"
};
exports.challengeListSubTypes = challengeListSubTypes;


const messages = {
	"friendFirstError": "You need to be friend with this user for creating challenge",
	"unblockOUserToChallenge": "Please unblock this user for creating challenge",
	"InsuffientYToCreateChallenge": "Insuffient yellow coins to create this challenge",
	"InsuffientYToAcceptChallenge": "Insuffient yellow coins to accept this challenge",
	"PastDtError": "Please select a future date for the challenge",
	"ChalCreatedSuccess": "Challenge created successfully",
	"InviteForChallenge": " has invited you for challenge",
	"ChalDeletedSuccess": "Challenge deleted successfully",
	"chalNotAvail": "Sorry, this challenge is currently not available",
	"chalNoLogerBeDeleted": "Sorry, this challenge can no longer be deleted",
	"ChalAcceptedSuccess": "Challenge accepted successfully",
	"ChalRejectSuccess": "Challenge rejected successfully",
	"ChalAcceptMsg": " has accepted your challenge",
	"ChalRejectMsg": " has rejected your challenge"
};
exports.messages = messages;


const sequelizeAtts = {
	"userChallengesList": { "exclude": ["timezone", "visibleAt", "createdAt", "updatedAt", "deleted", "blocked", "directions" ] },
	"chalCreatorDetailsList": ["userId", "profilePic", "name", "username", "email", "phoneCode", "phoneNumber"],
	"challengerDetailList": ["challengeUserId", "userId", "userType", "challengeId", "status", "updatedAt", "totalDistance", "totalSteps"]
};
exports.sequelizeAtts = sequelizeAtts;