const responseHandler = require("../../../commonFuncs/responseHandler");

const { SendReq2SelfErr } = require("../friends/constants").messages;

const { notificationTypes, notificationCreatorTypes, challengeUsersStatus } = require("../../../properties/constant");

const { pageOffsetCreator } = require("../../../utils/common");

const { sendPushNotification } = require("../../../packages/fcm/");

const { createNotification } = require("../../../services/userNotifications");
const { adminChallengeDetailsService, userChallengeDetailsService, chalSerMessages } = require("../../../services/challenges");
const { challengeUserStatuses } = require("../../../services/challengeUsers");

const {
	futureDtCheck, addSubtractTime
} = require("../../../commonFuncs/momentFuncs");

const { userChalStartCron } = require("../../common/crons/userChalStart");
const { adminChalStartCron } = require("../../common/crons/adminChalStart");


let services = require("./services");
let constants = require("./constants");
let utils = require("./utils");

//	///	Create Challenge
const createChallenge = async (request, response) => {
	try {
		if(parseInt(request.body.ouserId) === request.userDetails.userId)
			return responseHandler.failedActionHandler(response, SendReq2SelfErr);

		let dtCheck = utils.isPastDt(request.body.startDt);
		if (dtCheck) return responseHandler.failedActionHandler(response, constants.messages.PastDtError);

		if (request.userDetails.yellowCoins < request.body.totalDays) return responseHandler.failedActionHandler(response, constants.messages.InsuffientYToCreateChallenge);

		var oUser = await services.ouser4ChallengeCreate(response, request.userDetails.userId, request.body.ouserId);

		request.body.adminId = 0;//	Admin Id will be Zero;
		request.body.userId = request.userDetails.userId;//	User Id will be current User Id;
		request.body.totalDays = parseInt(request.body.totalDays);

		let addDays = request.body.totalDays;
		if(request.body.totalDays !== 1)
			addDays = addDays -1;

		request.body.endDt = addSubtractTime(request.body.startDt, addDays); 

		var challenge = await services.createChallenge(request.body);

		// ///      Push Notification
		let message = request.userDetails.name + response.trans({ phrase: constants.messages.InviteForChallenge, locale: appConfig.default.language });
		let type = notificationTypes.chalInvite;

		let uNotification = await createNotification({
			userId: request.body.ouserId,
			ouserId: request.userDetails.userId,
			challengeId: challenge.challengeId,
			message,
			type,
			creator: notificationCreatorTypes.user
		});

		sendPushNotification(
			oUser.fcmId,
			{
				message,
				userNotificationId: `${uNotification.userNotificationId}`,
				pushType: type,
				userId: `${request.userDetails.userId}`
			},
			{
				requestId: request.requestId,
				deviceOS: oUser.deviceOS || null
			}
		);
		// ///      Push Notification

		return responseHandler.createdHandler(response, constants.messages.ChalCreatedSuccess, { challenge });

	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.createChallenge = createChallenge;

//	///		Delete Challenge
const deleteChallenge = async (request, response) => {
try {
	request.body.userId = request.userDetails.userId;
	request.body.challengeId = parseInt(request.body.challengeId);

	var challenge = await services.getChallenge4Deletion(response, request.body);

	var update = await services.deleteChallenge(request.body.challengeId, {
		userId: challenge.userId,
		totalDays: challenge.totalDays
	});

	return responseHandler.successHandler(response, constants.messages.ChalDeletedSuccess, { challenge, update });
} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.deleteChallenge = deleteChallenge;

//	///		Delete Challenge
const listChallenges = async (request, response) => {
try {

	request.body = pageOffsetCreator(request.body);

	var result = {
		requests: {
			"adminsCount": 0,
			"admins": [],
			"friendsCounts": 0,
			"friends": []
		},
		friends: {
			"ongoingsCounts": 0,
			"ongoings": [],
			"upcomingsCounts": 0,
			"upcomings": []
		},
		admin: {
			"ongoingsCounts": 0,
			"ongoings": [],
			"upcomingsCounts": 0,
			"upcomings": []
		},
		history: {
			"adminsCount": 0,
			"admins": [],
			"friendsCounts": 0,
			"friends": []
		},
	};

	request.body.userId = request.userDetails.userId;

	if (request.body.type === constants.challengeListTypes.requests) { //	Request Types	Screen 28.18

		if(request.body.subType === constants.challengeListSubTypes.admin) {

			request.body.extraCheck = `AND (endDt > now()) AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ((CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) )) = 0) `;


			let asyncRes = await Promise.all([
				services.appAdminChallengesLists(request.body),
				services.appAdminChallengesCount(request.body.extraCheck)
			]);

			result.requests.admins = asyncRes[0];
			result.requests.adminsCount = asyncRes[1];

		}
		else {//Screen 28.20	

			request.body.extraCheck = `AND (endDt > now()) AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ( (CU.userId=${request.body.userId}) AND(CU.challengeId=challenges.challengeId) AND (CU.status = "${challengeUsersStatus.pending}") )) != 0) `;

			let asyncRes = await Promise.all([
				services.appUserChallengesLists(request.body),
				services.appUserChallengesCounts(request.body.extraCheck)
			]);

			result.requests.friends = asyncRes[0];
			result.requests.friendsCounts = asyncRes[1];

		}

	}//	Request Types
	else if(request.body.type === constants.challengeListTypes.friends) {//	Friend Ongoing Upcoming Challenges

		if(request.body.subType === constants.challengeListSubTypes.ongoing) {//Screen 28.23

			request.body.extraCheck = `AND (startDt <= now()) AND (endDt > now()) AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ((CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) AND (CU.status="${challengeUsersStatus.started}") )) != 0) `;

			let asyncRes = await Promise.all([
				services.appUserChallengesLists(request.body),
				services.appUserChallengesCounts(request.body.extraCheck)
			]);

			result.friends.ongoings = asyncRes[0];
			result.friends.ongoingsCounts = asyncRes[1];

		}
		else {//	Screen 28.24

			request.body.extraCheck = `AND (startDt > now()) AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ((CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) AND (CU.status="${challengeUsersStatus.accepted}") )) != 0) `;

			let asyncRes = await Promise.all([
				services.appUserChallengesLists(request.body),
				services.appUserChallengesCounts(request.body.extraCheck)
			]);

			result.friends.upcomings = asyncRes[0];
			result.friends.upcomingsCounts = asyncRes[1];

		}
	}//	Friend Ongoing Upcoming Challenges
	else if(request.body.type === constants.challengeListTypes.admin) {// Admin Ongoing Upcoming Challenges  - Screen 28.25
		if(request.body.subType === constants.challengeListSubTypes.ongoing) {

			request.body.extraCheck = `AND (startDt <= now()) AND (endDt > now()) AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ((CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) AND (CU.status="${challengeUsersStatus.started}") )) != 0) `;

			let asyncRes = await Promise.all([
				services.appAdminChallengesLists(request.body),
				services.appAdminChallengesCount(request.body.extraCheck)
			]);

			result.admin.ongoings = asyncRes[0];
			result.admin.ongoingsCounts = asyncRes[1];

		}
		else {//Screen 28.26

			request.body.extraCheck = `AND (endDt > now()) AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ((CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) AND (CU.status="${challengeUsersStatus.accepted}") )) != 0) `;

			let asyncRes = await Promise.all([
				services.appAdminChallengesLists(request.body),
				services.appAdminChallengesCount(request.body.extraCheck)
			]);

			result.admin.upcomings = asyncRes[0];
			result.admin.upcomingsCounts = asyncRes[1];

		}
	}// Admin Ongoing Upcoming Challenges
	else if (request.body.type === constants.challengeListTypes.history) { //	History

		if(request.body.subType === constants.challengeListSubTypes.admin) {//Screen - 28.27

			request.body.extraCheck = `AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ( (CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) AND (CU.status NOT IN ("${challengeUsersStatus.pending}", "${challengeUsersStatus.accepted}", "${challengeUsersStatus.started}", "${challengeUsersStatus.deleted}") ) )) != 0) `;

			let asyncRes = await Promise.all([
				services.appAdminChallengesLists(request.body),
				services.appAdminChallengesCount(request.body.extraCheck)
			]);

			result.history.admins = asyncRes[0];
			result.history.adminsCount = asyncRes[1];

		}
		else {//Screen - 28.28

			request.body.extraCheck = `AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ((CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) AND (CU.status NOT IN ("${challengeUsersStatus.pending}", "${challengeUsersStatus.accepted}", "${challengeUsersStatus.started}", "${challengeUsersStatus.deleted}") ) )) != 0) `;

			let asyncRes = await Promise.all([
				services.appUserChallengesLists(request.body),
				services.appUserChallengesCounts(request.body.extraCheck)
			]);

			result.history.friends = asyncRes[0];
			result.history.friendsCounts = asyncRes[1];

		}
	
	}//	History

	return responseHandler.successHandler(response, null, result);

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.listChallenges = listChallenges;

//	///		Challenge details
const challengeDetails = async (request, response) => {
try {

	let challenge = null;
	if(request.body.admin) {

		challenge = await adminChallengeDetailsService(response,
			{
				challengeId: request.body.challengeId,
				deleted: "0",
				blocked: "0"
			},
			{
				userId: request.userDetails.userId
			}
		);

	}
	else {

		challenge = await userChallengeDetailsService(response,
			{
				challengeId: request.body.challengeId,
				deleted: "0",
				blocked: "0"
			}
		);

	}
	
	return responseHandler.successHandler(response, null, {
		challenge
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.challengeDetails = challengeDetails;

//	///		Info Challenges List
const countChallenges = async (request, response) => {
try {

	let counts = await services.challengesInfoCounts({
		userId: request.userDetails.userId
	});

	let result = {
		counts: counts[0]
	};

	return responseHandler.successHandler(response, null, result);
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.countChallenges = countChallenges;

//	///		Challenge Accept or Reject
const actionUpdate = async (request, response) => {
try {
	let challenge = await services.chalDetails4Action(response, 
		{
			challengeId: request.body.challengeId,
			deleted: "0",
			blocked: "0"
		},
		{
			userId: request.userDetails.userId
		}
	);
	// let pastCheck = futureDtCheck(challenge.endDt);
	// if(!pastCheck)
	// 	return responseHandler.failedActionHandler(response, chalSerMessages.ChalEndedMsg, { challenge });

	let actionRequested = request.body.status;
	let actionMsg;
	if(actionRequested === challengeUserStatuses.accepted)
		actionMsg = "accepting";
	else
		actionMsg = "rejecting";

	let notificationType, notificationMsg;
	let resultMessage;
	if(request.body.status === challengeUserStatuses.accepted) {//		Accepting Challenge

		notificationType = notificationTypes.chalAccepted;

		notificationMsg = request.userDetails.name + response.trans({ phrase: constants.messages.ChalAcceptMsg, locale: appConfig.default.language });

		resultMessage = constants.messages.ChalAcceptedSuccess;

	}//		Accepting Challenge
	else {//		Rejecting Challenge

		notificationType = notificationTypes.chalRejected;

		notificationMsg = request.userDetails.name + response.trans({ phrase: constants.messages.ChalRejectMsg, locale: appConfig.default.language });

		resultMessage = constants.messages.ChalRejectSuccess;

	}//		Rejecting Challenge

	if(challenge.userId) {//	User Challenge Checks

		if(!challenge.challengeUser) {//	Not Invited Check
			return responseHandler.failedActionHandler(response, chalSerMessages.ChalNotInvitedAcceptErrorMsg);
		}//	Not Invited Check

		if(challenge.userId === request.userDetails.userId) {//	Challenge Creator Accepting
			return responseHandler.failedActionHandler(response, chalSerMessages.ChalCreatorAcceptErrorMsg);
		}//	Challenge Creator Accepting

		if (request.userDetails.yellowCoins < challenge.coins) {//	Insufficient Coins
			return responseHandler.failedActionHandler(response, constants.messages.InsuffientYToAcceptChallenge, 
				{ 
					currentCoins: request.userDetails.yellowCoins,
					requiredCoins: challenge.coins 
				}
			);
		}//	Insufficient Coins

		let chalUserStatus = challenge.challengeUser.status;

		if(chalUserStatus === challengeUserStatuses.accepted)//	Already Accepted
			return responseHandler.failedActionHandler(response, chalSerMessages.ChalAlreadyAcceptedMsg);
		else if(chalUserStatus === challengeUserStatuses.deleted)//	Already Rejected
			return responseHandler.failedActionHandler(response, chalSerMessages.ChalAlreadyRejectedMsg);

		if(chalUserStatus !== challengeUserStatuses.pending) {//	Not Pending	
			let message = `${chalSerMessages.ChalNotAvail4ActionMsg} ${actionMsg}`;
	
			return responseHandler.failedActionHandler(response, message, { challenge });
		}//		Not pending

		//	Transaction Service
		let acceptRejectUserChalResult = await services.acceptRejectUserChal({
			coins: challenge.coins,
			userId: request.userDetails.userId,
			ouserId: challenge.userId,
			challengeId: challenge.challengeId,
			notificationMsg,
			notificationType,
			creator: notificationCreatorTypes.user,
			challengeUserId: challenge.challengeUser.challengeUserId,
			actionRequested
		});
		//	Transaction Service

		//	Push Notification
		sendPushNotification(
			challenge.creator.fcmId,
			{
				message: notificationMsg,
				userNotificationId: `${acceptRejectUserChalResult[1].userNotificationId}`,
				pushType: notificationType,
				userId: `${request.userDetails.userId}`
			},
			{
				requestId: request.requestId,
				deviceOS: challenge.creator.deviceOS || null
			}
		);
		//	Push Notification

		if(request.body.status === challengeUserStatuses.accepted)
			userChalStartCron();

	}//	User Challenge Checks
	else {//	Admin Challenge Checks

		let message = `${chalSerMessages.ChalNotAvail4ActionMsg} ${actionMsg}`;
		if(challenge.challengeUser)
			return responseHandler.failedActionHandler(response, message, { challenge } );

		await services.acceptRejectAdminChal({
			coins: challenge.coins,
			userId: request.userDetails.userId,
			ouserId: challenge.userId,
			challengeId: challenge.challengeId,
			notificationMsg,
			notificationType,
			creator: notificationCreatorTypes.user,
			actionRequested
		});

		if(request.body.status === challengeUserStatuses.accepted)
			adminChalStartCron();

	}//	Admin Challenge Checks

	return responseHandler.successHandler(response, resultMessage);
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.actionUpdate = actionUpdate;
