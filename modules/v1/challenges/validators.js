const BaseJoi = require("@hapi/joi");
const Extension = require("@hapi/joi-date");

const Joi = BaseJoi.extend(Extension);

const apiRef = "challengeValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

const { challengeListTypes, challengeListSubTypes } = require("./constants");

const { challengeUserStatuses } = require("../../../services/challengeUsers");

const { latSchema, lngSchema } = require("../../../utils/joiSchemas");

// ///		Create Challenge
exports.createChallenge = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/createChallenge`;

	let schema = Joi.object().keys({
		"ouserId": Joi.number().min(1).required(),

		"startAddress": Joi.string().max(255).required(),
		"startLatitude": latSchema.required(),
		"startLongitude": lngSchema.required(),

		"endAddress": Joi.string().max(255).required(),
		"endLatitude": latSchema.required(),
		"endLongitude": lngSchema.required(),

		"totalDistance": Joi.number().min(0).required(),
		"timezone": Joi.string().max(50).required(),

		"startDt": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),

		"totalDays": Joi.number().min(1).required(),
		
		"directions": Joi.string().optional(),
		"track": Joi.string().optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//  ///     Delete Challenge
exports.deleteChallenge = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/deleteChallenge`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//  ///     User Challenges List
exports.listChallenges = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/listChallenges`;

	let schema = Joi.object().keys({
		"type": Joi.string().valid(challengeListTypes.requests, challengeListTypes.friends, challengeListTypes.admin, challengeListTypes.history).required(),
		"subType": Joi.string().valid(challengeListSubTypes.admin, challengeListSubTypes.friend).required(),
		"page": Joi.number().min(0).optional(),
		"limit": Joi.number().min(1).optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId });
	if (valCheck === true) next();
};

//  ///     User Challenge Details
exports.challengeDetails = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/challengeDetails`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().required(),
		"admin": Joi.string().valid("0", "1").optional()
	});

	request.body = {
		...request.body,
		...request.params
	};

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId });
	if (valCheck === true) next();

};

//  ///     User Challenges Counts Info
exports.countChallenges = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/countChallenges`;

	next();
};

//  ///     Challenge Status Update
exports.actionUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/actionUpdate`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().required(),
		"status": Joi.string().valid(challengeUserStatuses.accepted, challengeUserStatuses.deleted).required()
	});

	request.body = {
		...request.body,
		...request.params
	};

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId });
	if (valCheck === true) next();

};