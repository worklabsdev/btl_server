var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validator = require("./validators");
const controllers = require("./controllers");

router.post("/create", verifyJWTMiddleware, validator.createChallenge, controllers.createChallenge);

router.delete("/delete", verifyJWTMiddleware, validator.deleteChallenge, controllers.deleteChallenge);

router.post("/list", verifyJWTMiddleware, validator.listChallenges, controllers.listChallenges);

router.post("/details", verifyJWTMiddleware, validator.challengeDetails, controllers.challengeDetails);

router.get("/counts", verifyJWTMiddleware, validator.countChallenges, controllers.countChallenges);

router.post("/action", verifyJWTMiddleware, validator.actionUpdate, controllers.actionUpdate);

module.exports = router;
