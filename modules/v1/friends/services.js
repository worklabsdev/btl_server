var Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { userProfilePic, userCoverPic, userGoogleLinked, searchUserWhereRaw } = require("../../../services/userServices");
const { fReqStatuses } = require("../../../services/friendsServices");
const { appDevicesListAtts } = require("../../../services/devices").attributes;

const { failedActionHandler } = require("../../../commonFuncs/responseHandler");
const { ouserNotAvail } = require("../../../properties/constant").globalMessages.responseMessages;

const { sequelizeAtts, messages, friendListTypes } = require("./constants");

const { pageOffsetCreator } = require("../../../utils/common");
// const imageFullPath = [Sequelize.col(`CONCAT(${})`), "image"];

var Db = require("../../../models");// ///// DB Models

//	///Other User Details
exports.friendDetails = (response, whereJSON, attributes=sequelizeAtts.userProfile) => {
	return new Promise(async (resolve, reject) => {
		try {
		let user = await Db.users.findOne({
			where: whereJSON,
			attributes,
			include: [
				{
					model: Db.userDevices,
					as: "userDevice",
					required: false,
					where: { deleted: "0", isValid: "1" },
					attributes: sequelizeAtts.userDevicesAtts,
					include: [
						{
							model: Db.devices,
							as: "device",
							where: { blocked: "0" },
							attributes: appDevicesListAtts
						}
					]
				}
			],
			//logging: true,
			//raw: true
		});
			if (!user) return failedActionHandler(response, ouserNotAvail);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

// /// User Details for Friend Request
exports.getUser4FriendRequest = (response, opts) => {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await Db.users.findOne({

				where: Sequelize.literal(`(userId=${opts.ouserId}) AND (deleted="0") AND (blocked="0") AND (phoneVerified="1") AND ((SELECT COUNT(*) FROM userBlockeds UB WHERE UB.deleted="0" AND (UB.userId=${opts.ouserId} AND UB.ouserId=${opts.userId}) OR (UB.userId=${opts.userId} AND UB.ouserId=${opts.ouserId}) ) = 0)`),
				attributes: [
					"userId", "name", "fcmId", "socketId", "deviceOS",
					[Sequelize.literal(`(SELECT status FROM userFriends UF1 WHERE UF1.userId=${opts.userId} AND UF1.ouserId=${opts.ouserId} LIMIT 0, 1)`), "userSentRequest"],
					[Sequelize.literal(`(SELECT status FROM userFriends UF1 WHERE UF1.userId=${opts.ouserId} AND UF1.ouserId=${opts.userId} LIMIT 0, 1)`), "userReceivedRequest"]
				],
				raw: true,
				// logging: console.log
			});
			if (!user) return failedActionHandler(response, ouserNotAvail);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

//  ///         Create Record
exports.createRecord = (data) => {
	return Db.userFriends.create(data);
};

//  ///         Update Record
exports.updateFReq = (whereJSON, data) => {
	return Db.userFriends.update(
		data,
		{
			where: whereJSON,
			// logging: console.log
		}
	).then((result) => {
		return Db.userFriends.findOne({
			where: whereJSON
		});
	});
};

//  ///     Delete Record
exports.deleteFReq = (whereJSON) => {
	return Db.userFriends.destroy({
		where: whereJSON
	});
};

exports.getUserForChecks = (currentUserId, otherUserId) => {
	return Db.users.findOne({
		where: {
			userId: otherUserId,
			deleted: "0",
			blocked: "0"
		},
		attributes: sequelizeAtts.users,
		include: [
			{
				as: "userFriendsSent",
				model: Db.userFriends,
				where: {
					userId: otherUserId
				},
				required: false,
				attributes: sequelizeAtts.userFriends
			},
			{
				as: "userFriendsRec",
				model: Db.userFriends,
				where: {
					ouserId: currentUserId
				},
				required: false,
				attributes: sequelizeAtts.userFriends
			}
		]
	});
};

//	///		Add Friends List
exports.addFriendsList = (searchWhere, opts) => {

	let findJSON = {
		where: Sequelize.literal(`${searchWhere}`),
		attributes: sequelizeAtts.friendListUsers,

		include: [
			{
				model: Db.userFriends,
				as: "userFriendsSent",
				required: false,
				attributes: sequelizeAtts.userFriends,
				where: { ouserId: opts.userId }
			},
			{
				model: Db.userFriends,
				as: "userFriendsRec",
				required: false,
				attributes: sequelizeAtts.userFriends,
				where: { userId: opts.userId }
			}
		],
		//logging: console.log
	};


	if(opts.type === friendListTypes.local) {//	If Pagination Needed
		opts = pageOffsetCreator(opts);

		findJSON["offset"] = opts.offset;
		findJSON["limit"] = opts.limit;
	}//	If Pagination Needed

	return Db.users.findAll(findJSON);

};

//	///		Received Friend Request Users
exports.receivedRequestUsers = (opts) => {
	opts = pageOffsetCreator(opts);//	For Paginations

	return Db.users.findAll({
		where: Sequelize.literal(`deleted="0" AND blocked="0" AND users.userId <> ${opts.userId}`),
		attributes: sequelizeAtts.friendListUsers,
		include: [
			{
				model: Db.userFriends,
				as: "userFriendsSent",
				required: true,
				attributes: sequelizeAtts.userFriends,
				on: Sequelize.literal(`(status = "${fReqStatuses.pending}") AND 
				((userFriendsSent.userId = ${opts.userId}) AND (userFriendsSent.ouserId = users.userId))
					OR
				((userFriendsSent.ouserId = ${opts.userId}) AND (userFriendsSent.userId = users.userId))
				`)
			}
		],
		offset: opts.offset,
		limit: opts.pageSize,
		// logging: console.log,
		order: [
			["userFriendsSent", "userFriendId", "DESC"]
		]
	});
};

//	///		User All Friends
exports.userAllFriends = async (opts) => {

	opts = pageOffsetCreator(opts);//	For Paginations
	let searchRaw = searchUserWhereRaw(opts.search);

	return Db.sequelize.query(`
	SELECT 
		users.userId, users.name, users.email, users.phoneCode, users.phoneNumber, users.username, users.dob, users.gender, users.address, users.city, users.state, users.country, users.countryISO2, users.zipcode, ${userProfilePic}, ${userCoverPic}, UF.userFriendId, UF.status, UF.userId as requestSenderId
		FROM users 
		JOIN userFriends UF ON (
			((UF.userId=${opts.userId} AND UF.ouserId=users.userId) OR 
			(UF.ouserId=${opts.userId} AND UF.userId=users.userId)) AND 
			UF.status="${fReqStatuses.accepted}") 
		WHERE users.blocked="0" AND users.deleted="0" AND users.userId != ${opts.userId} ${searchRaw}
		HAVING ( (SELECT COUNT(*) FROM userBlockeds UB WHERE UB.deleted="0" AND ( (UB.userId=${opts.userId} AND UB.ouserId=users.userId) OR (UB.ouserId=${opts.userId} AND UB.userId=users.userId) ) ) = 0)
		ORDER BY UF.updatedAt DESC
		LIMIT ${opts.offset}, ${opts.pageSize}
	`, { type: Sequelize.QueryTypes.SELECT });

};

//	Friend Request Details
exports.friendReqDetails = (response, whereJSON) => {
	return new Promise(async (resolve, reject) => {
		try {
			let friendReq = await Db.userFriends.findOne(
				{
					where: whereJSON,
					include: [
						{
							model: Db.users,
							as: "ouser",
							attributes: ["userId", "name", "username", "fcmId", "socketId"]
						},
						{
							model: Db.users,
							as: "user",
							attributes: ["userId", "name", "username", "fcmId", "socketId"]
						}
					]
					//attributes: sequelizeAtts.userProfile
				},
				{
					raw: true
				}
			);
			if (!friendReq) return failedActionHandler(response, messages.FReqNotAvailMsg);
			return resolve(friendReq);
		} catch (error) {
			return reject(error);
		}
	});
};