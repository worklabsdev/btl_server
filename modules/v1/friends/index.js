var express = require("express");

var router = express.Router();

const { verifyJWTMiddleware } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/profile", verifyJWTMiddleware, validators.ouserProfile, controllers.ouserProfile);

router.post("/sendRequest", verifyJWTMiddleware, validators.sendRequest, controllers.sendRequest);

router.post("/updateRequest", verifyJWTMiddleware, validators.updateRequest, controllers.updateRequest);

router.post("/removeFriend", verifyJWTMiddleware, validators.removeFriend, controllers.removeFriend);

router.post("/addList", verifyJWTMiddleware, validators.addList, controllers.addListNew);

router.post("/receivedList", verifyJWTMiddleware, validators.receivedList, controllers.receivedList);

router.post("/list", verifyJWTMiddleware, validators.list, controllers.list);

router.delete("/delete", verifyJWTMiddleware, validators.deleteRequest, controllers.deleteRequest);

module.exports = router;
