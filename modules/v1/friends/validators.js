const Joi = require("@hapi/joi");

const apiRef = "friendValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

const { fReqStatuses } = require("../../../services/friendsServices");

const { friendListTypes } = require("./constants");

// ///		Other User Profile
exports.ouserProfile = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/ouserProfile`;

	let schema = Joi.object().keys({
		"ouserId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Send Friend Request
exports.sendRequest = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/sendRequest`;

	let schema = Joi.object().keys({
		"ouserId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///		Accept Reject Request
exports.updateRequest = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/updateRequest`;

	let schema = Joi.object().keys({
		"ouserId": Joi.number().min(1).required(),
		"status": Joi.string().valid(fReqStatuses.accepted, fReqStatuses.rejected).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///		Accept Reject Request
exports.removeFriend = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/removeFriend`;

	let schema = Joi.object().keys({
		"ouserId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Add Friend List
exports.addList = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addList`;

	let schema = Joi.object().keys({
		"type": Joi.string().valid(friendListTypes.facebook, friendListTypes.google, friendListTypes.local, friendListTypes.phonebook).required(),

		"fbFriends": Joi.when("type", {
			"is": Joi.exist().equal(friendListTypes.facebook),
			"then": Joi.array().items(Joi.string().required()).required(),
			"otherwise": Joi.forbidden(),
		}),
		"phoneNumbers": Joi.when("type", {
			"is": Joi.exist().equal(friendListTypes.phonebook),
			"then": Joi.array().items(Joi.string().required()).required(),
			"otherwise": Joi.forbidden(),
		}),
		"googleFriends": Joi.when("type", {
			"is": Joi.exist().equal(friendListTypes.google),
			"then": Joi.array().items(Joi.string().required()).required(),
			"otherwise": Joi.forbidden(),
		}),

		"search": Joi.string().optional(),

		"page": Joi.number().min(0).optional(),
		"limit": Joi.number().min(1).optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Received Friend Request
exports.receivedList = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/receivedList`;

	let schema = Joi.object().keys({
		"page": Joi.number().min(0).optional(),
		"limit": Joi.number().min(1).optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		All Friends List
exports.list = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/list`;

	let schema = Joi.object().keys({
		"page": Joi.number().min(0).optional(),
		"limit": Joi.number().min(1).optional(),
		"search": Joi.string().optional(),
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Delete Friend Request
exports.deleteRequest = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/deleteRequest`;

	let schema = Joi.object().keys({
		"userFriendId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};