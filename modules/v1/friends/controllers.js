var Sequelize = require("sequelize");

const responseHandler = require("../../../commonFuncs/responseHandler");

let services = require("./services");
let constants = require("./constants");

const { createNotification, updateNotificationsSer } = require("../../../services/userNotifications");
const { searchUserWhereRaw } = require("../../../services/userServices");
const { fReqStatuses } = require("../../../services/friendsServices");
const { userMedalsLevelDbCalculator } = require("../../../services/medalServices");
const { challengeUserStatuses } = require("../../../services/challengeUsers");

const { ouserNotAvail } = require("../../../properties/constant").globalMessages.responseMessages;
const { notificationTypes, notificationCreatorTypes } = require("../../../properties/constant");

const { sendPushNotification } = require("../../../packages/fcm/");

//	///	Other User Profile
const ouserProfile = async (request, response) => {
	try {

		let attributes = ["profilePic", "coverPic", "userId", "name", "username", "phoneCode", "phoneNumber", "email", "facebookId", "googleId", "dob", "address", "state", "city", "country", "countryISO2", "zipcode", "addressLatitude", "addressLongitude", "gender", "yellowCoins", "greenCoins", "totalGreenCoins", "totalDistance", "totalSteps", "referalUserId", "avail", "availDt", "availDays", "phoneVerified", "challengesPlayed", "challengesWon", "challengesCompleted", "deviceOS", "lastSyncDt",
		[Sequelize.literal(`(SELECT COUNT(*) FROM challenges C JOIN challengeUsers CU ON (CU.challengeId=C.challengeId AND CU.userId=${request.body.ouserId} AND CU.status="${challengeUserStatuses.started}") WHERE C.deleted="0" AND C.blocked="0")`), "activeChallengesCount"]
		];

		let ouser = await services.friendDetails(response, { userId: request.body.ouserId }, attributes);

				//	Level Medals Calculator
		let levelData = await userMedalsLevelDbCalculator({
			totalGreenCoins: ouser.totalGreenCoins
		});
		//	Level Medals Calculator

		return responseHandler.successHandler(response, null, { ouser, levelData });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.ouserProfile = ouserProfile;

//  /// Send Request
const sendRequest = async (request, response) => {
	try {
		request.body.ouserId = parseInt(request.body.ouserId);

		if (request.userDetails.userId === request.body.ouserId) return responseHandler.failedActionHandler(response, constants.messages.SendReq2SelfErr);

		let oUser = await services.getUser4FriendRequest(response, {
			ouserId: request.body.ouserId,
			userId: request.userDetails.userId
		});

		if (oUser.userSentRequest !== null) { // ///   Req Already Sent
			if (oUser.userSentRequest === fReqStatuses.pending) return responseHandler.failedActionHandler(response, constants.messages.FReqStillPendingByOUser);
			if (oUser.userSentRequest === fReqStatuses.accepted) return responseHandler.failedActionHandler(response, constants.messages.AlreadyFriend);
		}// ///   Req Already Sent
		else if (oUser.userReceivedRequest !== null) { // /// Req Already Received
			if (oUser.userReceivedRequest === fReqStatuses.pending) return responseHandler.failedActionHandler(response, constants.messages.FReqStillPendingBySUser);
			if (oUser.userReceivedRequest === fReqStatuses.accepted) return responseHandler.failedActionHandler(response, constants.messages.AlreadyFriend);
		}// /// Req Already Received

		let fReq = await services.createRecord({
			userId: request.userDetails.userId,
			ouserId: request.body.ouserId,
			status: fReqStatuses.pending
		});

		// ///      Push Notification
		let message = request.userDetails.name + response.trans({ phrase: constants.messages.fReqPush, locale: appConfig.default.language });

		let type = notificationTypes.fReqSent;
		let creator = notificationCreatorTypes.user;

		let uNotification = await createNotification({
			userId: request.body.ouserId,
			ouserId: request.userDetails.userId,
			message,
			userFriendId: fReq.userFriendId,
			type,
			creator
		});

		sendPushNotification(
			oUser.fcmId,
			{
				message,
				userNotificationId: `${uNotification.userNotificationId}`,
				pushType: type,
				userId: `${request.userDetails.userId}`
			},
			{
				requestId: request.requestId,
				deviceOS: oUser.deviceOS || null
			}
		);
		// ///      Push Notification

		return responseHandler.successHandler(response, constants.messages.FReqSent);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.sendRequest = sendRequest;

//  ///     Accept Reject Request
const updateRequest = async (request, response) => {
	try {
		let oUser = await services.getUser4FriendRequest(response, {
			ouserId: request.body.ouserId,
			userId: request.userDetails.userId
		});
		if (!oUser.userReceivedRequest) return responseHandler.failedActionHandler(response, constants.messages.FReqNotValid);
		if (oUser.userReceivedRequest === fReqStatuses.accepted) return responseHandler.failedActionHandler(response, constants.messages.FReqAlreadyAccepted);

		await updateNotificationsSer(
			{
				userId: request.userDetails.userId,
				ouserId: request.body.ouserId,
				type: notificationTypes.fReqSent,
				deleted: "0"
			},
			{
				deleted: "1",
				isRead: "1"
			}
		);

		if (request.body.status === fReqStatuses.rejected) { // /// Rejecting Request
			let deleteFReq = await services.deleteFReq({
				userId: request.body.ouserId,
				ouserId: request.userDetails.userId
			});

			return responseHandler.successHandler(response, constants.messages.FReqRejected);
		}// /// Rejecting Request

		let updateFReq = await services.updateFReq(
			{
				userId: request.body.ouserId,
				ouserId: request.userDetails.userId
			},
			{
				status: fReqStatuses.accepted
			}
		);

		// ///      Push Notification
		let message = request.userDetails.name + response.trans({ phrase: constants.messages.FReqAcceptedPush, locale: appConfig.default.language });

		let type = notificationTypes.fReqAccepted;
		let creator = notificationCreatorTypes.user;

		let uNotification = await createNotification({
			userId: request.body.ouserId,
			ouserId: request.userDetails.userId,
			message,
			userFriendId: updateFReq.userFriendId || 0,
			type,
			creator
		});

		sendPushNotification(
			oUser.fcmId,
			{
				message,
				userNotificationId: `${uNotification.userNotificationId}`,
				pushType: type,
				userId: `${request.userDetails.userId}`
			},
			{
				requestId: request.requestId,
				deviceOS: oUser.deviceOS || null
			}
		);
		// /      Push Notification

		return responseHandler.successHandler(response, constants.messages.fReqAccepted);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.updateRequest = updateRequest;

// ///      Accept Reject Request
const removeFriend = async (request, response) => {
	try {
		let ouser = await services.getUserForChecks(request.userDetails.userId, request.body.ouserId);
		if (!ouser) return responseHandler.failedActionHandler(response, ouserNotAvail);

		if (!ouser.userFriendsSent && !ouser.userFriendsRec) return responseHandler.failedActionHandler(response, constants.messages.FReqNotValid);

		let userFriendId;
		if (ouser.userFriendsSent) userFriendId = ouser.userFriendsSent.userFriendId;
		else userFriendId = ouser.userFriendsRec.userFriendId;

		await Promise.all([
			services.deleteFReq({
				userFriendId
			}),

			updateNotificationsSer(
				{ userFriendId },
				{
					deleted: "1",
					isRead: "1"
				}
			)
		]);

		return responseHandler.successHandler(response, constants.messages.FRemovedSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.removeFriend = removeFriend

//	///		Add Friend List
const addListNew = async (request, response) => {
	try {
		let searchWhere = `(users.userId != ${request.userDetails.userId}) AND (users.deleted="0") AND (blocked="0") AND (phoneVerified="1") AND (name != "")`;

		if (request.body.search) {
			searchWhere = `${searchWhere} ${searchUserWhereRaw(request.body.search)}`;
		}

		let users = [];

		let key = null;
		let requestUsers = [];
		if (request.body.type === constants.friendListTypes.facebook) { //	///	FB Friends
			request.body.fbFriends = JSON.parse(request.body.fbFriends);
			searchWhere = `${searchWhere} AND users.facebookId IN (${request.body.fbFriends})`;// FB Check Add

			key = "facebookId";
			requestUsers = request.body.fbFriends;
		}//	///	FB Friends
		else if (request.body.type === constants.friendListTypes.phonebook) { //	///	PhoneBook User
			request.body.phoneNumbers = JSON.parse(request.body.phoneNumbers);
			searchWhere = `${searchWhere} AND users.phoneNumber IN (${request.body.phoneNumbers})`;// Phone Book Check Add

			key = "phoneNumber";
			requestUsers = request.body.phoneNumbers;
		}
		else if (request.body.type === constants.friendListTypes.google) { //	///	Google User List
			request.body.googleFriends = JSON.parse(request.body.googleFriends);
			searchWhere = `${searchWhere} AND users.googleId IN (${request.body.googleFriends})`;// FB Check Add

			key = "googleId";
			requestUsers = request.body.googleFriends;
		}//	///	Google User List

		let opts = {
			userId: request.userDetails.userId,
			...request.body
		};

		users = await services.addFriendsList(searchWhere, opts);
		let requestUsersObj = {};
		if(key) {//	Format Users

			//	Users objects Create
			requestUsers.forEach(requestUser => {
				requestUsersObj[requestUser] = {
					userId: 0
				};
			});
			//	Users objects Create

			// let tempUsers = [];
			users.forEach((user, index) => {
				requestUsersObj[user[key]] = user;
			});

		}//	Format user

		return responseHandler.successHandler(response, null, { requestUsersObj, users });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.addListNew = addListNew;

//	///		Add Friend List
const addList = async (request, response) => {
	try {
		let searchWhere = `(users.userId != ${request.userDetails.userId}) AND (users.deleted="0") AND (blocked="0") AND (phoneVerified="1") AND (name != "")`;

		if (request.body.search) {
			searchWhere = `${searchWhere} ${searchUserWhereRaw(request.body.search)}`;
		}

		let users = [];
		if (request.body.type === constants.friendListTypes.facebook) { //	///	FB Friends
			request.body.fbFriends = JSON.parse(request.body.fbFriends);
			searchWhere = `${searchWhere} AND users.facebookId IN (${request.body.fbFriends})`;// FB Check Add
		}//	///	FB Friends
		else if (request.body.type === constants.friendListTypes.phonebook) { //	///	PhoneBook User
			request.body.phoneNumbers = JSON.parse(request.body.phoneNumbers);
			searchWhere = `${searchWhere} AND users.phoneNumber IN (${request.body.phoneNumbers})`;// Phone Book Check Add
		}
		else if (request.body.type === constants.friendListTypes.google) { //	///	Google User List
			request.body.googleFriends = JSON.parse(request.body.googleFriends);
			searchWhere = `${searchWhere} AND users.googleId IN (${request.body.googleFriends})`;// FB Check Add
		}//	///	Google User List

		let opts = {
			userId: request.userDetails.userId,
			...request.body
		};

		users = await services.addFriendsList(searchWhere, opts);

		return responseHandler.successHandler(response, null, { users });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.addList = addList;

//	///		Received Request List
const receivedList = async (request, response) => {
try {
	let opts = {
		userId: request.userDetails.userId,
		...request.body
	};

	let users = await services.receivedRequestUsers(opts);

	return responseHandler.successHandler(response, null, { users });
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.receivedList = receivedList;

//	///			All Friends List
const list = async (request, response) => {
try {
	let opts = {
		userId: request.userDetails.userId,
		...request.body
	};

	var friends = await services.userAllFriends(opts);

	return responseHandler.successHandler(response, null, { friends });
}
catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.list = list;

//	Delete Request
const deleteRequest = async (request, response) => {
try {

	let friendReqDetails = await services.friendReqDetails(
		response,
		{
			userFriendId: request.body.userFriendId
		}
	);
	if(friendReqDetails.userId !== request.userDetails.userId)
		return responseHandler.failedActionHandler(response, constants.messages.FReqDelUserCannotMsg);
	else if(friendReqDetails.status !== fReqStatuses.pending)
		return responseHandler.failedActionHandler(response, constants.messages.FReqDelNotPendingMsg, { status: friendReqDetails.status });

	await Promise.all([
		services.deleteFReq({
			userFriendId: request.body.userFriendId
		}),
		updateNotificationsSer(
			{ userFriendId: request.body.userFriendId },
			{
				isRead: "1",
				deleted: "1"
			}
		)
	]);

	return responseHandler.successHandler(response, constants.messages.FReqDeleteSuccessMsg);
}
catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.deleteRequest = deleteRequest;