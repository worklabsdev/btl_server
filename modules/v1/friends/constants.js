const { userProfilePicSeq } = require("../../../services/userServices");

const defaultValues = {
};

const friendListTypes = {
	"facebook": "facebook",
	"google": "google",
	"local": "local",
	"phonebook": "phonebook"
};

const messages = {
	"SendReq2SelfErr": "Sorry, you cannot send request to yourself",

	"FReqStillPendingByOUser": "Friend request is already sent, and is pending",
	"AlreadyFriend": "You are already friend with this user",
	"FReqStillPendingBySUser": "Please accept the friend request from the user",
	"FReqSent": "Friend request sent successfully",
	"fReqPush": " has sent you friend request",
	"FReqNotValid": "Sorry, this friend request is no longer valid",
	"FReqRejected": "Friend request rejected successfully",
	"fReqAccepted": "Friend request accepted successfully",
	"FReqAcceptedPush": " has accepted your friend request",
	"FReqAlreadyAccepted": "You have already accepted this friend request",
	"FRemovedSuccess": "Friend list updated successfully",

	"FReqDeleteSuccessMsg": "Friend request deleted successfully",
	"FReqNotAvailMsg": "This friend request is currently not available",
	"FReqNotPendingMsg": "Sorry, this friend request is no longer pending",
	"CannotDelMsg": "Sorry, this request can no longer be deleted",
	"FReqDelUserCannotMsg": "Sorry, friend request can only be deleted by sender",
	"FReqDelNotPendingMsg": "Sorry, friend request can only be deleted until pending"
};

const sequelizeAtts = {
	"users": ["userId", "name", "fcmId", "timezone"],
	"userProfile": {
		"exclude": ["salt", "accessToken", "currentLatitude", "currentLongitude", "fcmId", "socketId", "password", "createdAt", "updatedAt", "timezone", "otp", "otpValidity", "blocked", "deleted", "totalYellowCoins", "inviteCode"]
	},
	"userFriends": ["userFriendId", "userId", "ouserId", "status", "createdAt"],

	"friendListUsers": ["userId", "name", "email", "facebookId", "googleId", "username", "phoneCode", "phoneNumber", "profilePic", "address", "city", "state", "country", "countryISO2", "zipcode"]
};



module.exports = {
	defaultValues,
	messages,
	friendListTypes,
	sequelizeAtts
};
