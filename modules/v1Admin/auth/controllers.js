const Op = require("sequelize").Op;

const responseHandler = require("../../../commonFuncs/responseHandler");

const { verifyPassword, generatePassword } = require("../../../commonFuncs/password");
const { generateJWT } = require("../../../commonFuncs/auth");
const { generateRandStr } = require("../../../commonFuncs/commonFuncs");

const { fileUpload } = require("../../../packages/upload");

const { globalMessages } = require("../../../properties/constant");

const { messages } = require("./constants");

const services = require("./services");
const { loginTypes } = require("../../../services/userServices");
const { createLoginHist, updateLoginHist } = require("../../../services/loginHists");

//	///	Login
const login = async (request, response) => {
	try {
		let admin = await services.adminGet(response, {
			email: request.body.email
		});

		let verifyPass = await verifyPassword(request.body.password, admin.password);
		if (!verifyPass) return responseHandler.failedActionHandler(response, globalMessages.responseMessages.invalidEmailOrPass);

		delete request.body.email;
		delete request.body.password;

		// ///   JWT Generate
		let auth = generateJWT({
			adminId: admin.adminId,
			email: admin.email,
			name: admin.name,
			profilePic: admin.profilePic
		});

		await Promise.all([
			services.adminUpdate(//	Admin Token Update
				response,
				{ adminId: admin.adminId },
				{
					accessToken: auth
				}
			),//	Admin Token Update
			updateLoginHist(//	Login History Old Update
				{
					adminId: admin.adminId,
					isValid: "1",
					accessToken: { [Op.ne]: auth }
				},
				{
					isValid: "0"
				}
			),//	Login History Old Update
			createLoginHist({//	Login History Create
				adminId: admin.adminId,
				latitude: request.body.currentLatitude,
				longitude: request.body.currentLongitude,
				timezone: request.body.timezone,
				accessToken: auth,
				odata: {
					type: loginTypes.normal,
					ip: request.ip,
					device: request.device
				}
			})//	Login History Create
		]);

		admin = await services.adminSignInResponse({
			adminId: admin.adminId
		});

		return responseHandler.successHandler(response, messages.LoggedInSuccess, { auth, admin });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

//	///	Reset Password
const resetPassword = async (request, response) => {
	try {
		let admin = await services.adminGet(response, {
			email: request.body.email
		});

		let password = generateRandStr(10); //"asdfmnbvc"; // generateRandStr(10);
		let passwordHashed = generatePassword(password);
		passwordHashed = passwordHashed.hash;

		let updateAdmin = await services.adminUpdate(
			response,
			{ adminId: admin.adminId },
			{
				password: passwordHashed
			}
		);

		return responseHandler.successHandler(response, messages.ResetPasswordSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

//	///	Password Update
const passwordUpdate = async (request, response) => {
	try {
		let verifyPass = await verifyPassword(request.body.oldPassword, request.adminDetails.password);
		if (!verifyPass) return responseHandler.failedActionHandler(response, messages.InvalidOldPassword);

		if (request.body.newPassword === request.body.oldPassword) return responseHandler.failedActionHandler(response, messages.NewPasswordOldMatch);

		let passwordHashed = generatePassword(request.body.newPassword);
		passwordHashed = passwordHashed.hash;

		let updateAdmin = await services.adminUpdate(
			response,
			{ adminId: request.adminDetails.adminId },
			{
				password: passwordHashed
			}
		);

		return responseHandler.successHandler(response, messages.PasswordUpdateSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

//	///	Profile Update
const profileUpdate = async (request, response) => {
	try {
		// request.body.profilePic = request.adminDetails.profilePic;
		if (request.files.profilePic) request.body.profilePic = await fileUpload(request.files.profilePic, "Profile Pic");

		let updateAdmin = await services.adminUpdate(
			response,
			{ adminId: request.adminDetails.adminId },
			request.body
		);

		let admin = await services.adminSignInResponse({
			adminId: request.adminDetails.adminId
		});

		return responseHandler.successHandler(response, messages.ProfileUpdateSuccess, { admin });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

module.exports = {
	login,
	resetPassword,
	passwordUpdate,
	profileUpdate
};
