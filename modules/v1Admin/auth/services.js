const { authHandler, failedActionHandler } = require("../../../commonFuncs/responseHandler");

var Db = require("../../../models");// ///// DB Models

const { globalMessages } = require("../../../properties/constant");

const { sequelizeAtts } = require("./constants");

//     ///      Admin Get
exports.adminGet = (response, whereJSON) => {
	return Db.admins.findOne({
		where: whereJSON
	}).then((result) => {
		if (!result) return authHandler(response, 4);
		return result;
	})
		.catch((error) => {
			throw new Error(error.message);
		});
};

//		///		Update Admin
exports.adminUpdate = (response, whereJSON, data) => {
	let updatedOjs = {
		...data
	};

	if (!Object.keys(updatedOjs).length) throw Error(globalMessages.responseMessages.nothingToUpdate);

	return Db.admins.update(
		updatedOjs,
		{
			where: whereJSON,
			// logging: console.log
		}
	)
		.catch((error) => {
			throw new Error(error.message);
		});
};

//	///		 Sign In Response
exports.adminSignInResponse = (whereJSON) => {
	return Db.admins.findOne({
		where: whereJSON,
		attributes: sequelizeAtts.adminSignInAtts
	});
};
