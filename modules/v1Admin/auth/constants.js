let sequelizeAtts = {
	"adminSignInAtts": {
		"exclude": ["password", "createdAt", "updatedAt", "adminId"],
	}
};

const messages = {
	"LoggedInSuccess": "Logged in successfully",
	"ResetPasswordSuccess": "New password has been sent to your mail",
	"PasswordUpdateSuccess": "Password updated successfully",
	"InvalidOldPassword": "Please enter the correct old password",
	"NewPasswordOldMatch": "New password cannot be same as old password",
	"ProfileUpdateSuccess": "Profile updated successfully"
};

module.exports = {
	sequelizeAtts,
	messages
};
