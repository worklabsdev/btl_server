var express = require("express");

var router = express.Router();

const { verifyJWTMiddlewareAdmin } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/login", validators.login, controllers.login);

router.post("/reset/password", validators.resetPassword, controllers.resetPassword);

router.post("/password/update", verifyJWTMiddlewareAdmin, validators.passwordUpdate, controllers.passwordUpdate);

router.post("/profile/update", multipartMiddleware, verifyJWTMiddlewareAdmin, validators.profileUpdate, controllers.profileUpdate);

module.exports = router;
