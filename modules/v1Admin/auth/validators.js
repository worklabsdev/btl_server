const Joi = require("@hapi/joi");

const apiRef = "AdminAuthValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

// ///       Login Validation
module.exports.login = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/login`;

	let schema = Joi.object().keys({
		"email": Joi.string().email({ minDomainSegments: 2 }).required(),
		"password": Joi.string().min(6).required(),
		// timezone: Joi.string().max(50).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       resetPassword Validation
module.exports.resetPassword = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/resetPassword`;

	let schema = Joi.object().keys({
		"email": Joi.string().email({ minDomainSegments: 2 }).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Password Update Validation
module.exports.passwordUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/passwordUpdate`;

	let schema = Joi.object().keys({
		"oldPassword": Joi.string().min(6).required()
			.label("Old Password"),
		"newPassword": Joi.string().min(6).required()
			.label("New Password"),
		"newPasswordConfirmation": Joi.any().valid(Joi.ref("newPassword")).required().label("New Password Confirmation")
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Profile Update Validation
module.exports.profileUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/profileUpdate`;

	let schema = Joi.object().keys({
		"name": Joi.string().required().label("Name").trim(),
		"email": Joi.optional()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};
