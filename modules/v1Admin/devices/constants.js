let sequelizeAtts = {
	"devicesList": {
		"include": ["deviceId", "name", "blocked", "updatedAt"],
	}
};

const messages = {
	"DeviceBlockSuccess": "Device blocked successfully",
	"DeviceUnBlockSuccess": "Device unblocked successfully"
};

module.exports = {
	sequelizeAtts,
	messages
};
