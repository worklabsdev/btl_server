var express = require("express");

var router = express.Router();

const { verifyJWTMiddlewareAdmin } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/all", verifyJWTMiddlewareAdmin, validators.list, controllers.list);

router.post("/block", verifyJWTMiddlewareAdmin, validators.block, controllers.block);

module.exports = router;
