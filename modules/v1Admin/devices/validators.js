const Joi = require("@hapi/joi");

const apiRef = "AdminDevicesValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

const { listSortedSchema, listFilteredArraySchema } = require("../../../utils/joiSchemas");

// ///       Listing
module.exports.list = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/lists`;

	let schema = Joi.object().keys({
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Block Unblock
module.exports.block = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/block`;

	let schema = Joi.object().keys({
		"id": Joi.number().min(1).required(),
		"block": Joi.string().valid("0", "1").required(),
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};
