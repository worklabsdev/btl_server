var Sequelize = require("sequelize");

var Db = require("../../../models");// ///// DB Models

const { sequelizeAtts } = require("./constants");

//  ///     Admin Devices List
exports.devicesList = (data) => {
	return Db.devices.findAll({
		where: Sequelize.literal(data.listWhereRawSeq),
		attributes: sequelizeAtts.devicesList.include,
		offset: data.offset,
		limit: data.pageSize,
		order: data.sortByArray,
		// logging: console.log
	});
};

//  ///     Admin Devices Count
exports.devicesCount = (listWhereRawSeq) => {
	return Db.devices.count({
		where: Sequelize.literal(listWhereRawSeq)
	});
};
