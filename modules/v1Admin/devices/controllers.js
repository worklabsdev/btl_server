const constants = require("./constants");
const services = require("./services");

const responseHandler = require("../../../commonFuncs/responseHandler");

const { pageOffsetCreator } = require("../../../utils/common");
const { deviceValidCheck, deviceUpdate } = require("../../../services/devices");

//  ///     Devices List
const list = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		let res = await Promise.all([
			services.devicesCount(request.body.listWhereRawSeq),
			services.devicesList(request.body),
		]);

		request.body.pages = Math.ceil(res[0] / request.body.pageSize);

		let result = {
			body: request.body,
			pages: request.body.pages,
			data: res[1]
		};

		return responseHandler.successHandler(response, null, result);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

//  ///     Block Device
const block = async (request, response) => {
	try {
		let device = await deviceValidCheck(response, {
			deviceId: request.body.id
		});

		let message;
		if (request.body.block === "1") message = constants.messages.DeviceBlockSuccess;
		else message = constants.messages.DeviceUnBlockSuccess;

		await deviceUpdate(
			response,
			{ deviceId: request.body.id },
			{ blocked: request.body.block }
		);

		return responseHandler.successHandler(response, message);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};

module.exports = {
	list,
	block
};
