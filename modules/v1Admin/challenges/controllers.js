const constants = require("./constants");
const services = require("./services");

const responseHandler = require("../../../commonFuncs/responseHandler");
const momentFuncs = require("../../../commonFuncs/momentFuncs");

const { fileUpload } = require("../../../packages/upload");

const { pageOffsetCreator } = require("../../../utils/common");

const { chalSerMessages, challengeValidCheck, challengeUpdate, createChallenge, adminChallengeDetailsService, userChallengeDetailsService, updateChallenge} = require("../../../services/challenges");
const { fileServiceTypes, delFileSer, bulkCreateFilesSer } = require("../../../services/filesServices");

//	List Challenges
const challengeLists = async (request, response) => {
try {
	request.body.listWhereRawSeq = `(challenges.deleted="0") AND (challenges.userId=0)`;

	request.body = pageOffsetCreator(request.body);

	let res = await Promise.all([
		services.challengesCount(request.body.listWhereRawSeq),
		services.challengesList(request.body),
	]);

	request.body.pages = Math.ceil(res[0] / request.body.pageSize);

	let result = {
		body: request.body,
		pages: request.body.pages,
		data: res[1]
	};

	return responseHandler.successHandler(response, null, result);
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.challengeLists = challengeLists;

//      Block Challenge
const challengeBlock = async (request, response) => {
try {
	let challenge = await challengeValidCheck(response, {
		deleted: "0",
		challengeId: request.body.id
	});

	let message;
	if (request.body.block === "1") message = constants.messages.ChallengeBlockSuccess;
	else message = constants.messages.ChallengeUnBlockSuccess;

	await challengeUpdate(
		response,
		{ challengeId: request.body.id },
		{ blocked: request.body.block }
	);

	return responseHandler.successHandler(response, message);
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.challengeBlock = challengeBlock;

//      Add Challenge
const challengeAdd = async (request, response) => {
try {
	request.body.adminId = request.adminDetails.adminId;
	request.body.userId = 0;

	request.body.startDt = momentFuncs.utcConvertor(request.body.startDt, request.body.timezone);
	request.body.endDt = momentFuncs.utcConvertor(request.body.endDt, request.body.timezone);

	request.body.startDtStart = momentFuncs.moment(request.body.startDt).startOf("day");
	request.body.endDtEnd = momentFuncs.moment(request.body.endDt).add(1, "day").endOf("day");


	request.body.totalDays = request.body.coins = momentFuncs.momentsDiff(request.body.startDtStart, request.body.endDtEnd, "days");

	let i = 0;
	//	Images Upload
	let imagesPromises = [];
	let sponsorImagesEnd = 0;
	if(request.files && request.files.sponsorImages) {
		let len = request.files.sponsorImages.length;
		for(i=0; i< len; i++) {
			imagesPromises.push(
				fileUpload(request.files.sponsorImages[i], `Sponsor Image ${i}`)
			);
			sponsorImagesEnd = sponsorImagesEnd +1;
		}
	}

	let startImagesEnd = sponsorImagesEnd;
	if(request.files && request.files.startImages) {
		let len = request.files.startImages.length;
		for(i=0; i< len; i++) {
			imagesPromises.push(
				fileUpload(request.files.startImages[i], `Start Image ${i}`)
			);
			startImagesEnd = startImagesEnd + 1;
		}
	}

	//let endImagesStart = endImagesEnd = startImagesEnd;
	if(request.files && request.files.endImages) {
		let len = request.files.endImages.length;
		for(i=0; i< len; i++) {

			imagesPromises.push(
				fileUpload(request.files.endImages[i], `End Image ${i}`)
			);
			//endImagesEnd = endImagesEnd + 1;
		}
	}

	let images = [];
	if(imagesPromises.length) {
		images = await Promise.all(imagesPromises);
	}

	request.body.files = [];
	images.forEach((file, key) => {
		let type;

		if(key < sponsorImagesEnd)
			type = fileServiceTypes.sponsorChallenge;
		else if(key >= sponsorImagesEnd && key < startImagesEnd)
			type = fileServiceTypes.startChallenge;
		else
			type = fileServiceTypes.endChallenge

		request.body.files.push({
			userId: 0,
			adminId: request.adminDetails.adminId,
			file,
			type
		});

	});

	await createChallenge(request.body);

	return responseHandler.successHandler(response, constants.messages.ChallengeAddSuccess, {
		//data: request.body
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.challengeAdd = challengeAdd;

//	List User Challenges
const challengesUserAll = async (request, response) => {
try {
	request.body.listWhereRawSeq = `(challenges.deleted="0") AND (challenges.adminId=0)`;

	request.body = pageOffsetCreator(request.body);

	let res = await Promise.all([
		services.challengesCount(request.body.listWhereRawSeq),
		services.challengesUserList(request.body),
	]);

	request.body.pages = Math.ceil(res[0] / request.body.pageSize);

	let result = {
		//body: request.body,
		pages: request.body.pages,
		data: res[1]
	};

	return responseHandler.successHandler(response, null, result);
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.challengesUserAll = challengesUserAll;

//	Participants Listings
const participantsList = async (request, response) => {
try {
	request.body = pageOffsetCreator(request.body);	

	let challenge = await services.challengeParticipantsList(
		{
			deleted: "0",
			challengeId: request.body.challengeId
		},
		request.body
		);
	if(!challenge)
		return responseHandler.failedActionHandler(response, chalSerMessages.ChalNotAvailMsg);

	request.body.pages = Math.ceil(challenge.challengeUsersCount / request.body.pageSize);

	let data = JSON.parse(JSON.stringify(challenge.challengeUsers));
	delete challenge.challengeUsers;

	return responseHandler.successHandler(response, null, {
		body: request.body,
		challenge,
		data,
		pages: request.body.pages
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.participantsList = participantsList;

//	///		User Challenge Participant Track
const challengeTrack = async (request, response) => {
try {

	challenge = await userChallengeDetailsService(response,
		{
			challengeId: request.body.challengeId,
			deleted: "0"
		},
		{
			userId: request.body.userId
		}
	);
	if(!challenge)
		return responseHandler.failedActionHandler(response, chalSerMessages.ChalNotAvailMsg);

	return responseHandler.successHandler(response, null, {
		body: request.body,
		challenge
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.challengeTrack = challengeTrack;

//	///		Admin Challenge Participant Track
const participantTrack = async (request, response) => {
try {

	challenge = await adminChallengeDetailsService(response,
		{
			challengeId: request.body.challengeId,
			deleted: "0"
		},
		{
			userId: request.body.userId
		}
	);
	if(!challenge)
		return responseHandler.failedActionHandler(response, chalSerMessages.ChalNotAvailMsg);

	return responseHandler.successHandler(response, null, {
		body: request.body,
		challenge
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.participantTrack = participantTrack;

//	///		Admin Challenge Details
const chalDetails = async (request, response) => {
try {

	challenge = await adminChallengeDetailsService(response,
		{
			challengeId: request.body.challengeId,
			deleted: "0"
		},
		{
			userId: 0
		}
	);
	if(!challenge)
		return responseHandler.failedActionHandler(response, chalSerMessages.ChalNotAvailMsg);

	return responseHandler.successHandler(response, null, {
		//body: request.body,
		challenge
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.chalDetails = chalDetails;

//	///		Admin Challenge File Delete
const fileDelete = async (request, response) => {
try {

	await delFileSer(request.body);

	challenge = await adminChallengeDetailsService(response,
		{
			challengeId: request.body.challengeId,
			deleted: "0"
		},
		{
			userId: 0
		}
	);
	if(!challenge)
		return responseHandler.failedActionHandler(response, chalSerMessages.ChalNotAvailMsg);

	return responseHandler.successHandler(response, null, {
		challenge
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.fileDelete = fileDelete;

//	Update Challenge

//	Update Challenge
const chalUpdate = async (request, response) => {
try {
	let challenge = await challengeValidCheck(response, {
		deleted: "0",
		challengeId: request.body.challengeId
	});

	let i = 0;
	//	Images Upload
	let imagesPromises = [];
	let sponsorImagesEnd = 0;
	if(request.files && request.files.sponsorImages) {
		let len = request.files.sponsorImages.length;
		for(i=0; i< len; i++) {
			imagesPromises.push(
				fileUpload(request.files.sponsorImages[i], `Sponsor Image ${i}`)
			);
			sponsorImagesEnd = sponsorImagesEnd +1;
		}
	}

	let startImagesEnd = sponsorImagesEnd;
	if(request.files && request.files.startImages) {
		let len = request.files.startImages.length;
		for(i=0; i< len; i++) {
			imagesPromises.push(
				fileUpload(request.files.startImages[i], `Start Image ${i}`)
			);
			startImagesEnd = startImagesEnd + 1;
		}
	}

	//let endImagesStart = endImagesEnd = startImagesEnd;
	if(request.files && request.files.endImages) {
		let len = request.files.endImages.length;
		for(i=0; i< len; i++) {

			imagesPromises.push(
				fileUpload(request.files.endImages[i], `End Image ${i}`)
			);
			//endImagesEnd = endImagesEnd + 1;
		}
	}

	//	Files Upload
	let images = [];
	if(imagesPromises.length) {
		images = await Promise.all(imagesPromises);
	}
	//	Files Upload

	request.body.files = [];
	images.forEach((file, key) => {
		let type;

		if(key < sponsorImagesEnd)
			type = fileServiceTypes.sponsorChallenge;
		else if(key >= sponsorImagesEnd && key < startImagesEnd)
			type = fileServiceTypes.startChallenge;
		else
			type = fileServiceTypes.endChallenge

		request.body.files.push({
			userId: 0,
			adminId: request.adminDetails.adminId,
			file,
			type,
			challengeId: request.body.challengeId
		});

	});

	if(request.body.files.length) {
		await bulkCreateFilesSer(request.body.files);
	}

	await updateChallenge(
		{challengeId: request.body.challengeId},
		request.body
	);

	challenge = await adminChallengeDetailsService(response,
		{ challengeId: request.body.challengeId },
		{ userId: 0 }
	);

	return responseHandler.successHandler(response, constants.messages.ChallengeUpdateSuccess, {
		data: request.body,
		challenge
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.chalUpdate = chalUpdate;