const Joi = require("@hapi/joi");

const apiRef = "AdminChallengeValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

const { listSortedSchema, listFilteredArraySchema } = require("../../../utils/joiSchemas");

// ///       Listing
module.exports.challengeLists = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/challengeLists`;

	let schema = Joi.object().keys({
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Block Unblock
module.exports.challengeBlock = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/challengeBlock`;

	let schema = Joi.object().keys({
		"id": Joi.number().min(1).required(),
		"block": Joi.string().valid("0", "1").required(),
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Add Challenge
module.exports.challengeAdd = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/challengeAdd`;

	let schema = Joi.object().keys({
        startDt: Joi.string().required().label("Start Date").trim(),
        endDt: Joi.string().required().label("End Date").trim(),

        "sponsorTitle": Joi.string().required().max(255).label("Sponsor Title").trim(),
        "sponsorLink": Joi.string().label("Sponsor Link").optional().trim().allow('', null),
        "sponsorDescription": Joi.string().label("Sponsor Description").optional().trim().allow('', null),

        "startAddress": Joi.string().required().max(255).label("Start Address").trim(),
        "startLatitude": Joi.number().required().label("Start Latitude"),
        "startLongitude": Joi.number().required().max(255).label("Start Longitude"),
        "startTitle": Joi.string().label("Start Title").optional().trim().allow('', null),
        "startLink": Joi.string().required().label("Start Link").trim().allow('', null),
        "startDescription": Joi.string().label("Start Description").optional().trim().allow('', null),

        "endAddress": Joi.string().required().max(255).label("End Address").trim(),
        "endLatitude": Joi.number().required().label("End Latitude"),
        "endLongitude": Joi.number().required().max(255).label("End Longitude"),
        "endTitle": Joi.string().label("End Title").optional().trim().allow('', null),
        "endLink": Joi.string().required().label("End Link").trim().allow('', null),
        "endDescription": Joi.string().label("End Description").optional().trim().allow('', null),

		"directions": Joi.object().keys({
			"overview_polyline": Joi.string().required(),
			"start_address": Joi.string().required(),
			"end_address": Joi.string().required(),
			"totalDistance": Joi.number().optional()
		}).required(),
		"totalDistance": Joi.number().required(),

		"timezone": Joi.string().optional().default("Asia/Calcutta")
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Admin Challenges User List
module.exports.challengesUserAll = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/challengesUserAll`;

	let schema = Joi.object().keys({
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Admin Challenges Participants List
module.exports.participantsList = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/participantsList`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().min(1).required(),
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		User Challenge Track
module.exports.challengeTrack = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/challengeTrack`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Admin Challenge Participant Track
module.exports.participantTrack = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/participantTrack`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().min(1).required(),
		"userId": Joi.number().min(1).required(),
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Admin Challenge Details
module.exports.chalDetails = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/chalDetails`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	///		Admin Challenge Delete File
module.exports.fileDelete = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/fileDelete`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().min(1).required(),
		"fileId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Update Challenge
module.exports.chalUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/chalUpdate`;

	let schema = Joi.object().keys({
		"challengeId": Joi.number().min(1).required(),
        "sponsorTitle": Joi.string().required().max(255).label("Sponsor Title").trim(),
        "sponsorLink": Joi.string().label("Sponsor Link").optional().trim().allow('', null),
        "sponsorDescription": Joi.string().label("Sponsor Description").optional().trim().allow('', null),

        "startTitle": Joi.string().label("Start Title").optional().trim().allow('', null),
        "startLink": Joi.string().required().label("Start Link").trim().allow('', null),
        "startDescription": Joi.string().label("Start Description").optional().trim().allow('', null),

        "endTitle": Joi.string().label("End Title").optional().trim().allow('', null),
        "endLink": Joi.string().required().label("End Link").trim().allow('', null),
        "endDescription": Joi.string().label("End Description").optional().trim().allow('', null),
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};