var Sequelize = require("sequelize");

var Db = require("../../../models");// ///// DB Models

const { sequelizeAtts } = require("./constants");

const { challengeUsersTypes } = require("../../../services/challengeUsers");
const { userDetailsAtts, commonChalUserAtts } = require("../../../services/commonServices").commonSerAttributes;

//  ///     Admin Challenges List
exports.challengesList = (data) => {

	return Db.challenges.findAll({
		where: Sequelize.literal(data.listWhereRawSeq),
		attributes: sequelizeAtts.challengesList.include,
		offset: data.offset,
		limit: data.pageSize,
		order: data.sortByArray,
		//logging: console.log
	});

};

//		User Challenges List
exports.challengesUserList = (data) => {

	return Db.challenges.findAll({
		where: Sequelize.literal(data.listWhereRawSeq),
		attributes: sequelizeAtts.challengesList.include,
		include: [
			{//	Challenges User
				model: Db.challengeUsers,
				as: "chalUser",
				attributes: commonChalUserAtts,
				where: {
					userType: challengeUsersTypes.challenger
				},
				include: [
					{
						model: Db.users,
						as: "user",
						attributes: userDetailsAtts
					}
				]
			},//	Challenges User
			{//	Challenges User
				model: Db.challengeUsers,
				as: "chalOuser",
				attributes: commonChalUserAtts,
				where: {
					userType: challengeUsersTypes.creator
				},
				include: [
					{
						model: Db.users,
						as: "user",
						attributes: userDetailsAtts
					}
				]
			}
		],

		offset: data.offset,
		limit: data.pageSize,
		order: data.sortByArray,
		//logging: console.log
	});
};

exports.challengesCount = (listWhereRawSeq) => {
	return Db.challenges.count({
		where: Sequelize.literal(listWhereRawSeq)
	});
};

//	Admin Challenge Participants Listing
const challengeParticipantsList = (whereJSON, data) => {

	let attributes = sequelizeAtts.challengesList.include;

	attributes.push(
		[Sequelize.literal(`(SELECT COUNT(*) FROM challengeUsers WHERE challengeUsers.challengeId=challenges.challengeId AND ${data.listWhereRawSeq})`), "challengeUsersCount"]
	);

	return Db.challenges.findOne({
		attributes: attributes,
		where: whereJSON,
		include: [
			{
				model: Db.challengeUsers,
				where: Sequelize.literal(data.listWhereRawSeq),
				as: "challengeUsers",
				attributes: ["challengeUserId", "userId", "userType", "status", "createdAt", "totalDistance", "totalSteps", "startedAt"],
				include: [
					{
						model: Db.users,
						as: "user",
						attributes: userDetailsAtts
					}
				],
				required: false,
				order: data.sortByArray,
				offset: data.offset,
				limit: data.pageSize,
			}
		],
		//order: data.sortByArray,
		logging: true
		// order: [
		// 	["challengeUsers", "createdAt", "DESC"]
		// ]
	})
	.then((challenge) => {
		return JSON.parse(JSON.stringify(challenge));
	});

};
exports.challengeParticipantsList = challengeParticipantsList;