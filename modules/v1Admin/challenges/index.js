var express = require("express");

var router = express.Router();

const { verifyJWTMiddlewareAdmin } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/all", verifyJWTMiddlewareAdmin, validators.challengeLists, controllers.challengeLists);

router.post("/block", verifyJWTMiddlewareAdmin, validators.challengeBlock, controllers.challengeBlock);

router.post("/add", multipartMiddleware, verifyJWTMiddlewareAdmin, validators.challengeAdd, controllers.challengeAdd);

router.post("/user/all", verifyJWTMiddlewareAdmin, validators.challengesUserAll, controllers.challengesUserAll);

router.post("/participants/all", verifyJWTMiddlewareAdmin, validators.participantsList, controllers.participantsList);

router.post("/track", verifyJWTMiddlewareAdmin, validators.challengeTrack, controllers.challengeTrack);

router.post("/participant/track", verifyJWTMiddlewareAdmin, validators.participantTrack, controllers.participantTrack);

router.post("/details", verifyJWTMiddlewareAdmin, validators.chalDetails, controllers.chalDetails);

router.post("/details", verifyJWTMiddlewareAdmin, validators.chalDetails, controllers.chalDetails);

router.post("/file/delete", verifyJWTMiddlewareAdmin, validators.fileDelete, controllers.fileDelete);

router.post("/update", multipartMiddleware, verifyJWTMiddlewareAdmin, validators.chalUpdate, controllers.chalUpdate);

module.exports = router;
