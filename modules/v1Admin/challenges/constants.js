let sequelizeAtts = {
	"challengesList": {
		"include": ["challengeId", "createdAt", "startTitle", "endTitle", "startAddress", "endAddress", "blocked", "status", "coins", "userId", "startDt", "endDt", "totalDays", "sponsorTitle", "totalDistance"],
	}
};

const messages = {
	"ChallengeBlockSuccess": "Challenge blocked successfully",
	"ChallengeUnBlockSuccess": "Challenge unblocked successfully",
	"ChallengeAddSuccess": "Challenge added successfully",
	"ChallengeUpdateSuccess": "Challenge updated successfully"
};

module.exports = {
	sequelizeAtts,
	messages
};
