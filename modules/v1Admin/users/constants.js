const messages = {
	"UserBlockSuccess": "User blocked successfully",
	"UserUnBlockSuccess": "User unblocked successfully",
	"UserCoinUpdateSuccess": "User coins updated successfully"
};
exports.messages = messages;