const Joi = require("@hapi/joi");

const apiRef = "AdminUserValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

const { genderTypes } = require("../../../services/userServices");

const { listSortedSchema, listFilteredArraySchema } = require("../../../utils/joiSchemas");

// ///       Listing
module.exports.userLists = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/userLists`;

	let schema = Joi.object().keys({
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Block Unblock
module.exports.userBlock = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/userBlock`;

	let schema = Joi.object().keys({
		"id": Joi.number().min(1).required(),
		"block": Joi.string().valid("0", "1").required(),
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Block Unblock
module.exports.coinsUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/coinsUpdate`;

	let schema = Joi.object().keys({
        "userId": Joi.number().min(1).required(),
        "yellowCoins": Joi.number().min(-9999).max(9999).required().label("Yellow Coins"),
        "greenCoins": Joi.number().min(-9999).max(9999).required().label("Green Coins")
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Profile Get
module.exports.profileGet = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/profileGet`;

	let schema = Joi.object().keys({
        "userId": Joi.number().min(1).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       Profile Update
module.exports.profileUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/profileUpdate`;

	let schema = Joi.object().keys({
        "userId": Joi.number().min(1).required(),
        "name": Joi.string().required(),
        "email": Joi.string().required(),
        "username": Joi.string().required(),
        "phoneCode": Joi.string().required(),
        "phoneNumber": Joi.number().required(),

        "address": Joi.string().allow("").optional(),
        "city": Joi.string().allow("").optional(),
        "state": Joi.string().allow("").optional(),
        "country": Joi.string().allow("").optional(),
        "countryISO2": Joi.string().allow("").optional(),
        "zipcode": Joi.string().allow("").optional(),
        "addressLatitude": Joi.number().precision(5).min(-90).max(90).optional(),
        "addressLongitude": Joi.number().precision(5).min(-180).max(180).optional(),

        "dob": Joi.string().optional(),

		"gender": Joi.string().max(20).valid(genderTypes.male, genderTypes.female, genderTypes.notDisclosed).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       User Devices
module.exports.userDevices = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/userDevices`;

	let schema = Joi.object().keys({
		"userId": Joi.number().min(1).required(),
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       User Admin Challenges
module.exports.adminChals = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/adminChals`;

	let schema = Joi.object().keys({
		"userId": Joi.number().min(1).required(),
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       User Friends Challenges
module.exports.friendChals = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/friendChals`;

	let schema = Joi.object().keys({
		"userId": Joi.number().min(1).required(),
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

// ///       User Device Syncs
module.exports.deviceSyncs = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/deviceSyncs`;

	let schema = Joi.object().keys({
		"userId": Joi.number().min(1).required(),
		"userDeviceId": Joi.number().min(1).required(),
		"pageSize": Joi.number().min(5).optional(),
		"page": Joi.number().min(0).optional(),
		"sortedArray": listSortedSchema,
		"filteredArray": listFilteredArraySchema
	}).with("page", "pageSize");

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};