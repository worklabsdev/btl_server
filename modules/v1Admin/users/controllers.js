const Sequelize = require("sequelize");

const constants = require("./constants");

const { userUniqueCheck } = require("../../v1/userSignIn/services");

const responseHandler = require("../../../commonFuncs/responseHandler");

const { pageOffsetCreator } = require("../../../utils/common");

const { fileUpload } = require("../../../packages/upload");

const { coinsHistoryTypes } = require("../../../properties/constant");

const { adminUserDetailsAtts } = require("../../../services/commonServices").commonSerAttributes;
const { userValidCheck, userUpdate, usersCountSer, usersListSer } = require("../../../services/userServices");
const { createUserCoinsHistories } = require("../../../services/userCoinsHistories");
const { userDeviceValidCheckSer, userDevicesCountSer, userDevicesListSer } = require("../../../services/userDevices");
const { updateLoginHist } = require("../../../services/loginHists");
const { appAdminChallengesCount, appAdminChallengesLists, appUserChallengesCounts, appUserChallengesLists } = require("../../v1/challenges/services");
const { userDeviceSyncsListSer, userDeviceSyncsCountSer } = require("../../../services/userDeviceSyncServices");

//		User Listings
const userLists = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		let res = await Promise.all([
			usersCountSer(request.body.listWhereRawSeq),
			usersListSer(request.body),
		]);

		request.body.pages = Math.ceil(res[0] / request.body.pageSize);

		return responseHandler.successHandler(response, null, {
			body: request.body,
			pages: request.body.pages,
			data: res[1]
		});

	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.userLists = userLists;

//      Block User
const userBlock = async (request, response) => {
	try {
		let user = await userValidCheck(response, {
			deleted: "0",
			userId: request.body.id
		});

		let message;
		if (request.body.block === "1") message = constants.messages.UserBlockSuccess;
		else message = constants.messages.UserUnBlockSuccess;

		await userUpdate(
			response,
			{ userId: request.body.id },
			{ blocked: request.body.block }
		);

		return responseHandler.successHandler(response, message);
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.userBlock = userBlock;

//	User Coins Update
const coinsUpdate = async (request, response) => {
try {

	let user = await userValidCheck(response, {
		deleted: "0",
		userId: request.body.userId
	});

	request.body.yellowCoins = parseInt(request.body.yellowCoins);
	request.body.greenCoins = parseInt(request.body.greenCoins);

	await Promise.all([
		userUpdate(//	User Update
			response,
			{ userId: request.body.userId },
			{
				yellowCoins: Sequelize.literal(`yellowCoins + ${request.body.yellowCoins}`),
				greenCoins: Sequelize.literal(`greenCoins + ${request.body.greenCoins}`),
				totalYellowCoins: Sequelize.literal(`totalYellowCoins + ${request.body.yellowCoins}`),
				totalGreenCoins: Sequelize.literal(`totalGreenCoins + ${request.body.greenCoins}`)
			}
		),//	User Update
		createUserCoinsHistories({
			userId: request.body.userId,
			ouserId: 0,
			challengeId: 0,
			adminId: request.adminDetails.adminId,
			yellowCoins: request.body.yellowCoins,
			greenCoins: request.body.greenCoins,
			type: coinsHistoryTypes.adminUpdate
		})
	]);

	return responseHandler.successHandler(response, constants.messages.UserCoinUpdateSuccess, {
		yellowCoins: user.yellowCoins + request.body.yellowCoins,
		greenCoins: user.greenCoins + request.body.greenCoins
	});
	
} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.coinsUpdate = coinsUpdate;

//	User Profile Get
const profileGet = async (request, response) => {
try {

	let user = await userValidCheck(response, {
		deleted: "0",
		userId: request.body.userId
	},
	adminUserDetailsAtts
	);
	user = JSON.parse(JSON.stringify(user));

	return responseHandler.successHandler(response, null, {
		user
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.profileGet = profileGet;

//	User Profile Update
const profileUpdate = async (request, response) => {
try {

	let user = await userValidCheck(response, {
		deleted: "0",
		userId: request.body.userId
	},
	adminUserDetailsAtts
	);
	user = JSON.parse(JSON.stringify(user));

	let updateDevice = "0";

	if(user.email !== request.body.email) {//	Email Unique Check
		await userUniqueCheck(response, 
			{
				email: request.body.email,
				deleted: "0"
			},
			"email"
		);

		request.body.accessToken = "";

		updateDevice = "1";
	}//	Email Unique Check

	if(user.phoneNumber != request.body.phoneNumber) {//	Phone Number Unique Check
		await userUniqueCheck(response, 
			{
				phoneNumber: request.body.phoneNumber,
				deleted: "0"
			},
			"phone number"
		);

		request.body.accessToken = "";
		request.body.phoneVerified = "0";

		updateDevice = "1";
	}//	Phone Number Unique Check

	if(user.username !== request.body.username) {//	Username Unique Check
		await userUniqueCheck(response, 
			{
				username: request.body.username,
				deleted: "0"
			},
			"username"
		);
	}//	Username Unique Check

	if (request.files.profilePic) request.body.profilePic = await fileUpload(request.files.profilePic, "Profile Pic");

	if (request.files.coverPic) request.body.coverPic = await fileUpload(request.files.coverPic, "Cover Pic");

	await userUpdate(//	User Update
		response,
		{ userId: user.userId },
		request.body
	);

	if(updateDevice === "1") {//	Login Update
		updateLoginHist(
			{
				userId: user.userId,
				isValid: "1"
			},
			{
				accessToken: "",
				isValid: "0"
			}
		);
	}//	Login Update

	user = await userValidCheck(response, {
		userId: request.body.userId
	},
	adminUserDetailsAtts
	);


	return responseHandler.successHandler(response, null, {
		user,
		//data: request.body
	});

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.profileUpdate = profileUpdate;

//	User Devices Listings
const userDevices = async (request, response) => {
	try {

		let user = await userValidCheck(response, {
			deleted: "0",
			userId: request.body.userId
		},
		adminUserDetailsAtts
		);
		user = JSON.parse(JSON.stringify(user));


		request.body = pageOffsetCreator(request.body);

		request.body.listWhereRawSeq = `${request.body.listWhereRawSeq} AND (userId=${user.userId})`;

		let res = await Promise.all([
			userDevicesCountSer({
				userId: user.userId
			}),
			userDevicesListSer(request.body)
		]);

		request.body.pages = Math.ceil(res[0] / request.body.pageSize);

		return responseHandler.successHandler(response, null, {
			user,
			body: request.body,
			pages: request.body.pages,
			data: res[1]
		});

	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.userDevices = userDevices;

//	User Admin Listings
const adminChals = async (request, response) => {
try {
	let user = await userValidCheck(response, {
		deleted: "0",
		userId: request.body.userId
	},
	adminUserDetailsAtts
	);
	user = JSON.parse(JSON.stringify(user));

	request.body = pageOffsetCreator(request.body);

	request.body.extraCheck = request.body.countCheck = `AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ( (CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) )) != 0)`;

	request.body.extraCheck = `${request.body.extraCheck} AND ${request.body.listWhereRawSeq}`;

	let res = await Promise.all([
		appAdminChallengesCount(request.body.countCheck),
		appAdminChallengesLists(request.body)
	]);

	request.body.pages = Math.ceil(res[0] / request.body.pageSize);

	return responseHandler.successHandler(response, null, {
		user,
		body: request.body,
		pages: request.body.pages,
		data: res[1]
	});

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.adminChals = adminChals;

//	User Friends Listings
const friendChals = async (request, response) => {
try {

	let user = await userValidCheck(response, {
		deleted: "0",
		userId: request.body.userId
	},
	adminUserDetailsAtts
	);
	user = JSON.parse(JSON.stringify(user));

	request.body = pageOffsetCreator(request.body);

	//request.body.extraCheck = `AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ( (CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) )) != 0) AND ${request.body.listWhereRawSeq}`;
	request.body.extraCheck = request.body.countCheck = `AND ((SELECT COUNT(*) FROM challengeUsers CU WHERE ( (CU.userId=${request.body.userId}) AND (CU.challengeId=challenges.challengeId) )) != 0)`;

	request.body.extraCheck = `${request.body.extraCheck} AND ${request.body.listWhereRawSeq}`;

	let res = await Promise.all([
		appUserChallengesCounts(request.body.countCheck),
		appUserChallengesLists(request.body)
	]);

	request.body.pages = Math.ceil(res[0] / request.body.pageSize);

	return responseHandler.successHandler(response, null, {
		user,
		body: request.body,
		pages: request.body.pages,
		data: res[1]
	});

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.friendChals = friendChals;

const deviceSyncs = async (request, response) => {
try {

	//	User Details
	let user = await userValidCheck(response, {
		deleted: "0",
		userId: request.body.userId
	},
	adminUserDetailsAtts
	);
	//	User Details

	//	Device Details
	let userDevice = await userDeviceValidCheckSer(response, {
		userDeviceId: request.body.userDeviceId
	});
	//	Device Details

	//	Where Paginator Creator
	request.body = pageOffsetCreator(request.body);

	request.body.listWhereRawSeq = `${request.body.listWhereRawSeq} AND (userId=${request.body.userId}) AND (userDeviceId=${request.body.userDeviceId})`;
	//	Where Paginator Creator

	//	Sync Listings
	let res = await Promise.all([
		userDeviceSyncsCountSer(request.body.listWhereRawSeq),
		userDeviceSyncsListSer(request.body),
	]);

	request.body.pages = Math.ceil(res[0] / request.body.pageSize);
	//	Sync Listings


	return responseHandler.successHandler(response, null, {
		body: request.body,
		user,
		userDevice,
		pages: request.body.pages,
		data: res[1]
	});

}
catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.deviceSyncs = deviceSyncs;