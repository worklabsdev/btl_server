var express = require("express");

var router = express.Router();

const { verifyJWTMiddlewareAdmin } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/all", verifyJWTMiddlewareAdmin, validators.userLists, controllers.userLists);

router.post("/block", verifyJWTMiddlewareAdmin, validators.userBlock, controllers.userBlock);

router.post("/coinsUpdate", verifyJWTMiddlewareAdmin, validators.coinsUpdate, controllers.coinsUpdate);

router.post("/profile", verifyJWTMiddlewareAdmin, validators.profileGet, controllers.profileGet);

router.post("/profileUpdate", multipartMiddleware, verifyJWTMiddlewareAdmin, validators.profileUpdate, controllers.profileUpdate);

router.post("/devices", verifyJWTMiddlewareAdmin, validators.userDevices, controllers.userDevices);

router.post("/adminChals", verifyJWTMiddlewareAdmin, validators.adminChals, controllers.adminChals);

router.post("/friendChals", verifyJWTMiddlewareAdmin, validators.friendChals, controllers.friendChals);

router.post("/deviceSyncs", verifyJWTMiddlewareAdmin, validators.deviceSyncs, controllers.deviceSyncs);

module.exports = router;
