let pathPrefix = "/api/v1/admin";

app.use(`${pathPrefix}/`, require("./auth"));
app.use(`${pathPrefix}/challenge`, require("./challenges"));
app.use(`${pathPrefix}/`, require("./commons"));
app.use(`${pathPrefix}/device`, require("./devices"));
app.use(`${pathPrefix}/user`, require("./users"));