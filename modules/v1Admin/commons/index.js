var express = require("express");

var router = express.Router();

const { verifyJWTMiddlewareAdmin } = require("../../../middlewares/authMiddleware");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/dashboard/data", verifyJWTMiddlewareAdmin, validators.dashboardData, controllers.dashboardData);

router.post("/settings/get", verifyJWTMiddlewareAdmin, validators.settings, controllers.settings);

router.post("/settings/update", verifyJWTMiddlewareAdmin, validators.settingsUpdate, controllers.settingsUpdate);

module.exports = router;
