const BaseJoi = require("@hapi/joi");
const Extension = require("@hapi/joi-date");

const Joi = BaseJoi.extend(Extension);

const apiRef = "AdminCommonValidator";

const { joiValidate } = require("../../../commonFuncs/validate");

//	DashBoard Data
module.exports.dashboardData = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/login`;

	let schema = Joi.object().keys({
		"startDate": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
		"endDate": Joi.date().format("YYYY-MM-DD HH:mm:ss").required(),
		"timezone": Joi.string().max(50).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	Settings Get
module.exports.settings = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/settings`;

	let schema = Joi.object().keys({
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};

//	Settings Update
module.exports.settingsUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/settingsUpdate`;

	let schema = Joi.object().keys({
        redeemFactor: Joi.number().min(0).required(),
        defYellowCoins: Joi.number().min(0).required(),
        referalNewYellowCoins: Joi.number().min(0).required(),
        referalOldYellowCoins: Joi.number().min(0).required(),
        deviceUpdateRange: Joi.object().keys({
            start: Joi.string().required().label("Start Time"),
            end: Joi.string().required().label("End Time")
        }).required()
	});

	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, { requestId: request.requestId  });
	if (valCheck === true) next();
};