const responseHandler = require("../../../commonFuncs/responseHandler");
const { utcConvertor } = require("../../../commonFuncs/momentFuncs");

// const { messages } = require("./constants");

const services = require("./services");

const { appSettingsSerGetOne, updateAppSettingsSer } = require("../../../services/appSettingsServices");

//	Dashboard Data
const dashboardData = async (request, response) => {
	try {
		request.body.startDateUTC = utcConvertor(request.body.startDate, request.body.timezone);
		request.body.endDateUTC = utcConvertor(request.body.endDate, request.body.timezone);

		let counts = await services.dashboardCounts(request.body.startDateUTC, request.body.endDateUTC);

		return responseHandler.successHandler(response, null, { counts: counts[0] });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.dashboardData = dashboardData;

//	Settings Get
const settings = async (request, response) => {
try {

	let settings = await appSettingsSerGetOne({});

	return responseHandler.successHandler(response, null, { settings });

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.settings = settings;

//	Settings Update
const settingsUpdate = async (request, response) => {
try {

	let settings = await updateAppSettingsSer({}, request.body);

	return responseHandler.successHandler(response, null, { settings });

} catch (error) {
	return responseHandler.errorHandler(response, error);
}
};
exports.settingsUpdate = settingsUpdate;