var Sequelize = require("sequelize");

var Db = require("../../../models");// ///// DB Models

const { sequelizeAtts } = require("./constants");

//  ///     Admin DashBoard Counts
exports.dashboardCounts = (startDt, endDt) => {
	return Db.sequelize.query(`
	SELECT 
        (SELECT COUNT(*) FROM users U WHERE U.createdAt BETWEEN "${startDt}" AND "${endDt}") as users,
        (SELECT COUNT(*) FROM challenges C WHERE C.adminId!=0 AND C.createdAt BETWEEN "${startDt}" AND "${endDt}") as challenges,
        (SELECT COUNT(*) FROM challenges C WHERE C.adminId=0 AND C.createdAt BETWEEN "${startDt}" AND "${endDt}") as challengesUsersAll
        FROM admins A
        LIMIT 0, 1
	`, { 
                type: Sequelize.QueryTypes.SELECT, 
                //logging: console.log 
        });
};
