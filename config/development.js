const { appConstants } = require("../properties/constant");

module.exports = {
	"environment": "development",
	"name": appConstants.fullName,
	"port": 3000,
	"host": process.env.HOST,

	"default": {
		"language": "en"
	},

	"db": {
		"mysql": {
			"dbConnection": process.env.DB_CONNECTION,
			"dbHost": process.env.DB_HOST,
			"dbPort": process.env.DB_PORT,
			"dbName": process.env.DB_NAME,
			"dbUser": process.env.DB_USER,
			"dbPassword": process.env.DB_PASSWORD
		}
	},
	"upload": {
		"name": "s3",
		"spaces": {
			"accessKeyId": process.env.SPACES_ACCESS_KEY_ID,
			"secretAccessKey": process.env.SPACES_SECRET_ACCESS_KEY,
			"endpoint": process.env.SPACES_ENDPOINT,
			"bucket": process.env.SPACES_BUCKET,
			"folder": "BTL",
			"filesURL": process.env.SPACES_FILES_URL
		},
		"s3": {
			"accessKeyId": process.env.AWS_ACCESS_KEY_ID,
			"secretAccessKey": process.env.AWS_SECRET_ACCESS_KEY,
			"bucket": process.env.AWS_BUCKET,
			"filesURL": process.env.AWS_FILES_URL
		}
	},
	"errorSettings": {
		"showError": 1,
		"mail": 0,
		"log": {
			request: 1,
			validation: 1,
			response: 1,
			info: 1,
			error: 1,

			socket: {
				info: 1,
				error: 1
			},

			api: {
				request: 1,
				validation: 1,
				response: 1,
				info: 1,
				error: 1
			},

			push: {
				info: 1,
				error: 1
			},

			cron: {
				info: 1,
				error: 1
			},

			mail: {
				info: 1,
				error: 1
			}
		}
	},
	"auth": {
		"jwtKey": process.env.JWT_KEY
	},
	"sms": {
		"name": "sns",
		"otpValidityMinutes": 20,
		"twilio": {
			"accountSid": process.env.TWILIO_ACCOUNT_SID || null,
			"authToken": process.env.TWILIO_AUTH_TOKEN || null,
			"number": process.env.TWILIO_NUMBER || null
		},
		"sns": {
			"accessKeyId": process.env.AWS_ACCESS_KEY_ID,
			"secretAccessKey": process.env.AWS_SECRET_ACCESS_KEY,
			"region": process.env.AWS_SMS_REGION,
			"topic": process.env.AWS_SMS_TOPIC
		}
	},
	"appVersions": {
		"android": {
			"normal": "1.0.0",
			"force": "1.0.0"
		},
		"ios": {
			"normal": "1.0.0",
			"force": "1.0.0"
		}
	},
	"pushNotifications": {
		"name": "google",
		"google": {
			"projectId": process.env.FCM_PROJECT_ID,
			"clientEmail": process.env.FCM_CLIENT_EMAIL,
			"privateKey": process.env.FCM_PRIVATE_KEY,
			"title": appConstants.fullName	
		}
	},

	"devices": {

		"fitbit": {
			"clientId": process.env.FITBIT_CLIENT_ID,
			"clientSecret": process.env.FITBIT_CLIENT_SECRET,
			"redirectURL": process.env.FITBIT_REDIRECT_URL,
			"authURL": process.env.FITBIT_AUTH_URL,
			"tokenURL": process.env.FITBIT_TOKEN_URL,
			"subscriberId": process.env.FITBIT_SUBSCRIBE_ID
		}

	}

};
