const { authHandler, errorHandler, errorHandlerView, authHandlerView } = require("../commonFuncs/responseHandler");
const { decodeJWT } = require("../commonFuncs/auth");

const { userSignInDetailsAuth, userSignInDetailsAuthView } = require("../services/userServices");
const { adminSignInDetailsAuth } = require("../services/adminServices");

const apiRef = "authMiddleware";

// ///       Verify JWT
const verifyJWTMiddleware = async (request, response, next) => {
	try {
		let token = request.headers["x-access-token"] || request.headers["authorization"]; // Express headers are auto converted to lowercase

		if (!token) return authHandler(response, 1);

		if (token.startsWith("Bearer ")) {
			token = token.slice(7, token.length); // Remove Bearer from string
		}

		let valid = decodeJWT(token);

		if (!valid.success || !valid.decoded.userId) return authHandler(response, 2);

		let user = await userSignInDetailsAuth(response, token);

		if(user.phoneVerified === "0") {//	OTP Verification Step

			if ((user.otp || user.phoneVerified === "0") && (request.originalUrl !== "/api/v1/user/verifyPhone") && (request.originalUrl !== "/api/v1/user/resendOTP") && (request.originalUrl !== "/api/v1/user/addPhone") && (request.originalUrl !== "/api/v1/user/passwordReset")) { //	///	No Api Until OTP is Verified
				return authHandler(response, 5);
			}//	///	No Api Until OTP is Verified

		}//	OTP Verification Step

		request.userDetails = user;

		return next();
	} catch (error) {
		return errorHandler(response, error, { data: request.body, apiRef });
	}
};
exports.verifyJWTMiddleware = verifyJWTMiddleware;

//		User Auth View
const userAuthView = async (request, response, next) => {
try {

	let token = request.body.accessToken;

	if (!token) return authHandlerView(response, 1);
	
	let valid = decodeJWT(token);

	if (!valid.success || !valid.decoded.userId) return authHandlerView(response, 2);

	let user = await userSignInDetailsAuthView(response, token);

	request.userDetails = user;

	return next();

} catch (error) {
	return errorHandlerView(response, error, { data: request.body, apiRef });
}
};
exports.userAuthView = userAuthView;


const verifyJWTMiddlewareAdmin = async (request, response, next) => {
	try { // return authHandler(response, 2);
		let token = request.headers["authorization"];

		if (!token) return authHandler(response, 1);

		if (token.startsWith("Bearer ")) {
			token = token.slice(7, token.length); // Remove Bearer from string
		}

		let valid = decodeJWT(token);

		if (!valid.success || !valid.decoded.adminId) return authHandler(response, 2);

		let admin = await adminSignInDetailsAuth(response, valid.decoded.adminId, token);

		request.adminDetails = admin;
		return next();
	} catch (error) {
		return errorHandler(response, error, { data: request.body, apiRef });
	}
};
exports.verifyJWTMiddlewareAdmin = verifyJWTMiddlewareAdmin;