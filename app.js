require("dotenv").config();

//process.setMaxListeners(0);

const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const multipart = require("connect-multiparty");
const helmet = require("helmet");
const deviceParser = require('ua-parser-js');

multipartMiddleware = multipart({
	maxFilesSize: 10 * 1024 * 1024
});

app = express();

global.appConfig = require(`./config/${process.env.NODE_ENV}`);// /// Global Config Constants

// /// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

// ///   Packages Include
require("./packages");// ///  Localization

app.use(helmet());
app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

const { generateRequestId } = require("./commonFuncs/commonFuncs");
app.use(function(req, res, next) {	
	req.requestId = generateRequestId();
	req.device = deviceParser(req.headers['user-agent']);

	next();
});

require("./modules/v1");// ///		Adding Modules V1
require("./modules/v1Admin");//	///	Adding Modules V1 Admin

require("./modules/common");//	Common Routes
require("./modules/devices");// ///		Adding Devices Module

// app.use(express.static(__dirname + '/build'));
// app.get('*', function (request, response){
//   response.sendFile(path.resolve(__dirname, 'build', 'index.html'))
// });

// catch 404 and forward to error handler
const { pageNotFound, errorHandler } = require("./commonFuncs/responseHandler");

app.use((req, res, next) => {
	return pageNotFound(res);
});
// Error Handler
app.use((err, req, res, next) => {
	return errorHandler(res, err);
});

require("./modules/common/crons");

module.exports = app;


