let { responseMessages } = require("../properties/constant").globalMessages;//

//		Auth Error Message Generator
const authErrorMessageGenerator = (response, type) => {

	let message;
	switch (type) {
		case 1:
			message = response.trans(responseMessages.authHeaderMissing);
			break;
		case 2:
			message = response.trans(responseMessages.authFailedMsg);
			break;
		case 3:
			message = response.trans(responseMessages.accountSuspended);
			break;
		case 4:
			message = response.trans(responseMessages.accountNotFound);
			break;
		case 5:
			message = response.trans(responseMessages.otpVerificationRequired);
			break;
		default:
			message = response.trans(responseMessages.authFailedMsg);
			break;
		}

	return message;

};
exports.authErrorMessageGenerator = authErrorMessageGenerator;