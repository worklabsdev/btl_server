const { pageSize } = require("../properties/constant").defaultValues;

//  ///     Pagination Creator
const defaultSortBy = [["createdAt", "DESC"]];
const listDefaultSoryByAdmin = (data) => {
	try {
		if (data.sortedArray && data.sortedArray.length) {
			// "sorted":[{"id":"updatedAt","desc":true}]
			data.sortByArray = [];

			data.sortedArray.forEach((sort) => {
				data.sortByArray.push([sort.id, sort.desc ? "desc" : "asc"]);
			});
		}
		else {
			data.sortByArray = defaultSortBy;
		}

		return data;
	}
	catch (e) {
		console.log("listDefaultSoryByAdmin Error", e);
		data.sortByArray = defaultSortBy;

		return data;
	}
};
exports.listDefaultSoryByAdmin = listDefaultSoryByAdmin;


//  ///     Search Where Sequelize
const defaultFilterWhere = "(1=1)";
const listWhereRawSeq = (data) => {
	try {
		// "filteredArray":[{"id":"deviceId","value":"asd"}]
		if (!data.listWhereRawSeq) data.listWhereRawSeq = defaultFilterWhere;

		if (data.filteredArray && data.filteredArray.length) {
			data.filteredArray.forEach((filter) => {
				data.listWhereRawSeq = `${data.listWhereRawSeq} AND (${filter.id} LIKE "%${filter.value}%")`;
			});
		}

		return data;
	}
	catch (e) {
		console.log("listWhereRawSeq Error", e);
		if(!data.listWhereRawSeq)
			data.listWhereRawSeq = defaultFilterWhere;

		return data;
	}
};
exports.listWhereRawSeq = listWhereRawSeq;

//  /// Pagination Offset Creator
const pageOffsetCreator = (data) => {
	data.pageSize = parseInt(data.pageSize) || parseInt(data.limit) || pageSize;
	data.page = parseInt(data.page) || 0;
	data.offset = data.page * data.pageSize;

	if (data.sortedArray) data = listDefaultSoryByAdmin(data);
	if (data.filteredArray) data = listWhereRawSeq(data);

	return data;
};
exports.pageOffsetCreator = pageOffsetCreator;
