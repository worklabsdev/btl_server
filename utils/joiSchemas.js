const Joi = require("@hapi/joi");

const { devicesTypes } = require("../services/commonServices");

//  Sorting Schema
const listSortedSchema = Joi.array().items(Joi.object().keys({
	"id": Joi.string().required(),
	"desc": Joi.boolean().required()
})).optional();
exports.listSortedSchema = listSortedSchema;

//  Filter Schema
const listFilteredArraySchema = Joi.array().items(Joi.object().keys({
	"id": Joi.string().required(),
	"value": Joi.string().required()
})).optional();
exports.listFilteredArraySchema = listFilteredArraySchema;

//	Device OS Schema
const deviceOSSchema = Joi.string().valid(devicesTypes.ios, devicesTypes.android).optional();
exports.deviceOSSchema = deviceOSSchema;

//	Lat Long Schema
const latSchema = Joi.number().precision(5).min(-90).max(90);
exports.latSchema = latSchema;

const lngSchema = Joi.number().precision(5).min(-180).max(180);
exports.lngSchema = lngSchema;