var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Challenge = sequelize.define("challenges", {
		challengeId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		adminId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		startAddress: { type: DataTypes.STRING(255), allowNull: true, defaultValue: null },
		startLatitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0
		},
		startLongitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0
		},
		startTitle: { type: DataTypes.STRING(255), allowNull: true, defaultValue: null },
		startLink: { type: DataTypes.TEXT("tiny"), allowNull: true, defaultValue: null },
		startDescription: { type: DataTypes.TEXT("tiny"), allowNull: true, defaultValue: null },


		endAddress: { type: DataTypes.STRING(255), allowNull: true, defaultValue: null },
		endLatitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0
		},
		endLongitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0
		},
		endTitle: { type: DataTypes.STRING(255), allowNull: true, defaultValue: null },
		endLink: { type: DataTypes.TEXT("tiny"), allowNull: true, defaultValue: null },
		endDescription: { type: DataTypes.TEXT("tiny"), allowNull: true, defaultValue: null },

		timezone: { type: DataTypes.STRING(50), allowNull: false, defaultValue: "Asia/Calcutta" },

		totalDistance: {
			type: Sequelize.STRING(50),
			allowNull: false,
			defaultValue: 0,
			get(field) {
				var val = parseFloat(this.getDataValue(field));
				return parseFloat(val.toFixed(6));
			}
		},

		startDt: { type: DataTypes.STRING(20), allowNull: true, defaultValue: null },
		endDt: { type: DataTypes.STRING(20), allowNull: true, defaultValue: null },

		totalDays: { type: Sequelize.INTEGER, allowNull: false, defaultValue: 0 },
		coins: { type: Sequelize.INTEGER, allowNull: false, defaultValue: 0 },

		status: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Created" },
		directions: { type: Sequelize.TEXT("long"), allowNull: true, defaultValue: null },

		sponsorTitle: { type: DataTypes.STRING(255), allowNull: true, defaultValue: null },
		sponsorDescription: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },
		sponsorLink: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },

		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },
		blocked: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		visibleAt: { type: DataTypes.STRING(20), allowNull: true },
		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	Challenge.associate = function (models) {
		Challenge.belongsTo(models.users, { as: "creator", foreignKey: "userId", sourceKey: "userId" });
		Challenge.belongsTo(models.admins, { as: "admin", foreignKey: "adminId", sourceKey: "adminId" });

		Challenge.hasMany(models.challengeUsers, { as: "challengeUsers", foreignKey: "challengeId", sourceKey: "challengeId" });
		Challenge.hasOne(models.challengeUsers, { as: "challengeUser", foreignKey: "challengeId", sourceKey: "challengeId" });

		Challenge.hasOne(models.challengeUsers, { as: "chalUser", foreignKey: "challengeId", sourceKey: "challengeId" });
		Challenge.hasOne(models.challengeUsers, { as: "chalOuser", foreignKey: "challengeId", sourceKey: "challengeId" });

		Challenge.hasMany(models.userCoinsHistories, { as: "challengeHists", foreignKey: "challengeId", sourceKey: "challengeId" });

		Challenge.hasMany(models.userNotifications, { as: "notifications", foreignKey: "challengeId", sourceKey: "challengeId" });

		Challenge.hasMany(models.files, { as: "files", foreignKey: "challengeId", sourceKey: "challengeId" });

		Challenge.hasMany(models.files, { as: "sponsorFiles", foreignKey: "challengeId", sourceKey: "challengeId" });
		Challenge.hasMany(models.files, { as: "startFiles", foreignKey: "challengeId", sourceKey: "challengeId" });
		Challenge.hasMany(models.files, { as: "endFiles", foreignKey: "challengeId", sourceKey: "challengeId" });

		Challenge.hasOne(models.challengeSyncs, { as: "challengeSync", foreignKey: "challengeId", sourceKey: "challengeId" });
		Challenge.hasMany(models.challengeSyncs, { as: "challengeSyncs", foreignKey: "challengeId", sourceKey: "challengeId" });
	};

	return Challenge;
};
