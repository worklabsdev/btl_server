var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserBlocked = sequelize.define("userBlockeds", {
		userBlockedId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		ouserId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		comments: { type: DataTypes.TEXT, allowNull: true, defaultValue: null },

		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	UserBlocked.associate = function (models) {
		UserBlocked.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });
		UserBlocked.belongsTo(models.users, { as: "ouser", foreignKey: "ouserId", sourceKey: "userId" });
	};

	return UserBlocked;
};
