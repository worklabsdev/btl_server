var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {

	var Medal = sequelize.define("medals", {

		medalId: {
			type: Sequelize.BIGINT.UNSIGNED,
			primaryKey: true,
			autoIncrement: true
		},

		minVal: {
			type: Sequelize.BIGINT.UNSIGNED,
			allowNull: false,
			defaultValue: 0
		},
		maxVal: {
			type: Sequelize.BIGINT.UNSIGNED,
			allowNull: false,
			defaultValue: 0
		},

		createdAt: {
			type: Sequelize.STRING(20),
			allowNull: true
		},
		updatedAt: {
			type: Sequelize.STRING(20),
			allowNull: true
		}

	},
	{
		    underscored: false,
		    timestamps: false,
	});

	return Medal;
};
