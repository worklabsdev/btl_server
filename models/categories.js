var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Category = sequelize.define("categories", {
		categoryId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		name: { type: DataTypes.STRING(100), allowNull: false },
		ranking: { type: Sequelize.BIGINT, allowNull: false, defaultValue: 0 },

		blocked: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	Category.associate = function (models) {
		Category.belongsToMany(models.faqs, { as: "faqs", through: "faqCats", foreignKey: "categoryId" });
	};

	return Category;
};
