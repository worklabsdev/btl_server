var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Device = sequelize.define("devices", {
		deviceId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		name: { type: DataTypes.STRING(100), allowNull: false },
		ranking: { type: Sequelize.BIGINT, allowNull: false, defaultValue: 0 },

		blocked: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	Device.associate = function (models) {
		Device.hasMany(models.userDevices, { as: "userDevices", foreignKey: "deviceId", sourceKey: "deviceId" });
		Device.hasOne(models.userDevices, { as: "userDevice", foreignKey: "deviceId", sourceKey: "deviceId" });

		Device.hasMany(models.userDeviceSyncs, { as: "userDeviceSyncs", foreignKey: "deviceId", sourceKey: "deviceId" });

		Device.hasMany(models.devicesWebhooks, { as: "webhooks", foreignKey: "deviceId", sourceKey: "deviceId" });

		Device.hasMany(models.userNotifications, { as: "notifications", foreignKey: "deviceId", sourceKey: "deviceId" });

	};

	return Device;
};
