var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var ChallengeSync = sequelize.define("challengeSyncs", {
		challengeSyncId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		devicesWebhookId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		challengeId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		userDeviceSyncId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		distance: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				var val = parseFloat(this.getDataValue(field));
				return parseFloat(val.toFixed(6));
			}
		},
		steps: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				return parseInt(this.getDataValue(field));
			}
		},

		createdAt: { type: Sequelize.STRING(20), allowNull: true },
		updatedAt: { type: Sequelize.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	ChallengeSync.associate = function (models) {

		ChallengeSync.belongsTo(models.devicesWebhooks, { as: "devicesWebhook", foreignKey: "devicesWebhookId", sourceKey: "devicesWebhookId" });

        ChallengeSync.belongsTo(models.challenges, { as: "challenge", foreignKey: "challengeId", sourceKey: "challengeId" });
        
        ChallengeSync.belongsTo(models.userDeviceSyncs, { as: "userDeviceSync", foreignKey: "userDeviceSyncId", sourceKey: "userDeviceSyncId" });

    };

	return ChallengeSync;
};
