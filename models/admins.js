var Sequelize = require("sequelize");

const filesURL = appConfig.upload[appConfig.upload.name].filesURL;

module.exports = (sequelize, DataTypes) => {
	var Admin = sequelize.define("admins", {
		adminId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		name: { type: DataTypes.STRING(255), allowNull: false },

		email: { type: DataTypes.STRING(50), index: true, allowNull: false },
		password: { type: DataTypes.TEXT, allowNull: false },

		accessToken: { type: DataTypes.TEXT, allowNull: true, defaultValue: null },

		profilePic: {
			type: DataTypes.STRING(100),
			allowNull: true,
			defaultValue: null,
			get(field) {
				var val = this.getDataValue(field);

				if (!val) return "";
				return `${filesURL}${val}`;
			}
		},
		timezone: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Asia/Calcutta" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	Admin.associate = function (models) {
		Admin.hasMany(models.challenges, { as: "challenges", foreignKey: "adminId", sourceKey: "adminId" });

		Admin.hasMany(models.loginHists, { as: "loginHist", foreignKey: "adminId", sourceKey: "adminId" });
		Admin.hasMany(models.loginHists, { as: "loginHists", foreignKey: "adminId", sourceKey: "adminId" });

		Admin.hasMany(models.files, { as: "files", foreignKey: "adminId", sourceKey: "adminId" });

		Admin.hasMany(models.userNotifications, { as: "userNotifications", foreignKey: "adminId", sourceKey: "adminId" });
	};

	return Admin;
};
