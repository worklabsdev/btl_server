var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var AdminNotification = sequelize.define("adminNotifications", {
		adminNotificationId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		adminId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0
		},

		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		challengeId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		deviceId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0
		},
		userDeviceId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		message: { type: Sequelize.TEXT, allowNull: false, defaultValue: "" },

		type: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "" },

		isRead: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },
		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	AdminNotification.associate = function (models) {
		AdminNotification.belongsTo(models.admins, { as: "admin", foreignKey: "adminId", sourceKey: "adminId" });

		AdminNotification.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });

		AdminNotification.belongsTo(models.challenges, { as: "challenge", foreignKey: "challengeId", sourceKey: "challengeId" });

		AdminNotification.belongsTo(models.devices, { as: "device", foreignKey: "deviceId", sourceKey: "deviceId" });
		AdminNotification.belongsTo(models.userDevices, { as: "userDevice", foreignKey: "userDeviceId", sourceKey: "userDeviceId" });

	};

	return AdminNotification;
};
