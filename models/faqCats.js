var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var FaqCat = sequelize.define("faqCats", {
		faqCatId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		faqId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		categoryId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	FaqCat.associate = function (models) {
		FaqCat.belongsTo(models.faqs, { as: "faq", foreignKey: "faqId", sourceKey: "faqId" });
		FaqCat.belongsTo(models.categories, { as: "category", foreignKey: "categoryId", sourceKey: "categoryId" });
	};

	return FaqCat;
};
