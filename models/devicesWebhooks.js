var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var DevicesWebhook = sequelize.define("devicesWebhooks", {
		devicesWebhookId: {type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

        deviceId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		
		reference: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },
		requestId: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },

		devicesWebhookSubId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0
        },

        data: {type: Sequelize.JSON, defaultValue: {} },

        response: {type: Sequelize.JSON, defaultValue: {} },
		odata: {type: Sequelize.JSON, defaultValue: {} },
		actionResponse: {type: Sequelize.JSON, defaultValue: {} },
		
		updated: { type: DataTypes.ENUM("0", "1"), defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	DevicesWebhook.associate = function (models) {
		DevicesWebhook.belongsTo(models.devices, { as: "device", foreignKey: "deviceId", sourceKey: "deviceId" });


		DevicesWebhook.hasOne(models.challengeSyncs, { as: "challengeSync", foreignKey: "devicesWebhookId", sourceKey: "devicesWebhookId" });


	};

	return DevicesWebhook;
};
