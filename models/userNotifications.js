var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserNotification = sequelize.define("userNotifications", {
		userNotificationId: { type: Sequelize.BIGINT.UNSIGNED, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		ouserId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		challengeId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		deviceId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0
		},
		userDeviceId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		userDeviceSyncId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		adminId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0
		},

		userFriendId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0
		},

		message: { type: Sequelize.TEXT, allowNull: false, defaultValue: "" },

		type: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "" },

		isRead: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },
		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		creator: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "User" },
		// 'User, Admin, System'
		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	UserNotification.associate = function (models) {
		UserNotification.belongsTo(models.users, { as: "receiver", foreignKey: "userId", sourceKey: "userId" });
		UserNotification.belongsTo(models.users, { as: "sender", foreignKey: "ouserId", sourceKey: "userId" });

		UserNotification.belongsTo(models.challenges, { as: "challenge", foreignKey: "challengeId", sourceKey: "challengeId" });

		UserNotification.belongsTo(models.devices, { as: "device", foreignKey: "deviceId", sourceKey: "deviceId" });

		UserNotification.belongsTo(models.userDevices, { as: "userDevice", foreignKey: "userDeviceId", sourceKey: "userDeviceId" });
		UserNotification.belongsTo(models.userDeviceSyncs, { as: "userDeviceSync", foreignKey: "userDeviceSyncId", sourceKey: "userDeviceSyncId" });

		UserNotification.belongsTo(models.userFriends, { as: "userFriend", foreignKey: "userFriendId", sourceKey: "userFriendId" });

		UserNotification.belongsTo(models.admins, { as: "admin", foreignKey: "adminId", sourceKey: "adminId" });

	};

	return UserNotification;
};
