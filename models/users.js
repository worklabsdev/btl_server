var Sequelize = require("sequelize");
var isUrl = require('is-url');

const filesURL = appConfig.upload[appConfig.upload.name].filesURL;

module.exports = (sequelize, DataTypes) => {
	var User = sequelize.define("users", {
		userId: {
			type: Sequelize.BIGINT.UNSIGNED,
			primaryKey: true,
			autoIncrement: true
		},

		name: { type: DataTypes.STRING(255), allowNull: false },

		username: {
			type: DataTypes.STRING(100),
			allowNull: false,
			index: true
		},

		// accessToken: { type: DataTypes.TEXT, allowNull: true, defaultValue: null},

		phoneCode: { type: DataTypes.STRING(5), allowNull: false, defaultValue: "+91" },
		phoneNumber: { type: DataTypes.STRING(20), allowNull: true, defaultValue: null },

		email: {
			type: DataTypes.STRING(150),
			allowNull: false,
			index: true
		},

		facebookId: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },
		googleId: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },

		password: { type: DataTypes.TEXT, allowNull: true, defaultValue: null },
		salt: { type: DataTypes.STRING(50), allowNull: true, defaultValue: null },

		accessToken: { type: DataTypes.TEXT, allowNull: true, defaultValue: null },

		dob: { type: DataTypes.DATE, allowNull: true, defaultValue: null },

		address: { type: DataTypes.STRING(255), allowNull: true, defaultValue: null },
		state: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },
		city: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },
		country: { type: DataTypes.STRING(50), allowNull: true, defaultValue: null },
		countryISO2: { type: DataTypes.STRING(2), allowNull: true, defaultValue: null },
		zipcode: { type: DataTypes.STRING(20), allowNull: true, defaultValue: null },
		addressLatitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0
		},
		addressLongitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0
		},

		currentLatitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0
		},
		currentLongitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0
		},

		fcmId: { type: DataTypes.TEXT, allowNull: true, defaultValue: null },

		socketId: { type: DataTypes.STRING(50), allowNull: true, defaultValue: null },

		gender: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Not Disclosed" },

		profilePic: {
			type: Sequelize.TEXT,
			allowNull: true,
			defaultValue: null,
			get(field) {
				var val = this.getDataValue(field);

				if (!val) return "";
				else if(isUrl(val)) return val;
				return `${filesURL}${val}`;
			}
		},
		coverPic: {
			type: DataTypes.STRING(100),
			allowNull: true,
			defaultValue: null,
			get(field) {
				var val = this.getDataValue(field);

				if (!val) return "";
				return `${filesURL}${val}`;
			}
		},

		yellowCoins: { type: Sequelize.BIGINT, defaultValue: 5 },
		greenCoins: { type: Sequelize.BIGINT, defaultValue: 0 },
		totalYellowCoins: { type: Sequelize.BIGINT, defaultValue: 5 },
		totalGreenCoins: { type: Sequelize.BIGINT, defaultValue: 0 },

		totalDistance: { type: Sequelize.STRING(200), allowNull: false, defaultValue: "0" },
		totalSteps: { type: Sequelize.STRING(200), allowNull: false, defaultValue: "0" },

		inviteCode: {
			type: DataTypes.STRING(10),
			allowNull: false,
			unique: true,
			index: true
		},
		referalUserId: {
			type: Sequelize.BIGINT.UNSIGNED,
			allowNull: false,
			defaultValue: 0,
			index: true
		},

		otp: { type: Sequelize.STRING(4), allowNull: true, defaultValue: null },
		otpValidity: { type: DataTypes.STRING(20), allowNull: true, defaultValue: null },

		avail: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "1" },
		availDt: { type: DataTypes.STRING(20), allowNull: true, defaultValue: null },
		availDays: { type: DataTypes.INTEGER(4), allowNull: false, defaultValue: 0 },

		blocked: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },
		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },
		phoneVerified: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		challengesPlayed: { type: Sequelize.INTEGER(10), allowNull: false, defaultValue: 0 },
		challengesWon: { type: Sequelize.INTEGER(10), allowNull: false, defaultValue: 0 },
		challengesCompleted: { type: Sequelize.INTEGER(10), allowNull: false, defaultValue: 0 },

		timezone: { type: DataTypes.STRING(50), allowNull: false, defaultValue: "Asia/Calcutta" },

		deviceOS: { type: DataTypes.STRING(50), allowNull: true, defaultValue: null },

		lastSyncDt: { type: DataTypes.STRING(20), allowNull: true, defaultValue: null },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	User.associate = function (models) {
		User.hasMany(models.userDevices, { as: "userDevices", foreignKey: "userId", sourceKey: "userId" });
		User.hasOne(models.userDevices, { as: "userDevice", foreignKey: "userId", sourceKey: "userId" });

		User.hasMany(models.userDeviceSyncs, { as: "userDeviceSyncs", foreignKey: "userId", sourceKey: "userId" });
		User.hasOne(models.userDeviceSyncs, { as: "userDeviceSync", foreignKey: "userId", sourceKey: "userId" });

		//	///	User Friends
		User.hasMany(models.userFriends, { as: "userFriendsSents", foreignKey: "userId", sourceKey: "userId" });
		User.hasMany(models.userFriends, { as: "userFriendsRecs", foreignKey: "ouserId", sourceKey: "userId" });

		User.hasOne(models.userFriends, { as: "userFriendsSent", foreignKey: "userId", sourceKey: "userId" });
		User.hasOne(models.userFriends, { as: "userFriendsRec", foreignKey: "ouserId", sourceKey: "userId" });
		//	///	User Friends

		//	///	User Blockeds
		User.hasMany(models.userBlockeds, { as: "blockedUsers", foreignKey: "userId", sourceKey: "userId" });
		User.hasMany(models.userBlockeds, { as: "blockerUsers", foreignKey: "ouserId", sourceKey: "userId" });

		User.hasOne(models.userBlockeds, { as: "blockedUser", foreignKey: "userId", sourceKey: "userId" });
		User.hasOne(models.userBlockeds, { as: "blockerUser", foreignKey: "ouserId", sourceKey: "userId" });
		//	///	User Blockeds

		User.hasMany(models.challenges, { as: "challengesOwned", foreignKey: "userId", sourceKey: "userId" });
		User.hasOne(models.challenges, { as: "challengeOwned", foreignKey: "userId", sourceKey: "userId" });

		User.hasMany(models.challengeUsers, { as: "challenges", foreignKey: "userId", sourceKey: "userId" });
		User.hasOne(models.challengeUsers, { as: "challenge", foreignKey: "userId", sourceKey: "userId" });

		User.hasMany(models.userCoinsHistories, { as: "userCoinsHistories", foreignKey: "userId", sourceKey: "userId" });
		User.hasMany(models.userCoinsHistories, { as: "wonChallengeHists", foreignKey: "userId", sourceKey: "userId" });
		User.hasMany(models.userCoinsHistories, { as: "lostChallengeHists", foreignKey: "ouserId", sourceKey: "userId" });

		User.hasMany(models.userNotifications, { as: "receivedNotifications", foreignKey: "userId", sourceKey: "userId" });
		User.hasMany(models.userNotifications, { as: "sentNotifications", foreignKey: "ouserId", sourceKey: "userId" });

		User.hasMany(models.loginHists, { as: "loginHist", foreignKey: "userId", sourceKey: "userId" });
		User.hasMany(models.loginHists, { as: "loginHists", foreignKey: "userId", sourceKey: "userId" });

		User.hasMany(models.files, { as: "files", foreignKey: "userId", sourceKey: "userId" });
	};

	return User;
};
