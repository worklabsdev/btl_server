var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var ChallengeUser = sequelize.define("challengeUsers", {
		challengeUserId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		challengeId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		userType: { type: Sequelize.STRING(15), allowNull: false, defaultValue: "Creator" },

		yellowCoins: { type: Sequelize.INTEGER(11), allowNull: false, defaultValue: 0 },

		track: { type: Sequelize.STRING(100), allowNull: true, defaultValue: null },

		totalDistance: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				var val = parseFloat(this.getDataValue(field));
				return parseFloat(val.toFixed(6));
			}
		},
		totalSteps: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				return parseInt(this.getDataValue(field));
			}
		},

		currentLatitude: {
			type: Sequelize.FLOAT(9, 5), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0
		},
		currentLongitude: {
			type: Sequelize.FLOAT(9, 5), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0
		},

		history: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },

		status: { type: Sequelize.STRING(10), allowNull: false, defaultValue: "Pending" },
		// 'Pending, Won, Lost, NoResult, Deleted',

		startedAt: { type: Sequelize.STRING(20), allowNull: true },
		createdAt: { type: Sequelize.STRING(20), allowNull: true },
		updatedAt: { type: Sequelize.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	ChallengeUser.associate = function (models) {
		ChallengeUser.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });

		ChallengeUser.belongsTo(models.challenges, { as: "challenge", foreignKey: "challengeId", sourceKey: "challengeId" });
	};

	return ChallengeUser;
};
