var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Faq = sequelize.define("faqs", {
		faqId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		question: { type: DataTypes.TEXT, allowNull: false },
		answer: { type: DataTypes.TEXT, allowNull: false },

		ranking: { type: Sequelize.INTEGER(10), allowNull: false, defaultValue: 0 },

		type: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "FAQ" },

		blocked: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	Faq.associate = function (models) {
		Faq.belongsToMany(models.categories, { as: "categories", through: "faqCats", foreignKey: "faqId" });
	};

	return Faq;
};
