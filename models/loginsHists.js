var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var LoginHist = sequelize.define("loginHists", {

		loginsHistId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		adminId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		accessToken: { type: DataTypes.TEXT, allowNull: true, defaultValue: null },

		latitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0
		},
		longitude: {
			type: DataTypes.FLOAT(9, 5), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0
		},

		timezone: { type: DataTypes.STRING(50), allowNull: false, defaultValue: "Asia/Calcutta" },

		odata: { type: DataTypes.JSON, defaultValue: {}, required: false },

		isValid: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "1" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }

	},
	{
		    underscored: false,
		    timestamps: false,
	});

	LoginHist.associate = function (models) {
		LoginHist.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });
		LoginHist.belongsTo(models.admins, { as: "admin", foreignKey: "adminId", sourceKey: "adminId" });
	};

	return LoginHist;
};
