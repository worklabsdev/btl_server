var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserCoinsHistory = sequelize.define("userCoinsHistories", {
		
		userCoinsHistoryId: { type: Sequelize.BIGINT.UNSIGNED, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		ouserId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		challengeId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},

		adminId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},

		yellowCoins: { type: Sequelize.BIGINT, allowNull: false, defaultValue: 0 },
		greenCoins: { type: Sequelize.BIGINT, allowNull: false, defaultValue: 0 },

		type: { type: Sequelize.STRING(20), allowNull: false, defaultValue: "challengeWon" },

		deleted: { type: Sequelize.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: Sequelize.STRING(20), allowNull: true },
		updatedAt: { type: Sequelize.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	UserCoinsHistory.associate = function (models) {
		UserCoinsHistory.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });
		UserCoinsHistory.belongsTo(models.users, { as: "ouser", foreignKey: "userId", sourceKey: "ouserId" });

		UserCoinsHistory.belongsTo(models.challenges, { as: "challenge", foreignKey: "challengeId", sourceKey: "challengeId" });
	};

	return UserCoinsHistory;
};
