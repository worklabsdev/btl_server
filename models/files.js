var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var File = sequelize.define("files", {

		fileId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		adminId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
        },
        challengeId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
        },
        userDeviceId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
        },

        file: { type: DataTypes.STRING(100), allowNull: false, defaultValue: "" },
		type: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Image" },
		link: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },

		description: { type: DataTypes.TEXT, allowNull: true, defaultValue: null },

		blocked: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },
		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }

	},
	{
		    underscored: false,
		    timestamps: false,
	});

	File.associate = function (models) {
		File.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });
		File.belongsTo(models.admins, { as: "admin", foreignKey: "adminId", sourceKey: "adminId" });
	};

	return File;
};
