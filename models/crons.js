var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Cron = sequelize.define("crons", {
		cronId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		reference: { type: DataTypes.STRING(100), allowNull: false },

		challengeId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		requestId: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },

		action: { type: Sequelize.JSON, allowNull: true, defaultValue: {} },
        error: { type: Sequelize.JSON, allowNull: true, defaultValue: {} },
        odata: { type: Sequelize.JSON, allowNull: true, defaultValue: {} },


		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	// Cron.associate = function (models) {
	// };

	return Cron;
};
