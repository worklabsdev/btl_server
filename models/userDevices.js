var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserDevice = sequelize.define("userDevices", {
		userDeviceId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		deviceId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		deviceUserId: {
			type: Sequelize.STRING(100), allowNull: true, defaultValue: null
		},
		accessToken: {
			type: Sequelize.TEXT, allowNull: true, defaultValue: null
		},
		refreshToken: {
			type: Sequelize.TEXT, allowNull: true, defaultValue: null
		},

		totalDistance: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				var val = parseFloat(this.getDataValue(field));
				return parseFloat(val.toFixed(6));
			}
		},
		totalSteps: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				return parseInt(this.getDataValue(field));
			}
		},

		profileData: {type: Sequelize.JSON, defaultValue: {} },
		odata: {type: Sequelize.JSON, defaultValue: {} },

		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },
		isValid: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	UserDevice.associate = function (models) {
		UserDevice.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });

		UserDevice.belongsTo(models.devices, { as: "device", foreignKey: "deviceId", sourceKey: "deviceId" });

		UserDevice.hasMany(models.userNotifications, { as: "notifications", foreignKey: "userDeviceId", sourceKey: "userDeviceId" });

		UserDevice.hasMany(models.files, { as: "files", foreignKey: "userDeviceId", sourceKey: "userDeviceId" });


		UserDevice.hasOne(models.userDeviceSyncs, { as: "userDeviceSync", foreignKey: "userDeviceId", sourceKey: "userDeviceId" });
		UserDevice.hasMany(models.userDeviceSyncs, { as: "userDeviceSyncs", foreignKey: "userDeviceId", sourceKey: "userDeviceId" });

	};

	return UserDevice;
};
