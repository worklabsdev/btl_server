const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");

const basename = path.basename(__filename);
const db = {};

const mysqlConfig = appConfig.db.mysql;

const dbConfig = {
	"host": mysqlConfig.dbHost,
	"dialect": mysqlConfig.dbConnection,
	"port": mysqlConfig.dbPort,
	"logging": false,
	"pool": {
		"max": 20,
		"min": 5,
		"idle": 10000,
	},
	"dialectOptions": {
    	"dateStrings": true,
	    "typeCast": function (field, next) { // for reading from database
	        if (field.type === "DATETIME") {
	        	return field.string();
	        }
	        return next();
	    },
	},

};

const sequelize = new Sequelize(mysqlConfig.dbName, mysqlConfig.dbUser, mysqlConfig.dbPassword, dbConfig);

fs
	.readdirSync(__dirname)
	.filter((file) => {
		return (file.indexOf(".") !== 0) && (file !== basename) && (file.slice(-3) === ".js");
	})
	.forEach((file) => {
		let model = sequelize["import"](path.join(__dirname, file));
		db[model.name] = model;
	});

Object.keys(db).forEach((modelName) => {
	if (db[modelName].associate) {
		db[modelName].associate(db);
	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
