var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {

	var AppSetting = sequelize.define("appSettings", {

		appSettingId: {
			type: Sequelize.BIGINT.UNSIGNED,
			primaryKey: true,
			autoIncrement: true
		},

		appVersions: {
			type: Sequelize.JSON,
			allowNull: false,
			defaultValue: {}
		},
		coinsSettings: {
			type: Sequelize.JSON,
			allowNull: false,
			defaultValue: {}
		},

		redeemFactor: {
			type: Sequelize.SMALLINT.UNSIGNED,
			allowNull: false,
			defaultValue: 2
		},

		defYellowCoins: {
			type: Sequelize.BIGINT.UNSIGNED,
			allowNull: false,
			defaultValue: 5
		},
		referalNewYellowCoins: {
			type: Sequelize.BIGINT.UNSIGNED,
			allowNull: false,
			defaultValue: 5
		},
		referalOldYellowCoins: {
			type: Sequelize.BIGINT.UNSIGNED,
			allowNull: false,
			defaultValue: 5
		},

		deviceUpdateRange: {
			type: Sequelize.JSON,
			allowNull: false,
			defaultValue: {
				end: "06:00",
				start: "02:00"
			}
		},

		createdAt: {
			type: Sequelize.STRING(20),
			allowNull: true
		},
		updatedAt: {
			type: Sequelize.STRING(20),
			allowNull: true
		}

	},
	{
		    underscored: false,
		    timestamps: false,
	});

	AppSetting.associate = function (models) {
	};

	return AppSetting;
};
