var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserDeviceSync = sequelize.define("userDeviceSyncs", {
		userDeviceSyncId: { type: Sequelize.BIGINT.UNSIGNED, primaryKey: true, autoIncrement: true },

		userDeviceId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},
		userId: {
			type: Sequelize.BIGINT.UNSIGNED, allowNull: false, defaultValue: 0, index: true
		},

		totalDistance: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				var val = parseFloat(this.getDataValue(field));
				return parseFloat(val.toFixed(6));
			}
		},
		totalSteps: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				return parseInt(this.getDataValue(field));
			}
		},

		oldDistance: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				var val = parseFloat(this.getDataValue(field));
				return parseFloat(val.toFixed(6));
			}
		},
		oldSteps: {
			type: Sequelize.STRING(200),
			allowNull: false,
			defaultValue: "0",
			get(field) {
				return parseInt(this.getDataValue(field));
			}
		},

		dt: { type: Sequelize.STRING(20), allowNull: true, defaultValue: null },

		deleted: { type: DataTypes.ENUM("0", "1"), allowNull: false, defaultValue: "0" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	UserDeviceSync.associate = function (models) {
		UserDeviceSync.belongsTo(models.userDevices, { as: "userDevice", foreignKey: "userDeviceId", sourceKey: "userDeviceId" });

		UserDeviceSync.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });

		UserDeviceSync.hasMany(models.userNotifications, { as: "notifications", foreignKey: "userDeviceSyncId", sourceKey: "userDeviceSyncId" });

		UserDeviceSync.hasOne(models.challengeSyncs, { as: "challengeSync", foreignKey: "userDeviceSyncId", sourceKey: "userDeviceSyncId" });

	};

	return UserDeviceSync;
};
