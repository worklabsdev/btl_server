var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserFriend = sequelize.define("userFriends", {
		userFriendId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

		userId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},
		ouserId: {
			type: Sequelize.BIGINT, allowNull: false, defaultValue: 0, index: true
		},

		status: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "Pending" },

		createdAt: { type: DataTypes.STRING(20), allowNull: true },
		updatedAt: { type: DataTypes.STRING(20), allowNull: true }
	},
	{
		    underscored: false,
		    timestamps: false,
	});

	UserFriend.associate = function (models) {
		UserFriend.belongsTo(models.users, { as: "user", foreignKey: "userId", sourceKey: "userId" });
		UserFriend.belongsTo(models.users, { as: "ouser", foreignKey: "ouserId", sourceKey: "userId" });


		UserFriend.hasMany(models.userNotifications, { as: "notifications", foreignKey: "userFriendId", sourceKey: "userFriendId" });

	};

	return UserFriend;
};
