const Sequelize = require("sequelize");

const { globalMessages } = require("../properties/constant");

const filesURL = appConfig.upload[appConfig.upload.name].filesURL;

const Db = require("../models");

const { authHandler, authHandlerView, failedActionHandler } = require("../commonFuncs/responseHandler");

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

const messages = {
	UserNotAvail: "This user is currently not available"
};

const loginTypes = {
	"google": "Google",
	"facebook": "Facebook",
	"normal": "Normal",
	"signup": "Signup"
};
exports.loginTypes = loginTypes;

const genderTypes = {
	"male": "Male",
	"female": "Female",
	"notDisclosed": "Not Disclosed"
};
exports.genderTypes = genderTypes;

//	///		Constants
const searchUserWhereRaw = (search) => {
	if (!search) return "";
	return `AND ( (users.name LIKE "%${search}%") OR (users.email LIKE "%${search}%") OR (users.phoneNumber LIKE "%${search}%") OR (users.username LIKE "%${search}%") )`;
};
exports.searchUserWhereRaw = searchUserWhereRaw;

//const userProfilePicSeq = Sequelize.col(`CASE WHEN profilePic="" THEN "" WHEN profilePic LIKE "http:%" THEN profilePic ELSE CONCAT("${filesURL}", profilePic)`, "profilePic");
//exports.userProfilePicSeq = userProfilePicSeq;

const userProfilePic = `CASE WHEN profilePic="" THEN "" WHEN profilePic LIKE "http%" THEN profilePic ELSE CONCAT("${filesURL}", profilePic) END AS profilePic`;
exports.userProfilePic = userProfilePic;

const userCoverPic = `CASE WHEN coverPic="" THEN "" ELSE CONCAT("${filesURL}", coverPic) END AS coverPic`;
exports.userCoverPic = userCoverPic;

const userGoogleLinked = `CASE WHEN googleId IS NULL THEN "0" ELSE "1" END AS googleLinked`;
exports.userGoogleLinked = userGoogleLinked;

const userFacebookLinked = `CASE WHEN facebookId IS NULL THEN "0" ELSE "1" END AS facebookIdLinked`;
exports.userFacebookLinked = userFacebookLinked;


const userGenderRankSQL = `CASE WHEN gender="${genderTypes.female}" THEN 1 WHEN gender="${genderTypes.male}" THEN 2 ELSE 3 END`;
exports.userGenderRankSQL = userGenderRankSQL;

const userDobRankSQL = `CASE WHEN dob is NULL THEN CURDATE() ELSE dob END`;
exports.userDobRankSQL = userDobRankSQL;
//(CASE WHEN U.gender=${genderTypes.female} THEN)

let sequelizeAtts = {
	"usersList": {
		"include": ["userId", "name", "email", "createdAt", "username", "phoneCode", "phoneNumber", "phoneVerified", "profilePic", "yellowCoins", "greenCoins", "blocked", "inviteCode"],
	}
};

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//	Api 
exports.userSignInDetailsAuth = (response, accessToken) => {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await Db.users.findOne(
				{
					where: {
						accessToken,
						deleted: "0"
					}
				},
				{
					raw: true
				}
			);
			if (!user) return authHandler(response, 2);
			if (user.blocked === "1") return authHandler(response, 3);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

//	View
exports.userSignInDetailsAuthView = (response, accessToken) => {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await Db.users.findOne(
				{
					where: {
						accessToken,
						deleted: "0"
					},
					attributes: ["userId", "name", "email", "username", "phoneCode", "phoneNumber"]
				},
				{
					raw: true
				}
			);
			if (!user) return authHandlerView(response, 2);
			if (user.blocked === "1") return authHandlerView(response, 3);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

//	User Valid Check
exports.userValidCheck = (response, whereJSON, attributes=null) => {
	return new Promise(async (resolve, reject) => {
		try {

			let opWhere = {
				where: whereJSON
			};
			if(attributes) {
				opWhere.attributes = attributes;
			}

			let user = await Db.users.findOne(
				opWhere,
				{
					raw: true
				}
			);
			if (!user) 
				return failedActionHandler(response, messages.UserNotAvail);
			return resolve(user);
		} catch (error) {
			return reject(error);
		}
	});
};

// ///       User Update
exports.userUpdate = (response, whereJSON, data, options = null) => {
	let updatedOjs = {
		...data
	};

	if (!Object.keys(updatedOjs).length) 
		return failedActionHandler(response, globalMessages.responseMessages.nothingToUpdate);

	return Db.users.update(
		updatedOjs,
		{
			where: whereJSON,
			...options, // /	For Options Like Transactions
			// logging: console.log
		}
	);
};

// ///       User Update Service
exports.userUpdateSer = (whereJSON, data, options = {}) => {
	return Db.users.update(
		data,
		{
			where: whereJSON,
			...options,
			// logging: console.log
		}
	);
};

//  ///     Admin Users List
exports.usersListSer = (data) => {
	return Db.users.findAll({
		where: Sequelize.literal(data.listWhereRawSeq),
		attributes: sequelizeAtts.usersList.include,
		offset: data.offset,
		limit: data.pageSize,
		order: data.sortByArray,
		// logging: console.log
	});
};

exports.usersCountSer = (listWhereRawSeq) => {
	return Db.users.count({
		where: Sequelize.literal(listWhereRawSeq)
	});
};