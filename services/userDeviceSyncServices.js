const Db = require("../models");

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const userDevicesSyncSerAtts = {
	detailsAtts: []
};
exports.userDevicesSyncSerAtts = userDevicesSyncSerAtts;

let sequelizeAtts = {
	"userDeviceSyncList": {
		"include": ["userDeviceSyncId", "userDeviceId", "userId", "totalDistance", "totalSteps", "dt", "deleted", "createdAt", "updatedAt"],
	}
};
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//  Get One
const userDeviceSyncGetOne = (whereJSON, options={}) => {
	return Db.userDeviceSyncs.findOne({
		where: whereJSON,
		...options
	});
};
exports.userDeviceSyncGetOne = userDeviceSyncGetOne;

//  Create Row
const userDeviceSyncCreate = (data, options={}) => {
	return Db.userDeviceSyncs.create(
		data,
		options
	);
};
exports.userDeviceSyncCreate = userDeviceSyncCreate;

// ///       Device Update
const userDeviceSyncUpdate = (whereJSON, data, options = null) => {
	return Db.userDeviceSyncs.update(
		data,
		{
			where: whereJSON,
			...options, // /	For Options Like Transactions
			// logging: console.log
		}
	);
};
exports.userDeviceSyncUpdate = userDeviceSyncUpdate;

//	Admin User Sync Count
const userDeviceSyncsCountSer = (listWhereRawSeq) => {
	return Db.userDeviceSyncs.count({
		where: Db.Sequelize.literal(listWhereRawSeq)
	});
};
exports.userDeviceSyncsCountSer = userDeviceSyncsCountSer;

//	Admin User Sync List
const userDeviceSyncsListSer = (data) => {
	return Db.userDeviceSyncs.findAll({
		where: Db.Sequelize.literal(data.listWhereRawSeq),
		attributes: sequelizeAtts.userDeviceSyncList.include,
		offset: data.offset,
		limit: data.pageSize,
		order: data.sortByArray,
		//logging: console.log
	});
};
exports.userDeviceSyncsListSer = userDeviceSyncsListSer;