const cronTypes = {
    "userChalStart": "UserChalStart",
    "userChalEnd": "UserChalEnd",
    
    "userOngoingChalSync": "UserOngoingChalSync",

    "adminChalStart": "AdminChalStart",
    "adminChalEnd": "AdminChalEnd"
};
exports.cronTypes = cronTypes;