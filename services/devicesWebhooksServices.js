const Db = require("../models");

//      Webhook Create
const devWebhooksSerCreate = (data) => {
	return Db.devicesWebhooks.create(data);
};
exports.devWebhooksSerCreate = devWebhooksSerCreate;

//		Webhook Update
const devWebhookSerUpdate = (whereJSON, newData, options={}) => {
	return Db.devicesWebhooks.update(
		newData,
		{
			where: whereJSON,
			...options, // /	For Options Like Transactions
			//logging: console.log
		}
	);
};
exports.devWebhookSerUpdate = devWebhookSerUpdate;