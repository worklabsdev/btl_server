const Db = require("../models");

const attributes = {
	// appDevicesListAtts: { "exclude": ["blocked", "createdAt", "updatedAt", "ranking"] }
};
exports.attributes = attributes;

const messages = {
};

//  Insert History
createLoginHist = (data) => {
	return Db.loginHists.create(data);
};
exports.createLoginHist = createLoginHist;

//	Update Data
const updateLoginHist = (whereJSON, newData, options = {}) => {

	return Db.loginHists.update(
		newData,
		{
			where: whereJSON,
			...options, // /	For Options Like Transactions
			//logging: console.log
		}
	);

};
exports.updateLoginHist = updateLoginHist;
