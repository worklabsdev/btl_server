const Db = require("../models");

const Sequelize = require("sequelize");

const { commonSerAttributes } = require("./commonServices");

const userDevicesSerMessages = {
	"UserDeviceNotAvailMsg": "Sorry, this user device is currently not available"
};
exports.userDevicesSerMessages = userDevicesSerMessages;

const userDevicesSerAtts = {
	detailsAtts: []
};
exports.userDevicesSerAtts = userDevicesSerAtts;

//  Get One
const userDeviceGetOne = (whereJSON) => {
	return Db.userDevices.findOne({
		where: whereJSON
	});
};
exports.userDeviceGetOne = userDeviceGetOne;

//  Create Row
const userDeviceCreate = (data) => {
	return Db.userDevices.create({
		...data
	});
};
exports.userDeviceCreate = userDeviceCreate;

// ///       Device Update
userDeviceUpdate = (whereJSON, data, options = null) => {

	return Db.userDevices.update(
		data,
		{
			where: whereJSON,
			...options, // /	For Options Like Transactions
			// logging: console.log
		}
	);

};
exports.userDeviceUpdate = userDeviceUpdate;

//	///		User Device 
const userDeviceGetDetails4Webhook = (whereJSON, options) => {

	let includeArray = [
		{
			as: "user",
			attributes: ["userId", "name", "email", "totalDistance", "totalSteps"],
			model: Db.users,
			required: true
		}
	];

	if(options.userDeviceSyncWhere) {
		includeArray.push({
			model: Db.userDeviceSyncs,
			as: "userDeviceSync",
			attributes: ["userDeviceSyncId", "userDeviceId", "totalDistance", "totalSteps", "oldDistance", "oldSteps"],
			required: false,
			where: options.userDeviceSyncWhere
		});
	}

	return Db.userDevices.findOne({
		where: whereJSON,
		attributes: {
			"exclude": ["odata", "isValid", "deleted", "createdAt", "updatedAt"]
		}, 
		include: includeArray,
		//logging: console.log
	});
};
exports.userDeviceGetDetails4Webhook = userDeviceGetDetails4Webhook;

//  User Device For Push
const userDeviceDetails4Push = (whereJSON) => {

    return Db.userDevices.findOne({
		where: whereJSON,
		attributes: {
			exclude: ["profileData", "odata"]
		},
		include: [
			{
				model: Db.users,
				as: "user",
				attributes: commonSerAttributes.user4PushNotificationAtts
			}
		]
	})
	.then((userDevice) => {
		return JSON.parse(JSON.stringify(userDevice));
	})

};
exports.userDeviceDetails4Push = userDeviceDetails4Push;

//		User Device Details
const userDeviceDetails4SyncSerGet = (whereJSON) => {

	return Db.userDevices.findOne({
		where: whereJSON,
		attributes: [ "userDeviceId", "userId", "deviceId", "isValid" ],
		include: [
			{
				model: Db.userDeviceSyncs,
				as: "userDeviceSync",
				required: false,
				attributes: [
					"userDeviceSyncId", "dt", "totalDistance", "totalSteps"
				]
			}
		],
		order: [
			["userDeviceSync", "userDeviceSyncId", "DESC"]
		]
		//logging: console.log
	});

};
exports.userDeviceDetails4SyncSerGet = userDeviceDetails4SyncSerGet;

//	User Devices Get
const userDevicesListSer = (data) => {

	return Db.userDevices.findAll({
		where: Sequelize.literal(data.listWhereRawSeq),
		attributes: commonSerAttributes.adminUserDevicesAtts,

		include: [
			{
				model: Db.devices,
				as: "device",
				attributes: commonSerAttributes.commonDeviceAtts
			}
		],

		offset: data.offset,
		limit: data.pageSize,
		order: data.sortByArray,
		//logging: console.log
	});

};
exports.userDevicesListSer = userDevicesListSer;

//	User Devices Count
const userDevicesCountSer = (whereJSON) => {

	return Db.userDevices.count({
		where: whereJSON
	});

};
exports.userDevicesCountSer = userDevicesCountSer;

//	User Device Valid Check
exports.userDeviceValidCheckSer = (response, whereJSON, attributes=null) => {
	return new Promise(async (resolve, reject) => {
		try {

			let opWhere = {
				where: whereJSON,
				include: [
					{
						model: Db.devices,
						as: "device",
						attributes: commonSerAttributes.commonDeviceAtts
					}
				]
			};
			if(attributes) {
				opWhere.attributes = attributes;
			}

			let userDevice = await Db.userDevices.findOne(
				opWhere,
				{
					raw: true
				}
			);
			if (!userDevice) 
				return failedActionHandler(response, userDevicesSerMessages.UserDeviceNotAvailMsg);

			return resolve(userDevice);
		} catch (error) {
			return reject(error);
		}
	});
};