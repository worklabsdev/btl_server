const Db = require("../models");

const attributes = {
    appFilesAtts: ["fileId", "file", "type", "description", "link"],
    adminFilesAtts: ["fileId", "file", "type", "description", "link", "blocked"]
};
exports.attributes = attributes;

const messages = {
};

//	File Types
const fileServiceTypes = {
	"sponsorChallenge": "SponsorChal",
	"startChallenge": "StartChal",
	"endChallenge": "EndChal"
};
exports.fileServiceTypes = fileServiceTypes;

const fileSeqIncludes = {
    "filesIncludeSeq": {
        model: Db.files,
        as: "files",
        where: {
            deleted: "0",
            blocked: "0"
        },
        attributes: attributes.appFilesAtts,
        required: false
    }
};
exports.fileSeqIncludes = fileSeqIncludes;

//  Insert File Row
exports.createFiles = (data) => {
	return Db.files.create(data);
};

//  Insert File Row
exports.bulkCreateFilesSer = (data) => {
	return Db.files.bulkCreate(data);
};

//  Delete File
exports.delFileSer = (whereJSON) => {
    return Db.files.destroy({
        where: whereJSON
    });
}