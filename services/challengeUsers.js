const Db = require("../models");

//	Challenge User Types
const challengeUsersTypes = {
	"creator": "Creator",
	"challenger": "Challenger"
};
exports.challengeUsersTypes = challengeUsersTypes;

//	Challenge User Statuses
const challengeUserStatuses = {
	"pending": "Pending",
	"accepted": "Accepted",
	"started": "Started",
	"won": "Won",
	"lost": "Lost",
	"noResult": "NoResult",
	"deleted": "Deleted",
};
exports.challengeUserStatuses = challengeUserStatuses;

// Pending, Accepted, Won, Lost, NoResult, Deleted

// const challengeUsersTypes = {
// 	""
// 	"deleted": "Deleted"
// };
// exports.challengeUserStatuses = challengeUserStatuses;

exports.updateChallengeUsers = (whereJSON, data, opts = null) => {
	return Db.challengeUsers.update(
		data,
		{
			where: whereJSON,
			...opts
		},
	);
};

exports.createChallengeUser = (data) => {
	return Db.challengeUsers.create(data);
};