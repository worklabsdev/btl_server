const Db = require("../models");

const { appSettingAtts } = require("./commonServices").commonSerAttributes;

const { authHandler } = require("../commonFuncs/responseHandler");

//	Auth Check
const adminSignInDetailsAuth = (response, adminId, accessToken) => {
	return new Promise(async (resolve, reject) => {
		try {
			let admin = await Db.admins.findOne(
				{
					where: {
						adminId,
						accessToken
					}
				},
				{
					raw: true
				}
			);
			if (!admin) return authHandler(response, 2);
			return resolve(admin);
		} catch (error) {
			return reject(error);
		}
	});
};
exports.adminSignInDetailsAuth = adminSignInDetailsAuth;

//	One Admin Get
const adminGetOneSer = (whereJSON, attributes=appSettingAtts) => {
	return Db.admins.findOne({ 
		where: whereJSON,
		attributes
	});
};
exports.adminGetOneSer = adminGetOneSer;