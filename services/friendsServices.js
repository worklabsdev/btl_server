const Db = require("../models");

const attributes = {
};
exports.attributes = attributes;

//  Status
const fReqStatuses = {
	"accepted": "Accepted",
	"rejected": "Rejected",
	"pending": "Pending"
};
exports.fReqStatuses = fReqStatuses;

const friendsSerMsgs = {

};
exports.friendsSerMsgs = friendsSerMsgs;

const fileSeqIncludes = {
};
exports.fileSeqIncludes = fileSeqIncludes;

const commonFriendCheckSQL = (userId) => {

    let friendCheck = `((SELECT COUNT(*) FROM userFriends UF WHERE ((UF.userId=${userId} AND UD.ouserId=userId) OR (UF.ouserId=${userId} AND UD.userId=userId)) AND (status=) ) = 0)`

};
exports.commonFriendCheckSQL = commonFriendCheckSQL;

//	User Friends Count SQL
const userFriendsCountSQL = (userId) => {
	
	return `(SELECT COUNT(*) FROM userFriends UF WHERE (UF.userId=${userId} OR UF.ouserId=${userId}) AND UF.status="${fReqStatuses.accepted}")`;

};
exports.userFriendsCountSQL = userFriendsCountSQL;