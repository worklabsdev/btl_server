const Db = require("../models");

const { pageOffsetCreator } = require("../utils/common");

const { commonUserNotsAtts, userDetailsAtts, commonChalAtts, commonChalUserAtts, commonDeviceAtts, commonUserFriendsAtts } = require("./commonServices").commonSerAttributes;

// ///  Create New Record
exports.createNotification = (data, options={}) => {
	return Db.userNotifications.create(
		data,
		options
	);
};

// 	///		Update Record
exports.updateNotificationsSer = (whereJSON, data, opts = {}) => {
	return Db.userNotifications.update(
		data,
		{
			where: whereJSON
		},
		opts
	);
};

//	///		User Notifications List Count
exports.userNotsCountSer = (searchWhere) => {
	return Db.userNotifications.count({
		where: searchWhere
	})
};

//	///		User Notifications Listings
exports.userNotsListSer = (data) => {

	data = pageOffsetCreator(data);

	return Db.userNotifications.findAll({

		where: data.searchWhere,

		attributes: commonUserNotsAtts,

		include: [
			{
				model: Db.users,
				as: "sender",
				required: false,
				attributes: userDetailsAtts
			},
			{
				model: Db.challenges,
				as: "challenge",
				required: false,
				attributes: commonChalAtts,
				include: [
					{
						model: Db.challengeUsers,
						as: "chalUser",
						where: {
							userId: data.userId
						},
						required: false,
						attributes: commonChalUserAtts
					}
				]
			},
			{
				model: Db.devices,
				as: "device",
				required: false,
				attributes: commonDeviceAtts
			},
			{
				model: Db.userFriends,
				as: "userFriend",
				required: false,
				attributes: commonUserFriendsAtts
			}

		],

		order: [
			["userNotificationId", "DESC"]
		],

		offset: data.offset,
		limit: data.pageSize,

	});

};