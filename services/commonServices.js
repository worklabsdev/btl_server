var Sequelize = require("sequelize");

const commonSerAttributes = {

    appSettingAtts: ["appVersions", "coinsSettings", "redeemFactor", "defYellowCoins", "referalNewYellowCoins", "referalOldYellowCoins", "deviceUpdateRange"],

    user4PushNotificationAtts: ["userId", "username", "name", "fcmId", "socketId", "blocked", "deleted", "deviceOS"],
    userDetailsAtts: ["userId", "name", "username", "phoneCode", "phoneNumber", "email", "profilePic"],
    adminUserDetailsAtts: ["userId", "name", "username", "phoneCode", "phoneNumber", "email", "dob", "address", "state", "city", "country", "zipcode", "addressLatitude", "addressLongitude", "gender", "profilePic", "coverPic"],

    userDeviceAttsNormal: {
        "exclude": ["profileData", "odata", "accessToken", "refreshToken", "createdAt", "updatedAt"]
    },
    adminUserDevicesAtts: {
        "exclude": ["profileData", "odata", "accessToken", "refreshToken"]
    },

    commonChalAtts: ["challengeId", "startAddress", "endAddress", "startTitle", "endTitle", "startDt", "endDt"],

    commonChalUserAtts: ["challengeUserId", "userId", "userType", "totalDistance", "totalSteps", "status"],

    commonDeviceAtts: ["deviceId", "name"],

    commonUserFriendsAtts: ["userFriendId", "status"],

    commonUserNotsAtts: {
        "exclude": ["deleted", "updatedAt"]
    },

    userDeviceSyncAtts: ["userDeviceSyncId", "userDeviceId", "dt", "totalDistance", "totalSteps", "oldDistance", "oldSteps"],
    chalCronSyncAtts: ["challengeId", "userId", "adminId", "startAddress", "startTitle", "endAddress", "endTitle", "totalDistance", "startDt", "endDt", "totalDays", "coins", "sponsorTitle"],
    chalUserCronSyncAtts: ["challengeUserId", "challengeId", "userId", "userType", "yellowCoins", "totalDistance", "totalSteps", "status"            
    ],
    chalSyncsAtts: ["challengeSyncId", "distance", "steps"],

};
exports.commonSerAttributes = commonSerAttributes;

const waitTimeIncvalue = 5000;
exports.waitTimeIncvalue = waitTimeIncvalue;

const devicesTypes = {
    "android": "ANDROID",
    "ios": "IOS"
};
exports.devicesTypes = devicesTypes;

// const commonSeqAtts = {
//     "totalDistUpdate": commonSeqAtts.literal()
// }

//  Total Dist Increment
totalDistsStepsIncFunc = (incrementDistance, incrementSteps) => {

    return {
        "totalDistance": Sequelize.literal(`CASE WHEN (totalDistance+${incrementDistance}) >= 0 THEN (totalDistance + ${incrementDistance}) ELSE 0 END`),
        "totalSteps": Sequelize.literal(`CASE WHEN (totalSteps + ${incrementSteps}) >= 0 THEN (totalSteps + ${incrementSteps}) ELSE 0 END`)
    };

};
exports.totalDistsStepsIncFunc = totalDistsStepsIncFunc;

//  Old Dist Increment
oldDistsStepsIncFunc = (incrementDistance, incrementSteps) => {

    return {
        "oldDistance": Sequelize.literal(`CASE WHEN (oldDistance+${incrementDistance}) >= 0 THEN (oldDistance + ${incrementDistance}) ELSE 0 END`),
        "oldSteps": Sequelize.literal(`CASE WHEN (oldSteps + ${incrementSteps}) >= 0 THEN (oldSteps + ${incrementSteps}) ELSE 0 END`)
    };

};
exports.oldDistsStepsIncFunc = oldDistsStepsIncFunc;

let precisionDistValue = 6;
exports.precisionDistValue = precisionDistValue;