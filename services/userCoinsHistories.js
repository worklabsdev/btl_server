const Sequelize = require("sequelize");

const { userProfilePic, userGenderRankSQL, userDobRankSQL } = require("./userServices");
const { fReqStatuses } = require("./friendsServices");

const Db = require("../models");

exports.createUserCoinsHistories = (data, options = {}) => {
	return Db.userCoinsHistories.create(
		data,
		options
	);
};

exports.updateUserCoinsHistory = (whereJSON, data, opts = null) => {
	return Db.userCoinsHistories.update(
		data,
		{ where: whereJSON },
		opts
	);
};

//	Global LeaderBoard Getter
exports.globalLeaderListSer = (data) => {

	//	One User Rank
	let havingRaw = `HAVING userId != 0`;
	if(data.getUserRank) {
		havingRaw = `HAVING userId = ${data.userId}`;
	}
	//	One User Rank

	return Db.sequelize.query(`
	SELECT 
		userId, name, username, address, city, state, country, countryISO2, zipcode, profilePic,

		greenCoinsSum,
		totalDistanceSum,
		gender,
		dob,

		@rownum:=@rownum+1 as rank
		
		FROM (
			SELECT 
				U0.userId, name, username, address, city, state, country, countryISO2, zipcode,
				${userProfilePic},

				CAST(COALESCE(greenCoinsSum, 0) AS UNSIGNED) as greenCoinsSum,

				COALESCE(totalDistanceSum, 0) as totalDistanceSum,

				${userGenderRankSQL} as gender,

				${userDobRankSQL} as dob,
			
				U0.createdAt

			FROM users U0

			LEFT JOIN (
				SELECT
					SUM(UCH.greenCoins) as greenCoinsSum,
					UCH.userId
				FROM userCoinsHistories UCH
				WHERE UCH.createdAt BETWEEN "${data.startDt}" AND "${data.endDt}"
				GROUP BY UCH.userId
			)
			UCH1 ON (UCH1.userId=U0.userId)

			LEFT JOIN (
				SELECT
					SUM(UDS.totalDistance) as totalDistanceSum,
					UDS.userId
				FROM userDeviceSyncs UDS
				WHERE (UDS.deleted="0") AND (UDS.createdAt BETWEEN "${data.startDt}" AND "${data.endDt}")
				GROUP BY UDS.userId
			)
			UDS1 ON (UDS1.userId=U0.userId)


			WHERE (U0.blocked="0") AND (U0.deleted="0") AND (U0.name <> "") AND (U0.phoneVerified="1")

			#HAVING greenCoinsSum > 0

			ORDER BY 
				greenCoinsSum DESC,
				totalDistanceSum DESC,
				dob ASC,
				gender ASC,
				createdAt ASC

		) U1,
		(select @rownum:=0) R

		${havingRaw}

		ORDER BY rank ASC
		LIMIT ${data.offset}, ${data.pageSize}
		`,
		{ 
			type: Sequelize.QueryTypes.SELECT,
			//logging: console.log
		});

};

//	Friends LeaderBoard Getter
exports.friendsLeaderListSer = (data) => {

	//	One User Rank
	let havingRaw = `HAVING userId != 0`;
	if(data.getUserRank) {
		havingRaw = `HAVING userId = ${data.userId}`;
	}
	//	One User Rank

	return Db.sequelize.query(`
	SELECT 
		userId, name, username, address, city, state, country, countryISO2, zipcode, profilePic,

		greenCoinsSum,
		totalDistanceSum,
		gender,
		dob,
		#friendCount,

		@rownum:=@rownum+1 as rank
		
		FROM (
			SELECT 
				U0.userId, name, username, address, city, state, country, countryISO2, zipcode,
				${userProfilePic},

				CAST(COALESCE(greenCoinsSum, 0) AS UNSIGNED) as greenCoinsSum,

				COALESCE(totalDistanceSum, 0) as totalDistanceSum,

				${userGenderRankSQL} as gender,

				${userDobRankSQL} as dob,
			
				U0.createdAt,

				(SELECT COUNT(*) FROM userFriends UF WHERE 
					(UF.status="${fReqStatuses.accepted}") AND
					(
						(UF.userId=U0.userId AND UF.ouserId=${data.userId})
						OR
						(UF.ouserId=U0.userId AND UF.userId=${data.userId})
						OR
						(U0.userId=${data.userId})
					)
				) as friendCount

			FROM users U0

			LEFT JOIN (
				SELECT
					SUM(UCH.greenCoins) as greenCoinsSum,
					UCH.userId
				FROM userCoinsHistories UCH
				WHERE UCH.createdAt BETWEEN "${data.startDt}" AND "${data.endDt}"
				GROUP BY UCH.userId
			)
			UCH1 ON (UCH1.userId=U0.userId)

			LEFT JOIN (
				SELECT
					SUM(UDS.totalDistance) as totalDistanceSum,
					UDS.userId
				FROM userDeviceSyncs UDS
				WHERE (UDS.deleted="0") AND (UDS.createdAt BETWEEN "${data.startDt}" AND "${data.endDt}")
				GROUP BY UDS.userId
			)
			UDS1 ON (UDS1.userId=U0.userId)

			WHERE (U0.blocked="0") AND (U0.deleted="0") AND (U0.name <> "") AND (U0.phoneVerified="1")

			#HAVING greenCoinsSum > 0 AND friendCount > 0
			HAVING friendCount > 0

			ORDER BY 
				greenCoinsSum DESC,
				totalDistanceSum DESC,
				dob ASC,
				gender ASC,
				createdAt ASC

		) U1,
		(select @rownum:=0) R

		${havingRaw}

		ORDER BY rank ASC
		LIMIT ${data.offset}, ${data.pageSize}
		`,
		{ 
			type: Sequelize.QueryTypes.SELECT,
			//logging: console.log
		});

};