const Db = require("../models");

//  Messages
const adminNotificationsSerMessages = {
    "UserChalStarted": "{{name}} challenge has started",
    "UserChalNoResult": "{{name}} challenge has ended with no result"
};
exports.adminNotificationsSerMessages = adminNotificationsSerMessages;

// ///  Create New Record
exports.createAdminNotificationSer = (data, options={}) => {
	return Db.adminNotifications.create(
		data,
		options
	);
};