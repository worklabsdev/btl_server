const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const filesURL = appConfig.upload[appConfig.upload.name].filesURL;

const { failedActionHandler } = require("../commonFuncs/responseHandler");

const { globalMessages } = require("../properties/constant");

const { commonSerAttributes } = require("./commonServices");

const { fileServiceTypes } = require("./filesServices");
const { appFilesAtts, adminFilesAtts } = require("./filesServices").attributes;

const { challengeUserStatuses } = require("./challengeUsers");

const Db = require("../models");

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const chalSerMessages = {
	ChalNotAvailMsg: "This challenge is currently not available",
	ChalCreatorAcceptErrorMsg: "Challenge cannot be accepted by the creator",
	ChalNotInvitedAcceptErrorMsg: "Sorry, you are not invited to accept this challenge",
	ChalInvalidActionMsg: "Invalid action for this challenge",
	ChalEndedMsg: "Sorry, this challenge has already ended",
	ChalAlreadyAcceptedMsg: "Sorry, this challenge has already been accepted by you",
	ChalAlreadyRejectedMsg: "Sorry, this challenge has already been rejected by you",
	ChalNotAvail4ActionMsg: "Sorry, this challenge is no longer available for",
};
exports.chalSerMessages = chalSerMessages;

const challengeAttributes = {
	"appDetails": { exclude: ["deleted", "blocked", "createdAt", "updatedAt", "visibleAt", "timezone"] }
};
exports.challengeAttributes = challengeAttributes;

//	Includes Constants
const chalSerIncludes = {
	creatorInclude: {
		model: Db.users,
		as: "creator",
		required: false,
		attributes: commonSerAttributes.userDetailsAtts
	},
	creatorIncludePush: {
		model: Db.users,
		as: "creator",
		required: false,
		attributes: commonSerAttributes.user4PushNotificationAtts
	}
};
exports.chalSerIncludes = chalSerIncludes;

//	Attributes Constants
const chalSerAttributes = {
	chalDetails4PushAtts: ["challengeId", "userId", "adminId", "startTitle", "endTitle", "startDt", "endDt", "totalDays", "coins", "status"]
};
exports.chalSerAttributes = chalSerAttributes;

const challengeDefaultSort = [
	["challengeId", "desc"]
];
exports.challengeDefaultSort = challengeDefaultSort;

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//		Files Include Sequelize Create
const filesIncludeCreatorSeq = (isAdmin, fileType=null) => {

	let where = { deleted: "0" };
	let attributes = adminFilesAtts;

	if(!isAdmin) {
		where.blocked = "0";
		attributes = appFilesAtts;
	}

	let as = "files";
	if(fileType) {
		where.type = fileType;

		if(fileType === fileServiceTypes.sponsorChallenge)
			as = "sponsorFiles";
		else if (fileType === fileServiceTypes.startChallenge)
			as = "startFiles";
		else
			as = "endFiles";
	}

	return {
		model: Db.files,
		where: where,
		as: as,
		attributes: attributes,
		required: false
	};

}
exports.filesIncludeCreatorSeq = filesIncludeCreatorSeq;

//	Challenge Files Seperator
const filesSeperator = (files) => {
	let result = { 
		sponsorFiles: [],
		startFiles: [],
		endFiles: []
	};

	files.forEach((file) => {

		file.fullURL = `${filesURL}${file.file}`;

		if(file.type === fileServiceTypes.sponsorChallenge)
			result.sponsorFiles.push(file);
		else if (file.type === fileServiceTypes.startChallenge)
			result.startFiles.push(file);
		else
			result.endFiles.push(file);

		result[file.type]
	});

	return result;
};
exports.filesSeperator = filesSeperator;

//	///		Add Challenge
exports.createChallenge = (data) => {

	//delete data.directions;

	if(!data.files)
		return Db.challenges.create(data);

	return Db.challenges.create(
		{
			...data,
			files: data.files
		},
		{
			include: [
				{
					as: "files",
					model: Db.files
				}
			]
		}
	);

}

//	///		Update  Challenge
exports.updateChallenge = (whereJSON, data, opts) => {
	return Db.challenges.update(
		data,
		{
			where: whereJSON,
			...opts
		},
	);
};

//	Challenge Valid Check
exports.challengeValidCheck = (response, whereJSON) => {
	return new Promise(async (resolve, reject) => {
		try {
			let challenge = await Db.challenges.findOne(
				{ where: whereJSON },
				{
					raw: true,
					// logging: console.log
				}
			);
			if (!challenge) 
				return failedActionHandler(response, chalSerMessages.ChalNotAvailMsg);

			return resolve(challenge);
		} catch (error) {
			return reject(error);
		}
	});
};

// ///       Challenge Update
exports.challengeUpdate = (response, whereJSON, data, options = null) => {
	let updatedOjs = {
		...data
	};

	if (!Object.keys(updatedOjs).length) 
		return failedActionHandler(response, globalMessages.responseMessages.nothingToUpdate);

	return Db.challenges.update(
		updatedOjs,
		{
			where: whereJSON,
			...options, // /	For Options Like Transactions
			// logging: console.log
		}
	);
};

//	///		User Challenge Details App
exports.userChallengeDetailsService = (response, whereJSON, odata) => {

	//	Challenge Users Include
	let challengeUsersInclude = {
		as: "challengeUsers",
		model: Db.challengeUsers,
		required: false,
		include: [
			{
				model: Db.users,
				as: "user",
				attributes: commonSerAttributes.userDetailsAtts
			}
		]
	};
	//	Challenge Users Include

	let includes = [
		chalSerIncludes.creatorInclude,
		challengeUsersInclude
	];

	return new Promise(async (resolve, reject) => {
		try {
			let challenge = await Db.challenges.findOne(
				{ 
					where: whereJSON,
					attributes: challengeAttributes.appDetails,
					include: includes
				}
			);
			if (!challenge)
				return failedActionHandler(response, chalSerMessages.ChalNotAvailMsg);

			challenge = JSON.parse(JSON.stringify(challenge));

			return resolve(challenge);
		} catch (error) {
			return reject(error);
		}
	});

};

//	///		Admin Challenge Details App
exports.adminChallengeDetailsService = (response, whereJSON, odata) => {

	let includes = [
		filesIncludeCreatorSeq(false),
		{
			as: "challengeUsers",
			model: Db.challengeUsers,
			where: {
				userId: odata.userId
			},
			required: false,
			include: [
				{
					model: Db.users,
					as: "user",
					attributes: commonSerAttributes.userDetailsAtts
				}
			]
		}
	];

	return new Promise(async (resolve, reject) => {
		try {
			let challenge = await Db.challenges.findOne(
				{ 
					where: whereJSON,
					attributes: challengeAttributes.appDetails,
					include: includes,
					//raw: true,
					//logging: console.log
				}
			);
			if (!challenge)
				return failedActionHandler(response, chalSerMessages.ChalNotAvailMsg); 

			challenge = JSON.parse(JSON.stringify(challenge));

			let files = filesSeperator(challenge.files);
			delete challenge.files;

			challenge.sponsorFiles = files.sponsorFiles;
			challenge.startFiles = files.startFiles;
			challenge.endFiles = files.endFiles;

			return resolve(challenge);
		} catch (error) {
			return reject(error);
		}
	});

}

//	///		User Challenge Details App
exports.challengeDetailsService = (response, whereJSON, odata) => {

	//	Challenge Users Include
	let challengeUsersInclude = {
		as: "challengeUsers",
		model: Db.challengeUsers,
		required: false,
		include: [
			{
				model: Db.users,
				as: "user",
				attributes: commonSerAttributes.userDetailsAtts
			}
		]
		//where: { type: { [Op.in]: [] } }
	};
	// if(odata.userIds) {
	// 	challengeUsersInclude.where = {  }
	// }

	//	Challenge Users Include

	let includes = [
		filesIncludeCreatorSeq(false),
		chalSerIncludes.creatorInclude,
		challengeUsersInclude
	];

	return new Promise(async (resolve, reject) => {
		try {
			let challenge = await Db.challenges.findOne(
				{ 
					where: whereJSON,
					attributes: challengeAttributes.appDetails,
					include: includes
				},
				{
					raw: true,
					// logging: console.log
				}
			);
			if (!challenge)
				return failedActionHandler(response, chalSerMessages.ChalNotAvailMsg); 

			challenge = JSON.parse(JSON.stringify(challenge));

			let files = filesSeperator(challenge.files);
			delete challenge.files;

			challenge.sponsorFiles = files.sponsorFiles;
			challenge.startFiles = files.startFiles;
			challenge.endFiles = files.endFiles;

			return resolve(challenge);
		} catch (error) {
			return reject(error);
		}
	});

}

//	User Dashboard Challenges Listing
exports.userDashChallengesList = (data) => {

	return Db.challenges.findAll({
		where: {
			deleted: "0",
			blocked: "0"
		},
		include: [
			{
				model: Db.challengeUsers,
				as: "challengeUser",
				where: {
					userId: data.userId,
					status: challengeUserStatuses.started 
					// {
					// 	[Op.notIn]: [challengeUserStatuses.pending, challengeUserStatuses.accepted, challengeUserStatuses.deleted]
					// },
					// startedAt: {
					// 	[Op.between]: [data.startDt, data.endDt]
					// }
				},
				attributes: [
					"challengeUserId", "totalDistance", "totalSteps", "status", "startedAt"
				]
			}
		],
		attributes: ["challengeId", "userId", "adminId", "startAddress", "endAddress", "totalDistance", "startTitle", "endTitle", "sponsorTitle"],
		order: [
			["challengeUser", "startedAt", "ASC"]
		],
		//logging: console.log
	});

};