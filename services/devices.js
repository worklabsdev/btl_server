const { failedActionHandler } = require("../commonFuncs/responseHandler");

const { globalMessages } = require("../properties/constant");

const Db = require("../models");

const attributes = {
	appDevicesListAtts: { "exclude": ["blocked", "createdAt", "updatedAt", "ranking"] }
};
exports.attributes = attributes;

const deviceSerMessages = {
	DeviceNotAvail: "This device is currently not available",
	FitbitAuthNotAvail: "Sorry, fitbit device auth is currently not available",
	FitbitAuthSuccess: "Fitbit authorized successfully"
};
exports.deviceSerMessages = deviceSerMessages;


//  Devices List
exports.appDevicesList = () => {
	return Db.devices.findAll({
		where: { blocked: "0" },
		attributes: attributes.appDevicesListAtts
	});
};

//  Device Valid Check
exports.deviceValidCheck = (response, whereJSON) => {
	return new Promise(async (resolve, reject) => {
		try {
			let device = await Db.devices.findOne(
				{ where: whereJSON },
				{
					raw: true
				}
			);
			if (!device) 
				return failedActionHandler(response, deviceSerMessages.DeviceNotAvail);

			return resolve(device);
		} catch (error) {
			return reject(error);
		}
	});
};

// ///       Device Update
exports.deviceUpdate = (whereJSON, data, options = null) => {
	let updatedOjs = {
		...data
	};

	if (!Object.keys(updatedOjs).length) 
		return failedActionHandler(response, globalMessages.responseMessages.nothingToUpdate);

	return Db.devices.update(
		updatedOjs,
		{
			where: whereJSON,
			...options, // /	For Options Like Transactions
			// logging: console.log
		}
	);
};

//	Device Get
const deviceSerGetOne = (whereJSON) => {
	return Db.devices.findOne({
		where: whereJSON
	});
};
exports.deviceSerGetOne = deviceSerGetOne;