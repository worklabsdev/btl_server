const Db = require("../models");

//  User Level Medals Calculator
const userMedalsLevelDbCalculator = (data) => {

    return Db.medals.findAll({
        //where: whereJSON,
        attributes: [
            "medalId", "minVal", "maxVal"
        ],
        raw: true
    })
    .then((dbMedals) => {
        return userMedalsLevelCalculator(dbMedals, data.totalGreenCoins);
    });

}
exports.userMedalsLevelDbCalculator = userMedalsLevelDbCalculator;

//  Medals Get
const medalSerGet = (whereJSON) => {
    return Db.medals.findAll({
        where: whereJSON,
        attributes: [
            "medalId", "minVal", "maxVal"
        ],
        raw: true
    });
};
exports.medalSerGet = medalSerGet;

//      User Level Get
const userMedalsLevelCalculator = (dbMedals, totalGreenCoins=0) => {
    let userLevel = 1;
    let userMedals = 0;
    let medals = [];

    try{
        dbMedals.forEach((dbMedal, index) => {

            if((totalGreenCoins >= dbMedal.minVal) && (totalGreenCoins <= dbMedal.maxVal)) {
                userLevel = dbMedal.medalId + 1;
                userMedals = dbMedal.medalId;
                //medals.push(dbMedal);
                throw "break";
            }

            //medals.push(dbMedal);

        });
    }
    catch(e) {}

    return {
        userLevel,
        userMedals,
        medals: dbMedals
    };

}
exports.userMedalsLevelCalculator = userMedalsLevelCalculator;