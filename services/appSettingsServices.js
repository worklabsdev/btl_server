const Db = require("../models");

const { appSettingAtts } = require("./commonServices").commonSerAttributes;

//	App Settings Get
const appSettingsSerGetOne = (whereJSON, attributes=appSettingAtts) => {
	return Db.appSettings.findOne({
		where: whereJSON,
		attributes
	});
};
exports.appSettingsSerGetOne = appSettingsSerGetOne;

//	Update Settings
const updateAppSettingsSer = (whereJSON, data, opts = {}) => {
	return Db.appSettings.update(
		data,
		{
			where: whereJSON,
			...opts
		},
	);
};
exports.updateAppSettingsSer = updateAppSettingsSer;