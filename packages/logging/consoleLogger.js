const logConfig = appConfig.errorSettings.log;

const { currentUTC }= require("../../commonFuncs/momentFuncs");

//	Request Logger
const requestLogger = (apiRefFull, data, opts) => {
	if (!logConfig.request) return;

	const utcNow = currentUTC();
	console.log(
		`===RequestInfo===> ${utcNow} - ${apiRefFull}, opts--${JSON.stringify(opts)}, data--${JSON.stringify(data)} <===RequestInfo===`
	);
};
exports.requestLogger = requestLogger;

//      Socket Info Loggers
const socketInfoLogger = (reference=null, requestId=null, data) => {
	if (!logConfig.socket.info) return;

	console.info(`===SocketInfo===>`, data, `reference = ${reference}`, `requestId = ${requestId}`, "<===SocketInfo===");
};
exports.socketInfoLogger = socketInfoLogger;

//      Socket Error Loggers
const socketErrorLogger = (reference=null, requestId=null, error) => {
    if(!logConfig.socket.error) return;

    console.error(`===SocketError===>`,  error, `reference = ${reference}`, `requestId = ${requestId}`, "<===SocketError===");
};
exports.socketErrorLogger = socketErrorLogger;

//      Socket Info Loggers
const pushInfoLogger = (data) => {
	if (!logConfig.push.info) return;

	console.info(`===PushInfo===>`, "Data = ", data, "<===PushInfo===");
};
exports.pushInfoLogger = pushInfoLogger;

//      Socket Error Loggers
const pushErrorLogger = (error, opts) => {
	if (!logConfig.push.error) return;

	console.error(`===PushError===>`, JSON.stringify(error, null, 2), `requestId = ${opts.requestId}`, "<===PushError===");
};
exports.pushErrorLogger = pushErrorLogger;


//		Info Logger
const infoLogger = (reference=null, requestId=null, data) => {
	if (!logConfig.info) return;

	console.log(
		"===Info===>",
		JSON.stringify({ reference, requestId, data }, null, 2),
		"<===Info==="
	);

};
exports.infoLogger = infoLogger;

//		Error Loggers
const errorLogger = (reference=null, requestId=null, error, data) => {
    if(!logConfig.error) return;

    console.error(`===ERROR===>`,  error, `reference = ${reference}`, `requestId = ${requestId}`, `data = `,data, "<===ERROR===");
};
exports.errorLogger = errorLogger;

//      Mail Info Loggers
const mailInfoLogger = (reference=null, requestId=null, data) => {
	if (!logConfig.mail.info) return;

	console.info(
		`===MailInfo===>`,
		`reference = ${reference}`,
		`requestId = ${requestId}`,
		data,
		"<===MailInfo==="
	);
};
exports.mailInfoLogger = mailInfoLogger;

//      Mail Error Loggers
const mailErrorLogger = (reference=null, requestId=null, error, data) => {
    if(!logConfig.mail.error) return;

	console.error(
		`===MailError===>`, 
		`reference = ${reference}`,
		`requestId = ${requestId}`,
		error,
		data,
		"<===MailError==="
	);
};
exports.mailErrorLogger = mailErrorLogger;