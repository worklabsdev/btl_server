const axios = require('axios');

const requestPostMake = (options) => {

    return new Promise((resolve, reject) => {

        axios.post(options.url, null, options.options)
        .then(function (response) {
            return resolve({ options, response: response.data, error: null });
        })
        .catch(function (error) {
            return resolve({ options, response: null, error: error.response.data });
        })

    });

};
exports.requestPostMake = requestPostMake;