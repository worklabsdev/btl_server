let AWS = require("aws-sdk");

let snsConfig = appConfig.sms.sns;

const { smsPackageMsgs } = require("./smsConstant");

AWS.config.update({
    accessKeyId: snsConfig.accessKeyId,
    secretAccessKey: snsConfig.secretAccessKey,
    Bucket: snsConfig.bucket,
    region: snsConfig.region
});

let sns = new AWS.SNS();

//  Send SMS
const sendSMS = (PhoneNumber, message) => {

    let params = {
        Protocol: "sms",
        TopicArn: snsConfig.topic,
        Endpoint: PhoneNumber,
        ReturnSubscriptionArn: true
    };

    return new Promise((resolve, reject) => {

        sns.subscribe(params, function(err, data) {
        
            if (err) {
                console.log(err, err.stack); // an error occurred
                return resolve({error: err.stack});
            }
            else {
                console.log(data);           // successful response
            }
            return resolve({data});
    
            let params = {
                Message: message,
                MessageStructure: 'STRING_VALUE',
                PhoneNumber: PhoneNumber,
                Subject: 'STRING_VALUE'
            };
    
            sns.publish(params, function(err, data) {
                if (err) {
                    console.log(err, err.stack);
                    return resolve({error: err.stack});
                }
                else {
                    console.log(data);
                    return resolve({data});
                }
            });

        });


    });












    // var params = {
    //     Message: message,
    //     TopicArn: snsConfig.topic, 
    //     MessageStructure: 'string',
    //     //PhoneNumber: PhoneNumber,
    //     Subject: "OTP"
    // };



    // return new Promise((resolve, reject) => {

    //     sns.publish(params, function(err, data) {
    //         if (err) {
    //             console.log(err, err.stack);
    //             return resolve({error: err.stack});
    //         }
    //         else {
    //             console.log(data);
    //             return resolve({data});
    //         }
    //     });

    // });

};
exports.sendSMS = sendSMS;