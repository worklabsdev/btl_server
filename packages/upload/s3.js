const aws = require("aws-sdk");
const fs = require("fs");

let { FileUploadError } = require("../../properties/constant").globalMessages.responseMessages;
let { generateRandStr } = require("../../commonFuncs/commonFuncs");

let s3Config = appConfig.upload.s3;

const s3 = new aws.S3({
    accessKeyId: s3Config.accessKeyId,
    secretAccessKey: s3Config.secretAccessKey,
    Bucket: s3Config.bucket
});

//  File Upload Single
const fileUpload = (file, elementName, opts={}) => {
	//const [fileName, fileExt] = file.name.split('.');

	let mimeType = file.type;
	let [fileType, fileExt] = mimeType.split("/");
	let fileName = generateRandStr(65)+`.${fileExt}`;

	return new Promise((resolve, reject) => {

		fs.readFile(file.path, function (error, fileBuffer) {
            if (error) {
                return reject(new Error(`${elementName} ${FileUploadError}`));
            }
            var params   = {
                Bucket     : s3Config.bucket,
                Key        : fileName,
                Body       : fileBuffer,
                ACL        : 'public-read',
                ContentType: mimeType
            };
            
            // console.log({
            //     Bucket     : s3Config.bucket,
            //     Key        : fileName,
            //     Body       : fileBuffer,
            //     ACL        : 'public-read',
            //     ContentType: mimeType
            // }, 
            // s3Config);

            s3.upload(params, function (err, data) {
                if (err) {
                    console.log("err", err);
                    return reject(new Error(`${elementName} ${FileUploadError}`));
                }
                else {
                    return resolve(fileName);
					// return resolve({
                    //     file: `${spaceConfig.filesURL}${fileName}`,
                    //     fileType: fileType,
                    //     fileExt: fileExt,
                    //     fileSize: file.size/1000//In KB
                    // });
                }
            });
        });

	});
}
exports.fileUpload = fileUpload;