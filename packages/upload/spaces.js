const aws = require("aws-sdk");
const fs = require("fs");

let { responseMessages } = require("../../properties/constant").globalMessages;
let { generateRandStr } = require("../../commonFuncs/commonFuncs");

let spaceConfig = appConfig.upload.spaces;

aws.config.update({
	accessKeyId: spaceConfig.accessKeyId,
	secretAccessKey: spaceConfig.secretAccessKey
});
// Create an S3 client setting the Endpoint to DigitalOcean Spaces
const spacesEndpoint = new aws.Endpoint(spaceConfig.endpoint);
const s3 = new aws.S3({
	"endpoint": spacesEndpoint
});

//  File Upload Single
const fileUpload = (file, elementName, opts={}) => {
	//const [fileName, fileExt] = file.name.split('.');

	let mimeType = file.type;
	let [fileType, fileExt] = mimeType.split("/");
	let fileName = generateRandStr(65)+`.${fileExt}`;

	return new Promise((resolve, reject) => {

		fs.readFile(file.path, function (error, file_buffer) {
            if (error) {
                return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
            }
            var params   = {
                Bucket     : spaceConfig.bucket,
                Key        : `${spaceConfig.folder}/${fileName}`,
                Body       : file_buffer,
                ACL        : 'public-read',
                ContentType: mimeType
			};

            s3.putObject(params, function (err, data) {
                if (err) {
                    return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
                }
                else {
                    return resolve(fileName);
					// return resolve({
                    //     file: `${spaceConfig.filesURL}${fileName}`,
                    //     fileType: fileType,
                    //     fileExt: fileExt,
                    //     fileSize: file.size/1000//In KB
                    // });
                }
            });
        });

	});
};
exports.fileUpload = fileUpload;