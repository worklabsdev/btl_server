const pushConfig = appConfig.pushNotifications.name;

if(pushConfig === "google") {
    var { sendPushNotification } = require("./google");
}

module.exports = {
    sendPushNotification
};