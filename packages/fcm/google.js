var admin = require("firebase-admin");

const googleConfig = appConfig.pushNotifications.google;

const { devicesTypes } = require("../../services/commonServices");

const { pushInfoLogger, pushErrorLogger } = require("../logging/consoleLogger");

admin.initializeApp({
	credential: admin.credential.cert({
	  	projectId: googleConfig.projectId,
	    clientEmail: googleConfig.clientEmail,
  	    privateKey: googleConfig.privateKey
	})
});

//  Send Push Notifications
const sendPushNotification = async (fcmId, pushData, opts={}) => {

	if(!fcmId)
		return;

	let notification = {
		title: googleConfig.title,
		body: pushData.message,
		pushType: pushData.pushType
	};

	var payload;
	if(opts.deviceOS === devicesTypes.ios) {//	IOS Push

		delete pushData.message;

		payload = {
			notification,
			data: pushData
		};
	}//	IOS Push
	else if(opts.deviceOS === devicesTypes.android) {//	Android Push

		pushData.title = googleConfig.title;
		pushData.body = pushData.message;
		delete pushData.message;

		payload = {
			data: {
				custom_notification: JSON.stringify({
					"sound": "default",
					"priority": "high",
					"show_in_foreground": true,
					"auto_cancel": true, 
					"vibrate": 300,
					"wake_screen": true,
					"channel":"bamboo",
					...pushData
				})
			}
		};

	}//	Android Push
	else {//	All Push
		payload = {
			notification,
		  	data: pushData
		};
	}//	All Push

	return admin.messaging().sendToDevice(fcmId, payload)
		.then((response) => {
			
			pushInfoLogger(
				{ 
					fcmId,
					payload,
					opts,
					response: JSON.stringify(response) 
				}
			);
			return response;

		})
		.catch((error) => {
			pushErrorLogger(error, { 
				fcmId,
				payload,
				opts 
			});
		});

};
exports.sendPushNotification = sendPushNotification;