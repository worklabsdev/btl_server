const shortid = require("shortid");
const randomstring = require("randomstring");
const moment = require("moment");
const uniqueSlug = require("unique-slug");
const uuidv4 = require('uuid/v4');


const { fullName } = require("../properties/constant").appConstants;

const { generateFutureDt } = require("./momentFuncs");

const inviteCodeGenerator = () => {
	return shortid.generate();
};
exports.inviteCodeGenerator = inviteCodeGenerator;

const generateRandStr = (len) => {
	return randomstring.generate(len);
};
exports.generateRandStr = generateRandStr;

const utcConvertor = (dt, timezone, format = "YYYY-MM-DD hh:mm:ss") => {
	return moment.tz(dt, format, timezone).utc().format(format);
};
exports.utcConvertor = utcConvertor;

// ///////////////////		Generate OTP 	/////////////////////////////////////
const generateOTP = (length = 4) => {
	let { otpValidityMinutes } = appConfig.sms;

	let otp = 4444;

	// let otp = randomstring.generate({
	//   	length: length,
	//   	charset: "numeric"
	// });

	let otpValidity = generateFutureDt(otpValidityMinutes);

	return { otp, otpValidity };
};
exports.generateOTP = generateOTP;

// /			User Slug Generator
const slugGenerator = (name = fullName) => {
	// var str = "Java Script Object Notation";
	var matches = name.match(/\b(\w)/g); // ['J','S','O','N']
	var acronym = matches.join(""); // JSON

	return `${acronym}-${uniqueSlug()}`;
};
exports.slugGenerator = slugGenerator;

//	/		Request Id Generate
const generateRequestId = () => {
	return uuidv4();
};
exports.generateRequestId = generateRequestId;

var waitFunc = ms => new Promise((r, j)=>setTimeout(r, ms));
exports.waitFunc = waitFunc;
//(async () => { await wait(2000); console.warn('done') })()
//await wait(2000);
