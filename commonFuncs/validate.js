const Joi = require("@hapi/joi");

const { validationHandler, errorHandlerView } = require("../commonFuncs/responseHandler");

const { requestLogger } = require("../packages/logging/consoleLogger");

let valOptions = {
	allowUnknown: false,
	abortEarly: true
};

// ///       COmmon Validate JOI Function
exports.joiValidate = (apiRefFull, response, schema, data, opts = {}) => {
	requestLogger(apiRefFull, data, opts);//	Logger For Requests

	return Joi.validate(data, schema, valOptions, (error, value) => {
		if (error) {
			return validationHandler(
				response,
				error.details[0].message,
				{
					apiRefFull
				}
			);
		}
		return true;
	});
};

// ///       COmmon Validate JOI Function
exports.joiValidateView = (apiRefFull, response, schema, data, opts = {}) => {
	requestLogger(apiRefFull, data, opts);//	Logger For Requests

	return Joi.validate(data, schema, valOptions, (error, value) => {
		if (error) {
			return errorHandlerView(
				response,
				new Error(error.details[0].message),
				{
					apiRefFull
				}
			);
		}
		return true;
	});
};