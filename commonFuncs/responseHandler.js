let errorConfig = appConfig.errorSettings;

let { statusCodes, successCodes, globalMessages, appConstants } = require("../properties/constant");
const { commonPaths } = require("../properties/apisPath").paths;

const { authErrorMessageGenerator } = require("../utils/authFuncs");
// let logger = require("./logging");

// ///       Error Handler
exports.errorHandler = (response, error, data = {}) => {
	// logger.fatal("fatal", error);
	console.error("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~", error);
	console.log("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~");

	let success = successCodes.error;

	let message = response.trans(globalMessages.responseMessages.errorMsg);
	if (errorConfig.showError) message = error.message;

	return response.status(statusCodes.error).json({
		success,
		message,
		data
	});
};

// ///       Error Handler View
exports.errorHandlerView = (response, error, data = {}) => {
	console.error("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~", error);
	console.log("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~");

	let message = response.trans(globalMessages.responseMessages.errorMsg);
	if (errorConfig.showError) message = error.message;

	return response.redirect(`${commonPaths.errorPath}?message=${message}`);
};

// ///       Success Handler
exports.successHandler = (response, msg = null, data = {}) => {
	let { success } = successCodes;
	let message = response.trans(msg || globalMessages.responseMessages.successMsg);

	return response.status(statusCodes.success).json({
		success,
		message,
		data
	});
};

// ///       Success Handler View
exports.successHandlerView = (response, msg=null, data = {}) => {
	let message = response.trans(msg || globalMessages.responseMessages.successMsg);

	return response.redirect(`${commonPaths.successPath}?message=${message}`);
};

// ///       Created Handler
exports.createdHandler = (response, msg = null, data = {}) => {
	let success = successCodes.created;
	let message = response.trans(msg || globalMessages.responseMessages.successMsg);

	return response.status(statusCodes.created).json({
		success,
		message,
		data
	});
};

// ///       Validation Handler
exports.validationHandler = (response, msg = null, data = {}) => {
	let success = successCodes.validationFailed;
	let message = msg || globalMessages.responseMessages.successMsg;
	// response.trans(msg || globalMessages.responseMessages.successMsg);

	return response.status(statusCodes.validationFailed).json({
		success,
		message,
		data
	});
};

// ///       Failed Action Handler
exports.failedActionHandler = (response, message = globalMessages.responseMessages.FailedAction, data = {}) => {
	let success = successCodes.actionFailed;
	message = response.trans(message);

	return response.status(statusCodes.actionFailed).json({
		success,
		message,
		data
	});
};



// ///       Auth Handler
exports.authHandler = (response, type = 2) => {
	let success = successCodes.authFailed;

	let message = authErrorMessageGenerator(response, type);

	return response.status(statusCodes.authFailed).json({
		success,
		message
	});
};

// ///       Auth Handler View
exports.authHandlerView = (response, type = 2) => {
	let message = authErrorMessageGenerator(response, type);

	return response.redirect(`${commonPaths.errorPath}?message=${message}`);
};

// ///		Handle 404
exports.pageNotFound = (response) => {
	let success = successCodes.pageNotFound;
	let message = response.trans(globalMessages.responseMessages.pageNotFound);

	return response.status(statusCodes.pageNotFound).json({
		success,
		message
	});
};
