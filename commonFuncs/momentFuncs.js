const moment = require("moment");

const defaultMomentFormat = "YYYY-MM-DD HH:mm:ss";
exports.defaultMomentFormat = defaultMomentFormat;

const defaultDateFormat = "YYYY-MM-DD";
exports.defaultDateFormat = defaultDateFormat;

//      Diff In Days
const momentsDiff = (startDt, endDt, units="hours") => {
    return moment(endDt).diff(startDt, units);
};
exports.momentsDiff = momentsDiff;

//  Generate Future DateTime
const generateFutureDt = (minutes) => {
	return moment().utc().add(minutes, "minutes").format(defaultMomentFormat);
};
exports.generateFutureDt = generateFutureDt;

//  Current UTC
const currentUTC = () => {
	return moment().utc().format(defaultMomentFormat);
};
exports.currentUTC = currentUTC;

//  Future Date Check
const futureDtCheck = (dt) => {
	return moment().utc().isBefore(moment().utc(dt));
};
exports.futureDtCheck = futureDtCheck;

const utcConvertor = (dt, timezone, format=defaultMomentFormat) => {
	return moment.tz(dt, format, timezone).utc().format(format);
};
exports.utcConvertor = utcConvertor;

//	Current Date Time
const currentUTCGenerate = (tz="UTC", format=defaultMomentFormat) => {	
	return moment.utc().format(format);
};
exports.currentUTCGenerate = currentUTCGenerate;

//	Moment Formatter
const formatDtFunc = (dt, format=defaultMomentFormat) => {
	return moment(dt).format(format);
};
exports.formatDtFunc = formatDtFunc;

//	Convert Moment Dt
const momentDtConvertToUTC = (dt, format=defaultMomentFormat) => {
	return moment(dt).utc().format(format);
};
exports.momentDtConvertToUTC = momentDtConvertToUTC;

exports.addSubtractTime = (dt, val, valSI="days", valFormat=defaultMomentFormat) => {

	return moment.tz(dt, "UTC").add(val, valSI).format(valFormat)
	// return {
	// 	//"moment(dt)": moment(dt),
	// 	"moment.tz(dt, 'UTC')": moment.tz(dt, "UTC"),
	// 	'moment.tz(dt, "UTC").add(24, "hours")': moment.tz(dt, "UTC").add(24, "hours"),
	// 	'moment.tz(dt, "UTC").add(24, "hours").format(valFormat)': moment.tz(dt, "UTC").add(24, "hours").format("YYYY-MM-DD HH:mm:ss")
	// };

	return moment(dt).add(val, valSI).format(valFormat);

	// return moment.tz(dt, "UTC").add(val, valSI).format(valFormat);
};


exports.moment = moment;