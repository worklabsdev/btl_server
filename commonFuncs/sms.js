var Twilio = require("twilio");

const { errorHandler } = require("./responseHandler");

let twilioConfig = appConfig.sms.twilio;
var twilioClient;

var twilioEnabled = 0;
if (twilioConfig.accountSid) {
	twilioClient = new Twilio(twilioConfig.TWILIO_ACCOUNT_SID, twilioConfig.TWILIO_AUTH_TOKEN);
	twilioEnabled = 1;
}

// /////////////////////		Validate Phone Number 		/////////////////////////
exports.validatePhoneNumber = (phoneCode, phoneNumber) => {
	if (!twilioEnabled) return new Error("Twilio Creds are missing");

	return new Promise((resolve, reject) => {
		twilioClient.lookups.phoneNumbers(phoneCode + phoneNumber)
			.fetch({ type: "carrier" })
	            .then((result) => {
				if (!result.countryCode) { return reject(new Error("Please provide a valid phone number")); }
					  return resolve();
			})
			.catch((exception) => {
				return reject(exception);
			});
	});
};

// ///////////////////		Send SMS 		////////////////////////////////////
exports.sendSMS = async (phone, message) => {
	if (!twilioEnabled) return new Error("Twilio Creds are missing");

	console.log("SMS Sent successfully");

	return twilioClient.messages.create({
		body: message,
		to: phone, // Text this number
		from: twilioConfig.TWILIO_NUMBER // From a valid Twilio number
	})
		.then((result) => {
			console.log(result);
			return result;
		});
};
